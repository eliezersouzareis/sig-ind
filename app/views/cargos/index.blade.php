@extends('main')

@section('content')
	<div class="panel panel-default" style="font-size:18px">
	    <div class="panel-heading">
	        <b>Cargos</b>
	        <div class="pull-right">
	            <a href="{{ URL::route('cargos.create') }}" class="btn btn-default btn-xs">Adicionar Novo</a>
	        </div>
	    </div>
	    <table class="table table-hover">
	        <tr class="active">
	            <th>Nome</th>
	            <th>Descrição</th>
	            <th colspan="2">Opções</th>
	        </tr>
	        @foreach ($cargos as $cargo)
	        <tr>
	            <td style="font-size:22px;">{{ $cargo->nome }}</td>
	            <td style="font-size:22px;">{{ $cargo->descricao }}</td>
	            <td width="50">
	                <a href="{{ URL::route('cargos.edit', $cargo->id) }}" class="btn btn-primary btn-sm">
						<span class="glyphicon glyphicon-pencil"></span>
						Editar
					</a>
	            </td>
	            <td width="60">
	            	{{ Form::open(array('method' => 'DELETE', 'route' => array('cargos.destroy', $cargo->id))) }}                       
                    	<button type="submit" class="btn btn-danger btn-sm">
							<span class="glyphicon glyphicon-remove"></span>
								Deletar
						</button>
                    {{ Form::close() }}
	            </td>
	        </tr>
	        @endforeach
	    </table>
	    
	    <div class="panel-footer" style="text-align:right;"><b>Total de Funções Cadastrados: {{ $cargos->count() }}</b></div>
	</div>    
@stop