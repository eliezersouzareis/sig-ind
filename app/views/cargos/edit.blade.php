@extends('main')

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Adicionar Cargo</b>
		</div>
		<div class="panel-body">
			{{ Form::model($cargo, array('method' => 'PATCH', 'route' =>
 array('cargos.update', $cargo->id))) }}{{ Form::open(array('route' => 'cargos.store')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							{{ Form::label('nome', 'Nome do Cargo:') }}
							{{ Form::text('nome', null, array('class' => 'form-control', 'style' => 'text-transform:uppercase;')) }}
						</div>
						<div class="col-md-6">
							{{ Form::label('descricao', 'Descrição:') }}
							{{ Form::text('descricao', null, array('class' => 'form-control', 'style' => 'text-transform:uppercase;')) }}
						</div>
					</div>
				</div>
				<br/>
				<a href="{{ URL::route('cargos.index') }}" class="btn btn-warning">Fechar</a>
				{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>

@stop