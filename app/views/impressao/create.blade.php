@extends('main')

@section('content')
{{ HTML::script('js/bootstrap-wysiwyg.js') }}
{{ HTML::script('js/jquery.hotkeys.js') }}
	<div id="alerts"></div>
<div class="row">
	<div class="col-md-2">
		<input type="text"/>
	</div>
	<div class="col-md-10">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
					<div class="btn-group">
						<a class="btn btn-default dropdown-toggle" data-toggle="dropdown" title="Font">
							<span class="glyphicon glyphicon-font"></span><span class="caret"></span>
						</a>
							<ul class="dropdown-menu">
							</ul>
						</div>
					<div class="btn-group">
						<a class="btn btn-default dropdown-toggle" data-toggle="dropdown" title="Font Size">
							<span class="glyphicon glyphicon-text-size"></span><span class="caret"></span>
						</a>
							<ul class="dropdown-menu">
							<li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
							<li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
							<li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
							</ul>
					</div>
					<div class="btn-group">
						<a class="btn btn-default" data-edit="bold" title="Bold (Ctrl/Cmd+B)">
							<span class="glyphicon glyphicon-bold"></span>
						</a>
						<a class="btn btn-default" data-edit="italic" title="Italic (Ctrl/Cmd+I)">
							<span class="glyphicon glyphicon-italic"></span>
						</a>
						<a class="btn btn-default" data-edit="strikethrough" title="Strikethrough">
							<div class="wrap">
								<b style="text-decoration:line-through">S</b>
							</div>
						</a>
						<a class="btn btn-default" data-edit="underline" title="Underline (Ctrl/Cmd+U)">
							<b style="text-decoration:underline">u</b>
						</a>
					</div>
					<div class="btn-group">
						<a class="btn btn-default" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)">
							<span class="glyphicon glyphicon-align-left"></span>
						</a>
						<a class="btn btn-default" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)">
							<span class="glyphicon glyphicon-align-center"></span>
						</a>
						<a class="btn btn-default" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)">
							<span class="glyphicon glyphicon-align-right"></span>
						</a>
						<a class="btn btn-default" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)">
							<span class="glyphicon glyphicon-align-justify"></span>
						</a>
					</div>
					<div class="btn-group">
						<a class="btn btn-default" data-edit="outdent" title="Reduce indent (Shift+Tab)">
							<span class="glyphicon glyphicon-indent-right"></span>
						</a>
						<a class="btn btn-default" data-edit="indent" title="Indent (Tab)">
							<span class="glyphicon glyphicon-indent-left"></span>
						</a>
					</div>
					<div class="btn-group">
						<a class="btn btn-default" title="Insert picture (or just drag & drop)" id="pictureBtn">
							<span class="glyphicon glyphicon-picture"></span>
						</a>
						<input type="file" class="hidden" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
					</div>
					<div class="btn-group">
						<a class="btn btn-default" data-edit="undo" title="Undo (Ctrl/Cmd+Z)">
							<span class="glyphicon glyphicon-share-alt fliph"></span>
						</a>
						<a class="btn btn-default" data-edit="redo" title="Redo (Ctrl/Cmd+Y)">
							<span class="glyphicon glyphicon-share-alt"></span>
						</a>
					</div>
				</div>
			</div>
		<div class="panel-body">
			<div id="editor"></div>
		</div>
	</div>
	</div>
</div>
<script>
	$(function(){
		function initToolbarBootstrapBindings() {
			var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier', 
						'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
						'Times New Roman', 'Verdana'],
						fontTarget = $('[title=Font]').siblings('.dropdown-menu');
			$.each(fonts, function (idx, fontName) {
					fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
			});
			$('a[title]').tooltip({container:'body'});
			$('.dropdown-menu input').click(function() {return false;})
				.change(function () {$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');})
				.keydown('esc', function () {this.value='';$(this).change();});

			$('[data-role=magic-overlay]').each(function () { 
				var overlay = $(this), target = $(overlay.data('target')); 
				overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
			});
	};
	function showErrorAlert (reason, detail) {
		var msg='';
		if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
		else {
			console.log("error uploading file", reason, detail);
		}
		$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
		 '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
	};
		initToolbarBootstrapBindings();  
	$('#editor').wysiwyg({ fileUploadError: showErrorAlert} );
		window.prettyPrint && prettyPrint();
	});
</script>
@stop