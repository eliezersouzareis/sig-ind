@extends('main')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Entradas de Insumos</b>
			<div class="pull-right">
				<a href="{{ URL::route('compras.create') }}" class="btn btn-default btn-xs">Registrar Entrada de Insumos</a>
			</div>
		</div>
		<table class="table table-hover">
			<tr class="active">
				<th class="col-md-2">Data</th>
				<th class="col-md-4">Fornecedor</th>
				<th class="col-md-3">Valor</th>
				<th class="col-md-3">Opções</th>
			</tr>
			@foreach ($compras as $compra)
			<tr>
				<td>{{ date("d/m/Y",strtotime($compra->data)) }}</td>
				<td>{{ $compra->fornecedor->nome }}</td>
				<td>{{ $compra->valor_total }}</td>
				<td>
					<a href="{{ URL::route('compras.edit', $compra->id)}}" class="btn btn-primary btn-sm">
						<span class="glyphicon glyphicon-pencil"></span>
						Editar
					</a>
					{{ Form::open(['method' => 'DELETE', 'route' => ['compras.destroy', $compra->id], 'class' => 'force-inline-block']) }}                       
						<button type="submit" class="btn btn-danger btn-sm">
							<span class="glyphicon glyphicon-remove"></span>
							Deletar
						</button>
					{{ Form::close() }}
				</td>
			</tr>
			@endforeach
		</table>
		
		<div class="panel-footer" style="text-align:right;"><b>Total de Compras Cadastrados: {{ Compra::count() }}</b></div>
	</div>    
	<div style="float:left; position:absolute;"></div>
@stop