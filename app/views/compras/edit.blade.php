@extends('main')

@section('content')

<script>
jQuery(function($){
	$(".data-mask").mask("99/99/9999");
	
	$('.input-qtd').on('focusout',function(){
		var qtd = parseFloat($('.input-qtd').val());
		var valor = parseFloat($('.input-valor').val());
		$('.input-total').val(parseFloat(valor*qtd));
	});

});
</script>


<div ng-controller="AdicionarEntrada">
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Editar Entrada de Insumo</b>
		</div>
		<div class="panel-body" ng-init="compraId = {{ $compra->id }};">
			{{ Form::model($compra, array('method' => 'PATCH', 'route' =>array('compras.update', $compra->id))) }}
			{{ Form::open(array('route' => 'compras.store')) }}
			<div class="form-group">
					<div class="row">
						<div class="col-md-2">
							{{ Form::label('data', 'Data da Compra:') }}
							{{ Form::input('date','data', date('Y-m-d'), array('class' => 'form-control')) }}
						</div>
							{{ Form::text('fornecedor_id', null, array('class' => 'form-control hidden', 'readonly', 'ng-model' => 'fornecedorSelecionado.id')) }}
						<div class="col-md-10">
							{{ Form::label('fornecedor', 'Fornecedor:') }}
							<div class="input-group">
								<input type="text" class="form-control" disabled ng-model="fornecedorSelecionado.nome">
								<span class="input-group-btn">
									<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#myModal">Selecionar...</button>
								</span>
							</div><!-- /input-group -->
						</div>
					</div>
					<br/>
					<fieldset>
						<div class="row">
							<div class="col-md-12">
								<legend>Itens:</legend>	
							</div>
						</div>
							<table class="table">
								<thead>
									<tr class="active">
										<th class="col-md-5"> Insumo</th>
										<th class="col-md-3"> Valor </th>
										<th class="col-md-3"> Quant.</th>
										<th class="col-md-1">
											<button type="button" class="btn btn-sm btn-primary pull-right" data-toggle="modal" data-target="#insumosModal">
												<span class="glyphicon glyphicon-plus"></span>
												Adicionar Insumo
											</button>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="insumo in insumosSelecionados">
										<td>
											[[ insumo.nome ]]
										</td>
										<td>
											<div class="input-group">
												<span class="input-group-addon">
													R$
												</span>
												<input type="text" name="insumos[ [[insumo.id]] ][preco]" class="form-control" ng-model="insumo.preco"/>
											</div>
										</td>
										<td>
											<div class="input-group">
												<input type="text" name="insumos[ [[insumo.id]] ][qtd]" class="form-control" ng-model="insumo.qtd"/>
												<span class="input-group-addon">
													[[ insumo.unidade.abreviatura ]]
												</span>
											</div>
										</td>
										<td>
											<button type="button" class="btn btn-danger btn-sm" ng-click="delInsumo(insumo.id)">
												<span class="glyphicon glyphicon-remove">
												</span>
											</button>
										</td>
									</tr>
								</tbody>
							</table>
							<div class="row">
								<div class="col-md-1 col-md-offset-8">
									<b>Total:</b>
								</div>
								<div class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon">
											R$
										</span>
										<input type="text" name="valor_total" class="form-control" value="[[sum()]]"/>
									</div>
								</div>
							</div>
						<!-- Colocar uma forma de adquirir mais insumos por compras -->
					</fieldset>
					<hr/>
					<br/>
				</div>
				<a href="{{ URL::route('compras.index') }}" class="btn btn-warning">Cancelar</a>
				{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>


	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Fornecedor</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<tr class="active">
						<th>Nome</th>
						<th>CPF/CNPJ</th>
						<th>Telefone</th>
						<th colspan="2">Opções</th>
					</tr>

					<tr ng-repeat="f in fornecedores | filter:search">
						<td>[[f.nome]]</td>
						<td>[[f.cpf_cnpj]]</td>
						<td>[[f.telefone_fixo]]</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="pegaFornecedor(f.nome,f.id);" data-dismiss="modal">Selecionar</button></td>
					</tr>
				</table>

				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>

@stop