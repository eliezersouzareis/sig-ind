@extends('main')

@section('content')

<script>
jQuery(function($){
	$(".data-mask").mask("99/99/9999");
});
</script>

<div ng-controller="AdicionarEntrada">
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Nova Entrada de Insumo</b>
		</div>
		<div class="panel-body">
			{{ Form::open(array('route' => 'compras.store')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-2">
							{{ Form::label('data', 'Data da Compra:') }}
							{{ Form::input('date','data', date('Y-m-d'), array('class' => 'form-control')) }}
						</div>
							{{ Form::text('fornecedor_id', null, array('class' => 'form-control hidden', 'readonly', 'ng-model' => 'fornecedorSelecionado.id')) }}
						<div class="col-md-10">
							{{ Form::label('fornecedor', 'Fornecedor:') }}
							<div class="input-group">
								<input type="text" class="form-control" disabled ng-model="fornecedorSelecionado.nome">
								<span class="input-group-btn">
									<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#myModal">Selecionar...</button>
								</span>
							</div><!-- /input-group -->
						</div>
					</div>
					<br/>
					<fieldset>
						<div class="row">
							<div class="col-md-12">
								<legend>Itens:</legend>	
							</div>
						</div>
							<table class="table">
								<thead>
									<tr class="active">
										<th class="col-md-5"> Insumo</th>
										<th class="col-md-3"> Valor </th>
										<th class="col-md-3"> Quant.</th>
										<th class="col-md-1">
											<button type="button" class="btn btn-sm btn-primary pull-right" data-toggle="modal" data-target="#insumosModal">
												<span class="glyphicon glyphicon-plus"></span>
												Adicionar Insumo
											</button>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="insumo in insumosSelecionados">
										<td>
											[[ insumo.nome ]]
										</td>
										<td>
											<div class="input-group">
												<span class="input-group-addon">
													R$
												</span>
												<input type="text" name="insumos[ [[insumo.id]] ][preco]" class="form-control" ng-model="insumo.preco"/>
											</div>
										</td>
										<td>
											<div class="input-group">
												<input type="text" name="insumos[ [[insumo.id]] ][qtd]" class="form-control" ng-model="insumo.qtd"/>
												<span class="input-group-addon">
													[[ insumo.unidade.abreviatura ]]
												</span>
											</div>
										</td>
										<td>
											<button type="button" class="btn btn-danger btn-sm" ng-click="delInsumo(insumo.id)">
												<span class="glyphicon glyphicon-remove">
												</span>
											</button>
										</td>
									</tr>
								</tbody>
							</table>
							<div class="row">
								<div class="col-md-1 col-md-offset-8">
									<b>Total:</b>
								</div>
								<div class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon">
											R$
										</span>
										<input type="text" name="valor_total" class="form-control" value="[[sum()]]"/>
									</div>
								</div>
							</div>
						<!-- Colocar uma forma de adquirir mais insumos por compras -->
					</fieldset>
					<hr/>
					<br/>
				</div>
				<a href="{{ URL::route('compras.index') }}" class="btn btn-warning">Cancelar</a>
				{{ Form::submit('Salvar e Adicionar Conta a Pagar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>

	<!-- Modal Fornecedor-->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Fornecedor</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<tr class="active">
						<th>Fornecedor</th>
						<th>Nome Fantasia</th>
						<th colspan="2"></th>
					</tr>

					<tr ng-repeat="fornecedor in fornecedores | filter:search">
						<td>[[fornecedor.nome]]</td>
						<td>[[fornecedor.nome_fantasia]]</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getFornecedor(fornecedor.id);" data-dismiss="modal">Selecionar</button></td>
					</tr>
				</table>

				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>

<!-- Modal Insumos -->
	<div class="modal fade" id="insumosModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Insumo</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<thead>
						<tr class="active">
							<th>Nome</th>
							<th>Unidade</th>
							<th></th>
						</tr>
					</thead>	
					<tbody>
					<tr ng-repeat="insumo in insumosFornecidos | filter:search">
						<td>[[insumo.nome]]</td>
						<td>
							<span class="badge">&nbsp;[[ insumo.unidade.abreviatura ]]&nbsp;</span>
						</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getInsumo(insumo.id);" data-dismiss="modal">Selecionar</button></td>
					</tr>	
					</tbody>
				</table>

				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
	
</div>

@stop