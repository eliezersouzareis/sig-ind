@extends('config')

@section('tabContent')
<script type="text/javascript">
	$("#tabulacao li a[href='{{ URL::route('config.unidades.index') }}']").parent().addClass('active');;
</script>
	<div class="panel panel-default">
	    <div class="panel-heading">
	        <b style="font-size:24px;">Unidades</b>
	        <div class="pull-right">
	            <a href="{{ URL::route('config.unidades.create') }}" class="btn btn-default">Cadastrar nova</a>
	        </div>
	    </div>
	    <table class="table table-hover" style="font-size:18px;">
	        <tr class="active">
	            <th class="col-md-8">Nome</th>
	            <th class="col-md-3">Abreviatura</th>
	            <th class="col-md-1" colspan="2">Opções</th>
	        </tr>
	        @foreach ($unidades as $unidade)
	        <tr>
	            <td>{{ $unidade->nome }}</td>
	            <td>{{ $unidade->abreviatura }}</td>
	            <td>
	                {{ link_to_route('config.unidades.edit', 'Editar', array($unidade->id), array('class' => 'btn btn-info btn-xs')) }}
	            </td>
	            <td>
	            	{{ Form::open(array('method' => 'DELETE', 'route' => array('config.unidades.destroy', $unidade->id))) }}                       
                    	{{ Form::submit('Deletar', array('class' => 'btn btn-danger btn-xs')) }}
                    {{ Form::close() }}
	            </td>
	        </tr>
	        @endforeach
	    </table>
	    
	    <div class="panel-footer" style="text-align:right;"><b>Total Unidades Cadastrados: <?php  echo DB::table('unidades')->count(); ?></b></div>
	</div>

@stop