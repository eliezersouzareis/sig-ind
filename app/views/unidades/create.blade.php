@extends('config')

@section('tabContent')
<script type="text/javascript">
	$("#tabulacao li a[href='{{ URL::route('config.unidades.index') }}']").parent().addClass('active');;
</script>
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Cadastrar Unidade</b>
		</div>
		<div class="panel-body">
			{{ Form::open(array('route' => 'config.unidades.store')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							{{ Form::label('nome', 'Nome:') }}
							{{ Form::text('nome', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6">
							{{ Form::Label('abreviatura', 'Abreviatura:') }}
							{{ Form::text('abreviatura', null, array('class' => 'form-control')) }}
						</div>
					</div>
				</div>
				<br/>
				<a href="{{ URL::route('config.unidades.index') }}" class="btn btn-warning">Fechar</a>
				{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>


@stop