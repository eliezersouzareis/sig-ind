@extends('main')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Contas a Pagar</b>
			<div class="pull-right">
				<a href="{{ URL::route('contaspagar.create') }}" class="btn btn-default btn-xs">Nova Conta</a>
			</div>
		</div>
		
		<div class="panel-body">
		</div>
	
		<table class="table table-hover">
			<thead>
				<tr class="active">
					<th class="col-md-1">Data</th>
					<th class="col-md-4">Fornecedor</th>
					<th class="col-md-3">Conta</th>
					<th class="col-md-2">Valor Total</th>
					<th class="col-md-2">Opções</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($contasPagar as $conta)
				<?php 
					$isEditable = true;
					$isPaid = false;
					$sum = 0; $count = 0;	
					foreach($conta->parcelas as $parcela)
					{
						if($parcela->data_pagamento && $isEditable)
						{
							$isEditable = false;
						}
						if($parcela->valor_pago)
						{
							$sum += $parcela->valor_pago;
							$count++;
						}
					}
					if($sum >= $conta->valor_total && $count == $conta->parcelas->count())
					{
						$isPaid = true;
					}
				?>
				@if($isPaid)
					<tr class="active text-muted">
				@else
					<tr>
				@endif
					<td>{{ date("d/m/Y",strtotime($conta->data)) }}</td>
					<td>{{ $conta->fornecedor }}</td>
					<td>{{ ($conta->categoria)? $conta->categoria->codigo.' - '.$conta->categoria->nome :"" }}</td>
					<td>{{ $conta->valor_total }}</td>
					<td class="row">
						<a href="{{ URL::route('contaspagar.show', $conta->id) }}" class="btn btn-success btn-sm">
							<span class="glyphicon glyphicon-eye-open"></span>
							Ver
						</a>
						@if($isPaid)
							<span class="glyphicon glyphicon-ok"></span>
							Pago
						@endif
						@if($isEditable)
						<a href="{{ URL::route('contaspagar.edit', $conta->id) }}" class="btn btn-primary btn-sm">
							<span class="glyphicon glyphicon-pencil"></span>
						</a>
						{{ Form::open(['method' => 'DELETE', 'route' => ['contaspagar.destroy', $conta->id], 'class'=> 'force-inline-block']) }}                       
							<button type="submit" class="btn btn-danger btn-sm">
								<span class="glyphicon glyphicon-remove"></span>
							</button>
						{{ Form::close() }}
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		
		<div class="panel-footer" style="text-align: right;"><b>Total de Contas Encontradas: {{ $contasPagar->count() }}</b></div>
	</div>    

	


@stop