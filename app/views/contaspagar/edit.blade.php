@extends('main')

@section('content')
<div ng-controller="AdicionarContaPagar">

<script>
jQuery(function($){
	$('.input-qtd').on('focusout',function(){
		var qtd = parseFloat($('.input-qtd').val());
		var valor = parseFloat($('.input-valor').val());
		$('.input-total').val(parseFloat(valor*qtd));
	});
	$('.input-valor').on('focusout',function(){
		var qtd = parseFloat($('.input-qtd').val());
		var valor = parseFloat($('.input-valor').val());
		$('.input-total').val(parseFloat(valor*qtd));
	});
});
</script>

<div class="panel panel-default" ng-init="contaPagarId = {{ $conta->id }};">
		<div class="panel-heading">
			<b>Editar Conta a Pagar</b>
		</div>
		<div class="panel-body">
			{{ Form::model($conta, array('method' => 'PATCH', 'route' =>
			 array('contaspagar.update', $conta->id))) }}
			{{ Form::open(array('route' => 'contaspagar.store')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-2">
							{{ Form::label('data', 'Data da Conta:') }}
							{{ Form::input('date','data', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4">
							{{ Form::label('fornecedor', 'Fornecedor:') }}
							{{ Form::text('fornecedor', null, array('class' => 'form-control', 'id' => 'autocomplete')) }}
						</div>
							<input type="text" name="categoria_conta_id" class="hidden" ng-model="categoriaConta.id" ng-init="categoriaConta.id = {{ $conta->categoria_conta_id }}"/>
						<div class="col-md-6">
							{{ Form::label('', 'Categoria da Conta:') }}
							<div class="input-group">
								<input type="text" class="form-control" value="[[ (categoriaConta ? categoriaConta.codigo +' - '+ categoriaConta.nome:'') ]]" disabled/>
								<span class="input-group-btn">
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#categoriasModal">
										<span class="glyphicon glyphicon-search"></span>
									</button>
								</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							{{ Form::label('nf_data', 'Data NF:') }}
							{{ Form::input('date', 'nf_data', null,array('class' => 'form-control')) }}
						</div>
						<div class="col-md-10">
							{{ Form::label('nf_num', 'Num. NF:') }}
							{{ Form::text('nf_num', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<br/>
					<fieldset>
						<div class="row" ng-init="count=0;">
							<div class="col-md-12">
								<legend>
									Itens da Conta:
								</legend>	
							</div>
						</div>
						<table class="table">
							<thead>
								<tr class="active">
									<th class="col-md-5">Item</th>
									<th class="col-md-2">Valor</th>
									<th class="col-md-2">Quantidade</th>
									<th class="col-md-2">Total</th>
									<th class="col-md-1">
										<button type="button" class="btn btn-primary btn-sm" ng-click="addItem()" >
											<span class="glyphicon glyphicon-plus"></span>
											Adicionar Item
										</button>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="item in cesta">
									<td>
										<input type="text" name="itens[ [[item.id]] ][nome]" class="form-control" ng-model="item.nome" />
									</td>
									<td>
										<div class="input-group">
											<span class="input-group-addon">
												R$
											</span>
											<input type="text" name="itens[ [[item.id]] ][valor]" class="form-control" ng-model="item.valor" />
										</div>
									</td>
									<td>
										<input type="text" name="itens[ [[item.id]] ][qtd]" class="form-control" ng-model="item.qtd" />
									</td>
									<td>
										<div class="input-group">
											<span class="input-group-addon">
												R$
											</span>
											<input type="text" name="itens[ [[item.id]] ][total]" class="form-control" value="[[item.total=item.valor*item.qtd]]" readonly/>
										</div>
									</td>
									<td>
										<button type="button" class='btn btn-danger btn-sm' ng-click="delItem(item.id)">
											<span class="glyphicon glyphicon-remove"></span>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="row">
							<div class="col-md-1 col-md-offset-8">
								Total:
							</div>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
										R$
									</span>
									<input type="text" name="valor_total" class="form-control" value="[[sum()]]"/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-1 col-md-offset-8">
								Desconto:
							</div>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
										R$
									</span>
									<input type="text" name="desconto" class="form-control" value="" ng-model="desconto" ng-init="desconto = {{$conta->desconto}}"/>
								</div>
							</div>
						</div>
					</fieldset>
					
					<hr/>
				</div>
				<a href="{{ URL::previous() }}" class="btn btn-warning">Cancelar</a>
				{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
</div>

	<div class="modal fade" id="categoriasModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-focus="page = 0">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Categoria</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<thead>
						<tr class="active">
							<th>Cód</th>
							<th>Nome</th>
							<th></th>
						</tr>
					</thead>	
					<tbody>
					<tr ng-repeat="categoria in categoriasConta | filter:search | limitTo:6:page">
						<td>[[ categoria.codigo ]]</td>
						<td>[[ categoria.nome ]]</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getCategoria(categoria.id)" data-dismiss="modal">Selecionar</button></td>
					</tr>	
					</tbody>
				</table>
				<div class="row">
					<div class="col-xs-1 col-xs-offset-4">
						<button type="button" class="btn btn-default" ng-click="page = page-6" ng-disabled="page == 0">
							<span class="glyphicon glyphicon-menu-left"></span>
						</button>
					</div>
					<div class="col-xs-1 col-xs-offset-2">
						<button type="button" class="btn btn-default" ng-click="page = page+6" ng-disabled="(page+6)>categoriasConta.length">
							<span class="glyphicon glyphicon-menu-right"></span>
						</button>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>


</div>
@stop