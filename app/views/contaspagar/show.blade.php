@extends('main')

@section('content')
<div ng-controller="ViewContaPagar">

	<h4 class="page-header">
		Conta
	</h4>
	<div class="row">
		<div class="col-md-1">
			<b>Data:</b></br>
			{{ date('d/m/Y',strtotime($conta->data)) }}
		</div>
		<div class="col-md-5">
			<b>Fornecedor:</b></br>
			{{ $conta->fornecedor }}
		</div>
		<div class="col-md-3">
			<b>Categoria da Conta:</b></br>
			{{ $conta->categoria->codigo." - ".$conta->categoria->nome  }}
		</div>
		<div class="col-md-3">
			<b>Total:</b></br>
			R$ {{ $conta->valor_total }}
		</div>
	</div>
	<br />
	<div class="row">
		<div class="col-md-1">
			<b>Data NF:</b></br>
			{{ date('d/m/Y',strtotime($conta->nf_data)) }}
		</div>
		<div class="col-md-11">
			<b>Número NF:</b></br>
			{{ $conta->nf_num }}
	</div>
	<br />
	<a href="#itensConta" data-toggle="collapse" href="#itensConta" aria-expanded="false" aria-controls="itensConta">
	<h4 class="page-header">
		Itens da Conta
		<small>
				<span class="glyphicon glyphicon-chevron-down"></span>
		</small>
	</h4>
	</a>
	<div class="collapse" id="itensConta">
		<table class="table table-default">
			<thead>
				<tr>
					<th class="col-md-4">Nome</th>
					<th class="col-md-3">Quantidade</th>
					<th class="col-md-2">Valor</th>
					<th class="col-md-3">Total</th>
				</tr>
			</thead>
			<tbody>
			@foreach($conta->itens as $item)
				<tr>
					<td>{{ $item->nome }}</td>
					<td>{{ $item->qtd }}</td>
					<td>R$ {{ $item->valor }}</td>
					<td>R$ {{ $item->valor*$item->qtd }}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
	</br>
	<h4 class="page-header">
		Parcelas Existentes
		@if($conta->parcelas->isEmpty())
		<span class="pull-right">
			<a href="{{ URL::route('contaspagar.parcelas.create',$conta->id) }}" class="btn btn-primary btn-sm">
				<span class="glyphicon glyphicon-plus"></span>
				Criar parcelas
			</a>
		</span>
		@endif
	</h4>
	<?php 
		$parcelas = $conta->parcelas;
		$parcelas = $parcelas->sortBy('data_pagamento');
		$pago  = $conta->parcelas->sum('valor_pago');
		$total = $conta->parcelas->sum('valor');
	?>
	<b>Pago:</b> R$ {{ $pago }} <b>Restante:</b> R$ {{ $total - $pago }}
	<table class="table table-default">
		<thead>
			<tr>
				<th class="col-md-2">Vencimento</th>
				<th class="col-md-3">Valor</th>
				<th class="col-md-2">Valor pago</th>
				<th class="col-md-3">Forma de Pagamento</th>
				<th class="col-md-2"></th>
			</tr>
		</thead>
		<tbody>
		@foreach($parcelas as $parcela)
			@if($parcela->data_pagamento)
				<tr class="active">
					<td class="text-muted">{{ date('d/m/Y', strtotime($parcela->data_vencimento)) }}</td>
					<td class="text-muted">R$ {{ $parcela->valor }}</td>
					<td class="text-muted">R$ {{ $parcela->valor_pago }}</td>
					<td class="text-muted">{{ $parcela->formaPagamento->codigo.' '.$parcela->formaPagamento->nome }}</td>
					<td class="text-muted">
						<span class="glyphicon glyphicon-ok"></span>
						Pago
					</td>
				</tr>
			@else
				<tr>
					<td>{{ date('d/m/Y', strtotime($parcela->data_vencimento)) }}</td>
					<td>R$ {{ $parcela->valor }}</td>
					<td></td>
					<td>{{ $parcela->formaPagamento->codigo.' '.$parcela->formaPagamento->nome }}</td>
					<td>
						<a role="button" class="btn btn-warning btn-sm" href="{{ URL::route('contaspagar.parcelas.pagar',[$conta->id,$parcela->id]) }}">
							<span class="glyphicon glyphicon-usd"></span>
							Pagar
						</a>
						<a role="button" class="btn btn-primary btn-sm" href="{{ URL::route('contaspagar.parcelas.edit',[$conta->id,$parcela->id]) }}">
							<span class="glyphicon glyphicon-pencil"></span>
							Editar
						</a>
					</td>
				</tr>
			@endif
		@endforeach
		</tbody>
	</table>
</div>
@stop