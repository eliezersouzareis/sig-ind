@extends('config')

@section('tabContent')
<script type="text/javascript">
	$("#tabulacao li a[href='{{ URL::route('config.usuarios.index') }}']").parent().addClass('active');;
</script>
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Editar Usuário</b>
		</div>
		<div class="panel-body">
			{{ Form::model($usuario, array('method' => 'PATCH', 'route' =>
			array('config.usuarios.update', $usuario->id))) }}	    	
			{{ Form::open(array('route' => 'config.usuarios.store')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{{ Form::label('name', 'Nome:') }}
							{{ Form::text('name', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							{{ Form::Label('username', 'Usuário:') }}
							{{ Form::text('username', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							{{ Form::Label('email', 'E-mail:') }}
							{{ Form::text('email', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-12">
							{{ Form::Label('password', 'Nova Senha') }}
							{{ Form::password('password', array('class' => 'form-control')) }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							{{ Form::Label('confirmar', 'Confimar Senha') }}
							{{ Form::password('confirmar', array('class' => 'form-control')) }}
						</div>
					</div>
				</div>
				<br/>
				<a href="{{ URL::route('config.usuarios.index') }}" class="btn btn-warning">Fechar</a>
				{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>

@stop