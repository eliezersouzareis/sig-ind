@extends('config')

@section('tabContent')
<script type="text/javascript">
	$("#tabulacao li a[href='{{ URL::route('config.usuarios.index') }}']").parent().addClass('active');;
</script>

	<div class="panel panel-default">
	    <div class="panel-heading">
	        <b style="font-size:24px;">Usuários</b>
	        <div class="pull-right">
	            <a href="{{ URL::route('config.usuarios.create') }}" class="btn btn-default">Cadastrar novo</a>
	        </div>
	    </div>
	    <table class="table table-hover" style="font-size:18px;">
	        <tr class="active">
	            <th class="col-md-2">Usuário</th>
	            <th class="col-md-6">Nome</th>
	            <th class="col-md-3">E-mail</th>
	            <th class="col-md-1" colspan="2">Opções</th>
	        </tr>
	        @foreach ($usuarios as $usuario)
	        <tr>
	            <td>{{ $usuario->username }}</td>
	            <td>{{ $usuario->name }}</td>
	            <td>{{ $usuario->email }}</td>
	            <td>
	                {{ link_to_route('config.usuarios.edit', 'Editar', array($usuario->id), array('class' => 'btn btn-info btn-xs')) }}
	            </td>
	            <td>
	            	{{ Form::open(array('method' => 'DELETE', 'route' => array('config.usuarios.destroy', $usuario->id))) }}                       
                    	{{ Form::submit('Deletar', array('class' => 'btn btn-danger btn-xs')) }}
                    {{ Form::close() }}
	            </td>
	        </tr>
	        @endforeach
	    </table>
	    
	    <div class="panel-footer" style="text-align:right;"><b>Usuarios Cadastrados: <?php  echo DB::table('users')->count(); ?></b></div>
	</div>


@stop