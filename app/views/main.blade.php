<!doctype html>
<html ng-app='sigind'>
<head>
	<meta charset="utf-8">
	<title>Administração</title>

	{{ HTML::style('css/style.css') }}
	<!-- Latest compiled and minified CSS -->
	{{ HTML::style('css/bootstrap.min.css') }}
	{{ HTML::style('css/autocomplete.css') }}
	{{ HTML::style('css/jquery-ui.css') }}

	{{ HTML::script('js/jquery-1.11.2.min.js') }}
	{{ HTML::script('js/jquery-ui.js') }}
	{{ HTML::script('js/angular.min.js') }}
	{{ HTML::script('js/jquery.maskedinput.min.js') }}
	{{ HTML::script('js/bootstrap.min.js') }}

	{{ HTML::script('js/modernizr.js') }}

	{{ HTML::script('js/autocomplete.js') }}
	{{ HTML::script('js/app.js') }}
</head>
<body>
	<nav class="navbar navbar-default navbar-static-top hidden-print" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">SIG-IND</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" role="button" >Cadastro <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ URL::route('insumos.index') }}">Insumo</a></li>
							<li><a href="{{ URL::route('produtos.index') }}">Produto</a></li>
							<li><a href="{{ URL::route('fornecedores.index') }}">Fornecedor</a></li>                                
							<li><a href="{{ URL::route('funcionarios.index') }}">Funcionario</a></li>
							<li><a href="{{ URL::route('cargos.index') }}">Função</a></li>
							<li><a href="{{ URL::route('clientes.index') }}">Cliente</a></li>
							<li><a href="{{ URL::route('veiculos.index') }}">Veículo</a></li>
						</ul>
					</li>
					<!--
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" role="button" >Relatórios <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Insumos</a></li>
							<li><a href="#">Produtos</a></li>
							<li><a href="#">Clientes</a></li>
							<li><a href="#">Fornecedores</a></li>
							<li><a href="#">Funcionários</a></li>
							<li><a href="{{ URL::to('relatorios/analise') }}">Análise Economico-Financeiro</a></li>
						</ul>
					</li>-->

					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" role="button" >Estoque <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ URL::to('estoque/insumos') }}">Insumos</a></li>
							<li><a href="{{ URL::to('estoque/produtos') }}">Produtos</a></li>
						</ul>
					</li>
					<li><a href="{{ URL::route('compras.index') }}">Entradas</a></li>
					<!--
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" role="button" >
							Entradas
							<span class="caret"></span>
						 </a>
						<ul class="dropdown-menu" role="menu">
								<li><a href="{{ URL::route('compras.index') }}">Listar Entradas</a></li>
								<li><a href="{{ URL::route('compras.create') }}">Insumos</a></li>                            
						</ul>
					</li>
					-->
					<li><a href="{{ URL::route('contaspagar.index') }}">Contas a Pagar</a></li>
					<li><a href="{{ URL::route('contasreceber.index') }}">Contas a Receber</a></li>
					<li><a href="{{ URL::route('caixas.index') }}">Caixas</a></li>
					<li><a href="{{ URL::route('pedidos.index') }}">Vendas</a></li>
					<li><a href="{{ URL::route('producao.index') }}">Ordem de Produção</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<!--
					<?php $parcelas = Parcela::where('data_vencimento', '<', Date('y:m:d', strtotime("+3 days")))->where('data_pagamento', '>', Date('y:m:d', strtotime("+300 days")))->orderBy('data_vencimento')->get(); ?>
					<li class="dropdown">
						<button type="button" class="btn btn-warning navbar-btn dropdown-toggle" data-toggle="dropdown">Parcelas Vencendo!</button>
						<ul class="dropdown-menu" role="menu">
							@foreach ($parcelas as $p)
								<li><a href="{{ URL::route('contas.show', $p->conta_id) }}">{{ $p->data_vencimento }} / {{$p->conta->fornecedor}} / {{ $p->conta->conta }} / R${{ $p->valor }}</a></li>
							@endforeach
						</ul>
					</li>-->
					<li>
						<a href="{{ URL::to('/config') }}"
						 data-toggle="tooltip" data-placement="bottom" title="Configurações">
							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						</a>
					</li>
					<li><a href="{{ Route('logout') }}">Sair</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>

	<div class="container">

@if(Session::has('message'))
	<div class="alert alert-info hidden-print alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<p>
			<span class="glyphicon glyphicon-info-sign"></span>
			{{ Session::get('message') }}
		</p>
	</div>
@endif

@if(Session::has('flash_notice'))
	<div id="flash_notice" class="alert alert-info hidden-print alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		{{ Session::get('flash_notice') }}
	</div>
@endif

@if(Session::has('success'))
	<div class="alert alert-success hidden-print alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<p>
			<span class="glyphicon glyphicon-ok-sign"></span>
			{{ Session::get('success') }}
		</p>
	</div>
@endif

@if($errors->any())
	<div class="alert alert-danger hidden-print alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		@foreach($errors->all() as $error)
		<p>
			<span class="glyphicon glyphicon-alert"></span>
			{{ $error }}
		</p>
		@endforeach
		</div>
@endif

@if (Session::has('fail'))
	<div class="alert alert-danger hidden-print alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<p>
			<span class="glyphicon glyphicon-remove-sign"></span>
			{{ Session::get('fail') }}
		</p>
	</div>
@endif

@if(Session::has('warning'))
	<div class="alert alert-warning hidden-print alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<p>
			<span class="glyphicon glyphicon-warning-sign"></span>
			{{ Session::get('warning') }}
		</p>
	</div>
@endif

		@yield('content')
	
	</div>

	<script>
		if (!Modernizr.inputtypes.date)
		{
			$( "input[type=date]" ).datepicker({
				dateFormat: 'yy-mm-dd'
			});
		}
	</script>	

<!--
	<div class="container-fluid no-print" style="bottom:0;left:0;position:fixed;z-index:-100;">
		<div class="row">
			<div class="col-md-9"></div>
			<div class="col-md-3">
				<img src="<?php echo asset('logo_horizontal.png'); ?>" class="" style="margin-top:2%">
			</div>
		</div>
	</div>--> 
</body>
</html>