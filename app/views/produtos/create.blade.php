@extends('main')


@section('content')
<div ng-controller="AdicionarProduto">
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Adicionar Produto</b>
		</div>
		<div class="panel-body">
			{{ Form::open(array('route' => 'produtos.store')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-4">
							{{ Form::label('nome', 'Nome:') }}
							{{ Form::text('nome', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::label('codigo', 'Código:') }}
							{{ Form::text('codigo', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2">
							{{ Form::label('rendimento','Rendimento:') }}
							{{ Form::text( 'rendimento', 1, array('class' => 'form-control') ) }}
						</div>
						<div class="col-md-3">
							{{ Form::label('unidade_id','Unidade de medida:') }}
							{{ Form::select( 'unidade_id', $unidades, 1, array('class' => 'form-control') ) }}
						</div>		            	
					</div>
					<div class="row">
						<div class="col-md-12">
							{{ Form::label('descricao', 'Descrição:') }}
							{{ Form::text('descricao', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<br />
					<div class="row">
						<div class='col-md-12'>
							<div class="panel panel-default">
								<div class="panel-heading">Composição do Produto</div>
								<div id="listaInsumos" class="panel-body">
									<table class="table">
										<thead>
										<tr class="active">
											<th class="col-md-2">Insumo</th>
											<th class="col-md-7">Quantidade usada</th>
											<th class="col-md-3"></th>
										</tr>
										</thead>
										<tbody>
										<tr ng-repeat="insumoSelecionado in insumosSelecionados">
											<td>[[ insumoSelecionado.nome ]]</td>
											<td class="row">
												<div class="col-md-8">
													<input class="form-control"  type="text" name="insumos[ [[ insumoSelecionado.id ]] ][quantidade]"/>
												</div>
												<div class="col-md-4"> 
													<select class="form-control col-md-1" name="insumos[ [[ insumoSelecionado.id ]] ][unidade]" >
														<option ng-repeat="unit in insumoSelecionado.unidades" value="[[ unit.unidade_id ]]">[[ unit.unidade.nome ]]</option>
													</select>
												</div>
											</td>
											<td><input type="button" class="btn btn-danger btn-sm" ng-click="delInsumo(insumoSelecionado.id)" value="Remover"/></td>
										</tr>
										</tbody>
									</table>
								</div>
								<div class="panel-footer">
									<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#insumoModal"> Adicionar Insumo </button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br/>
				<a href="{{ URL::route('produtos.index') }}" class="btn btn-warning">Fechar</a>
				{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>

	<!-- Modal Insumos -->
	<div class="modal fade" id="insumoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Insumo</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<thead>
						<tr class="active">
							<th>Nome</th>
							<th>Unidades</th>
							<th></th>
						</tr>
					</thead>	
					<tbody>
					<tr ng-repeat="insumo in insumos | filter:search">
						<td>[[insumo.nome]]</td>
						<td>
							<span class="badge" ng-repeat="u in insumo.unidades">&nbsp;[[ u.unidade.abreviatura ]]&nbsp;</span>
						</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getInsumo(insumo.id);" data-dismiss="modal">Selecionar</button></td>
					</tr>	
					</tbody>
				</table>

				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>


@stop