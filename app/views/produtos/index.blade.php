@extends('main')


@section('content')
<div ng-controller="MostrarItem">
	<div class="panel panel-default">
		<div class="panel-heading">
			<b style="font-size:24px;">Produtos</b>
			<div class="pull-right">
				<a href="{{ URL::route('produtos.create') }}" class="btn btn-default">Adicionar Novo</a>
			</div>
		</div>
		<table class="table table-hover" style="font-size:18px;">
			<thead>
				<tr class="active">
					<th class="col-md-1">Código</th>
					<th class="col-md-6">Produto</th>
					<th class="col-md-4">Unidade</th>
					<th class="col-md-1" colspan="2">Opções</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($produtos as $produto)
				<tr data-toggle="modal" data-target="#modalItem" 
				ng-click="getItem('{{ $produto->nome }}','{{ URL::route('produtos.show',['id' => $produto->id]) }}')">
					<td role="button" >{{ $produto->codigo }}</td>
					<td role="button" >{{ $produto->nome }}</td>
					<td role="button" >{{ $produto->unidade->abreviatura }}</td>
					<td>
						<a href="{{ URL::route('produtos.edit', $produto->id) }}" class="btn btn-primary btn-sm">
							<span class="glyphicon glyphicon-pencil"></span>
							Editar
						</a>
					</td>
					<td>
						{{ Form::open(array('method' => 'DELETE', 'route' => array('produtos.destroy', $produto->id))) }}                       
							<button type="submit" class="btn btn-danger btn-sm">
								<span class="glyphicon glyphicon-remove"></span>
									Deletar
							</button>
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		
		<div class="panel-footer" style="text-align:right;"><b>Total Produtos Cadastrados: {{  $produtos->count(); }}</b></div>
	</div>

	<!-- Modal Item -->
	<div class="modal fade" id="modalItem" tabindex="-1" role="dialog" 
	aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="modalLabel">[[ modalTitle ]]</h4>
				</div>
				<div class="modal-body" ng-bind-html="item">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>				
			</div>
		</div>
	</div>
</div>
@stop