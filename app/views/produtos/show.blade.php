<div class="row">
	<div class="col-xs-3">
		<b>Código</b>
	</div>
	<div class="col-xs-9">
		 {{ $produto->codigo }} 
	</div>
</div>
<div class="row">
	<div class="col-xs-3"><b>Rendimento:</b></div>
	<div class="col-xs-9">{{ $produto->rendimento }}&nbsp;{{ $produto->unidade->abreviatura }}</div>
</div>
@if(!empty($produto->descricao))
<div class="row">
	<div class="col-xs-3"><b>Descrição:</b></div>
	<div class="col-xs-9">{{ $produto->descricao }}</div>
</div>
@endif
<br />
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Composição</h3>
	</div>
		<table class="table">
			<tr class="active">
				<th>Insumo</th>
				<th>Quantidade</th>
			</tr>
		@foreach($produto->insumos as $insumo)
			<tr>
				<td>{{ $insumo->nome }}</td>
				<td>{{ $insumo->pivot->qtd }}&nbsp;{{ $unidades->find($insumo->pivot->unidade_id)->abreviatura }}</td>
			</tr>
		@endforeach
		</table>
</div>