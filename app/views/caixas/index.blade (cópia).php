@extends('main')

@section('content')
<div class="row">
	<div class="col-md-3">
		<div class="panel panel-default">
		    <div class="panel-heading">
		        <b>Contas</b>
		    </div>
		    <?php 
		    	//Tentar pegar saldo por conta(categoria)
		    	$contas = Conta::groupBy('conta')->get();
		    ?>
		    <table class="table table-hover">
		    	<tr class="active">
		            <th>Conta</th>
		            <th>Saldo</th>
		        </tr>	        
		        @foreach ($contas as $conta)
		        <tr>
		        	<?php 
		        		$todas_contas = Conta::all();
		        		foreach ($todas_contas as $c) {
		        			$saldo_conta = Parcela::where('conta_id', $conta->id)->where('data_pagamento', '<=', date("Y-m-d",time()))->sum('valor');
		        		}
		        	?>
		        	<td>{{$conta->conta}}</td>
		        	<td>- {{$saldo_conta}}</td></tr>
		        @endforeach
		    </table>
		</div>  
	</div>

	<div class="col-md-9">
		<div class="panel panel-default">
		    <div class="panel-heading">
		        <b>Caixa</b>
		        <div class="pull-right">
		            <a href="{{ URL::route('caixas.create') }}" class="btn btn-default btn-xs">Adicionar Novo</a>
		        </div>
		    </div>
		    
	        <table class="table table-bordered">
	        <tr>
	        	<th>Nome</th>
	        	@foreach ($caixas as $caixa)
	        	<th>{{$caixa->nome}}</th>
	        	@endforeach
	        </tr>
	        <tr>
	        	<th>Observações</th>
	        	@foreach ($caixas as $caixa)
	        	<td>{{$caixa->nota}}</td>
	        	@endforeach
	        </tr>	    
	        <tr>
	        	<th>Saldo</th>
	        	@foreach ($caixas as $caixa)
	        	<td>R$9999,99</td> <!-- Aqui vai um DB:: com um sum das contas ja pagas -->
	        	@endforeach
	        </tr>
		    </table>
		</div>  

		<div class="panel panel-default">
		    <div class="panel-heading">
		        <b>Parcelas Pagas</b>
		    </div>
		    <?php $parcelas = Parcela::orderBy('data_pagamento')->where('conta_id', Session::get('parcela_conta'))->where('data_pagamento', '<=', date("Y-m-d",time()))->get(); ?>
		    <table class="table table-hover">
		        <tr class="active">
		            <th>Data de Pagamento</th>
		            <th>Fornecedor</th>
		            <th>Forma de Pagamento</th>
		            <th>Valor</th>
		        </tr>
		        @foreach ($parcelas as $parcela)
		        <tr>
		            <td>{{ $parcela->data_pagamento }}</td>
		            <td>{{ $parcela->conta->fornecedor }}</td>
		            <td>{{ $parcela->forma_pagamento }}</td>
		            <td>{{ $parcela->valor }}</td>
		        </tr>
		        @endforeach
		    </table>
		    <div class="panel-footer" style="text-align: right;"><b>Total de Contas Pagas: <?php  echo $parcelas->count(); ?></b></div>
		</div>    

	</div>
</div>
@stop