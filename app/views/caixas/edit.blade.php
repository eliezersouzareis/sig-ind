@extends('main')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Editar Caixa</b>
		</div>
		<div class="panel-body">
			{{ Form::model($caixa, array('method' => 'PATCH', 'route' =>
 array('caixas.update', $caixa->id))) }}{{ Form::open(array('route' => 'caixas.store')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-7">
							{{ Form::label('nome', 'Nome:') }}
							{{ Form::text('nome', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-5">
							{{ Form::label('nota', 'Notas/Observações:') }}
							{{ Form::text('nota', null, array('class' => 'form-control')) }}
						</div>
					</div>
				</div>
				<br/>
				<a href="{{ URL::route('caixas.index') }}" class="btn btn-warning">Fechar</a>
				{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>


@stop