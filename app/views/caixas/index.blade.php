@extends('main')

@section('content')
<div class="row">


	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<b>Caixas Disponiveis</b>
				<div class="pull-right">
					<a href="{{ URL::route('caixas.create') }}" class="btn btn-default btn-xs">Adicionar Novo</a>
				</div>
			</div>
			
			<table class="table table-bordered">
			<tr>
				<th>Nome</th>
				@foreach ($caixas as $caixa)
				<th>{{$caixa->nome}}
					<button type="button" onclick="url('{{ URL::route('caixas.edit',$caixa->id)}}')"
						class="btn btn-primary btn-xs pull-right">
						<span class="glyphicon glyphicon-pencil"></span>
						Editar
					</button>
				</th>
				@endforeach
			</tr>
			<tr>
				<th>Observações</th>
				@foreach ($caixas as $caixa)
				<td>{{$caixa->nota}}</td>
				@endforeach
			</tr>	    
			<tr>
				<th>Saldo</th>
				@foreach ($caixas as $caixa)
				<td>
					@if($caixa->saldo < 0)
					<span class="text-danger">
						R$ {{ $caixa->saldo }}
					</span>
					@else
						R$ {{ $caixa->saldo }}
					@endif
				</td> <!-- Aqui vai um DB:: com um sum das contas ja pagas -->
				@endforeach
			</tr>
			<tr>
				<th>
					Saldo Total:
				</th>
				<td>
					<?php $total = 0;?>
					@foreach($caixas as $caixa)
					{{ '';$total +=$caixa->saldo }}
					@endforeach
					R$ {{ $total }}
				</td>
			</tr>
			</table>
		</div>  


	</div>
</div>
@stop