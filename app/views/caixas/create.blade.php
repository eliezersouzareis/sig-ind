@extends('main')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Adicionar Caixa</b>
		</div>
		<div class="panel-body">
			{{ Form::open(array('route' => 'caixas.store')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-7">
							{{ Form::label('nome', 'Nome:') }}
							{{ Form::text('nome', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-5">
							{{ Form::label('nota', 'Notas/Observações:') }}
							{{ Form::text('nota', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<div class="row">
						<div  class="col-md-12">
							{{ Form::label('saldo', 'Saldo:') }}
							<div class="input-group">
								<span class="input-group-addon">
									R$
								</span>
								<input type="text" name="saldo" class="form-control" />
							</div>
						</div>
					</div>
				</div>
				<br/>
				<a href="{{ URL::route('caixas.index') }}" class="btn btn-warning">Fechar</a>
				{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>

@stop