@extends('main')

@section('content')

<div ng-controller="AdicionarFornecedor">
<script>
jQuery(function($){
	$(".telefone-mask").mask("(99)9999-9999");
	$(".cep-mask").mask("99999-999");
});

</script>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4><b>Adicionar Fornecedor</b></h4>
		</div>
		<div class="panel-body">
			{{ Form::open(array('route' => 'fornecedores.store')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-7">
							{{ Form::label('nome', 'Nome:') }}
							{{ Form::text('nome', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-5">
							{{ Form::label('cpf_cnpj', 'CPF ou CNPJ:') }}
							{{ Form::text('cpf_cnpj', null, array('class' => 'form-control cpf_cnpj-mask')) }}
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-4">
							{{ Form::label('telefone_fixo', 'Telefone:') }}
							{{ Form::text('telefone_fixo', null, array('class' => 'form-control telefone-mask')) }}
						</div>
						<div class="col-md-4">
							{{ Form::label('telefone_celular', 'Celular:') }}
							{{ Form::text('telefone_celular', null, array('class' => 'form-control telefone-mask')) }}
						</div>
						<div class="col-md-4">
							{{ Form::label('email', 'Email:') }}
							{{ Form::text('email', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<br/>
					<fieldset>
					<legend> Endereço </legend>
						<div class="row">
							<div class="col-md-9">
								{{ Form::label('endereco[logradouro]', 'Rua:') }}
								{{ Form::text('endereco[logradouro]', null, array('class' => 'form-control')) }}
							</div>
							<div class="col-md-3">
								{{ Form::label('endereco[numero]', 'Numero:') }}
								{{ Form::text('endereco[numero]', null, array('class' => 'form-control')) }}
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								{{ Form::label('endereco[complemento]', 'Complemento:') }}
								{{ Form::text('endereco[complemento]', null, array('class' => 'form-control')) }}
							</div>
							<div class="col-md-6">
								{{ Form::label('endereco[referencia]', 'Referencia:') }}
								{{ Form::text('endereco[referencia]', null, array('class' => 'form-control')) }}
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-md-3">
								{{ Form::label('endereco[bairro]', 'Bairro:') }}
								{{ Form::text('endereco[bairro]', null, array('class' => 'form-control')) }}
							</div>
							<div class="col-md-2">
								{{ Form::label('endereco[cep]', 'CEP:') }}
								{{ Form::text('endereco[cep]', null, array('class' => 'form-control cep-mask')) }}    
							</div>
							<div class="col-md-5">
								{{ Form::label('endereco[cidade]', 'Cidade:') }}
								
								{{ Form::text('endereco[cidade]', null, array('class' => 'form-control')) }}
							</div>
							<div class="col-md-2">
								{{ Form::label('endereco[estado]', 'Estado:') }}
								{{ Form::text('endereco[estado]', null, array('class' => 'form-control')) }}
							</div>
						</div>
					</fieldset>
					<br/>
					<fieldset>
						<legend>Insumos Fornecidos:</legend>
						<div class="row">
							<div class="col-md-8">
								<table class="table">
									<thead>
										<tr>
											<th class="col-md-2">Insumo</th>
											<th class="col-md-6">Valor</th>
											<th class="col-md-2"></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="insumo in insumosSelecionados">
											<td>[[ insumo.nome ]]</td>
											<td>
												<div class="input-group">
													<span class="input-group-addon">
														R$
													</span>
													<input type="text" name="insumos[ [[ insumo.id ]] ]" class="form-control" value="0.00"/>
													<span class="input-group-addon">
														[[ insumo.unidade.abreviatura ]]
													</span>
												</div>
											</td>
											<td>
												<button type="button" class="btn btn-danger btn-sm" ng-click="delInsumo(insumo.id)" >
													<span class="glyphicon glyphicon-remove"></span>
													Remover
												</button>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#insumoModal">
							<span class="glyphicon glyphicon-plus"></span>
							Adicionar Insumo
						</button>
					</fieldset>
				</div>
				<br/>
				<a href="{{ URL::route('fornecedores.index') }}" class="btn btn-warning">Fechar</a>
				{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>

	<!-- Modal Insumos -->
	<div class="modal fade" id="insumoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Insumo</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<thead>
						<tr class="active">
							<th>Nome</th>
							<th>Unidade</th>
							<th></th>
						</tr>
					</thead>	
					<tbody>
					<tr ng-repeat="insumo in insumos |filter:search">
						<td>[[insumo.nome]]</td>
						<td>
							<span class="badge">&nbsp;[[ insumo.unidade.abreviatura ]]&nbsp;</span>
						</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getInsumo(insumo.id);" data-dismiss="modal">Selecionar</button></td>
					</tr>	
					</tbody>
				</table>

				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>

@stop