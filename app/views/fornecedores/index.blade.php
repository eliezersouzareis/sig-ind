@extends('main')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Fornecedores</b>
			<div class="pull-right">
				<a href="{{ URL::route('fornecedores.create') }}" class="btn btn-default btn-xs">Adicionar Novo</a>
			</div>
		</div>
		<div class="panel-body">
			<input type="search" class="form-control input-sm" placeholder="Buscar..."> 
		</div>
		<table class="table table-hover">
			<thead>
				<tr class="active">
					<th class="col-md-4">Nome</th>
					<th class="col-md-1">CPF/CNPJ</th>
					<th class="col-md-1">Telefone</th>
					<th class="col-md-2">Email</th>
					<th class="col-md-3">Endereço</th>
					<th class="col-md-1" colspan="2">Opções</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($fornecedores as $fornecedor)
				<tr>
					<td>{{ $fornecedor->nome }}</td>
					<td>{{ $fornecedor->cpf_cnpj }}</td>
					<td>{{ $fornecedor->telefone_fixo }}</td>
					<td>{{ $fornecedor->email }}</td>
					<td>{{ $fornecedor->endereco->logradouro }}, {{ $fornecedor->endereco->numero }} - {{ $fornecedor->endereco->cidade }}</td>
					<td width="50">
						<a href="{{ URL::route('fornecedores.edit', $fornecedor->id) }}" class="btn btn-primary btn-sm">
							<span class="glyphicon glyphicon-pencil"></span>
							Editar
						</a>
					</td>
					<td width="60">
						{{ Form::open(array('method' => 'DELETE', 'route' => array('fornecedores.destroy', $fornecedor->id))) }}                       
							<button type="submit" class="btn btn-danger btn-sm">
								<span class="glyphicon glyphicon-remove"></span>
									Deletar
							</button>
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		
		<div class="panel-footer" style="text-align: right;" ><b>Total Fornecedores Cadastrados: {{ Fornecedor::all()->count() }}</b></div>
	</div>    
@stop