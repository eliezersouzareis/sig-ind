@extends('main')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Finalizar Ordem de Produção</b>
		</div>
		<div class="panel-body">
			{{ Form::model($ordemProducao, array('method' => 'PATCH',
			 'route' => array('producao.finalizar', $ordemProducao->id)
			 )) }}
			{{ Form::open(array( 'route' => 'producao.store' )) }}
			{{ Form::text('produto_id', null, array('class' => 'form-control hidden')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">

							{{ Form::label('produto', 'Produto:') }}
							<div class="input-group">
								{{ Form::text('produto', $ordemProducao->produto->nome, array('class' => 'form-control',
								 'disabled')) }}
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-ban-circle"></span>
								</span>
							</div>
						</div>
						<div class="col-md-3">
							{{ Form::label('qtd_inicial', 'Quantidade Inicial') }}
							<div class="input-group">
								{{ Form::text( 'qtd_inicial', null, array('class' => 'form-control', 'disabled' ) ) }}
								<span class="input-group-addon">
									{{ $ordemProducao->produto->unidade->abreviatura }}
								</span>
							</div>
						</div>		            	
						<div class="col-md-3">
							{{ Form::label('qtd_final', 'Quantidade Final') }}
							<div class="input-group">
								{{ Form::text( 'qtd_final', null, array('class' => 'form-control' ) ) }}
								<span class="input-group-addon">
										{{ $ordemProducao->produto->unidade->abreviatura }}
									</span>
								</div>
							</div>
					</div>
				</div>
				<br/>
				<a href="{{ URL::route('producao.index') }}" class="btn btn-warning">Fechar</a>
				{{ Form::submit('Concluir', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>

@stop