<div class="row">
	<div class="col-xs-3">
		<b>Criado em:</b>
	</div>
	<div class="col-xs-9">
		{{ $ordemProducao->data }}
	</div>
</div>
<div class="row">
	<div class="col-xs-3">
		<b>Finalizado em:</b>
	</div>
	<div class="col-xs-9">
		{{ $ordemProducao->data_fim }}
	</div>
</div>
<br />
<div class="row">
	<div class="col-xs-3">
		<b>Produto:</b>
	</div>
	<div class="col-xs-9">
		{{ $produto->nome }}
	</div>
</div>
<div class="row">
	<div class="col-xs-4">
		<b>Quantidade Solicitada</b>
	</div>
	<div class="col-xs-2">
		{{ $ordemProducao->qtd_inicial }} {{ $produto->unidade->abreviatura }}
	</div>
	<div class="col-xs-4">
		<b>Quantidade Produzida:</b>
	</div>
	<div class="col-xs-2">
		{{ $ordemProducao->qtd_final }} {{ $produto->unidade->abreviatura }}
	</div>
</div>

