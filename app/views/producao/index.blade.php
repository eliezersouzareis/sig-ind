@extends('main')

@section('content')
<div ng-controller="MostrarItem">
	<div class="panel panel-default">
		<div class="panel-heading">
			<b style="font-size:24px;">Ordens de Produção</b>
			<div class="pull-right">
				<a href="{{ URL::route('producao.create') }}" class="btn btn-default">
					Adicionar Nova
				</a>
			</div>
		</div>

			<div class="form-inline text-center">
				{{ Form::open(array('url'=> '#','method'=>'GET')) }}
					Mês:
					<div class="input-group col-xs-2">
						<span class="input-group-btn">
							<button class="btn btn-default" type="submit" ng-click="mes = mes-1">
								<span class="glyphicon glyphicon-chevron-left"></span>
							</button>
						</span>
						<input type="text" name="mes" class="form-control text-center" ng-model="mes" ng-init="mes={{ $data['month'] }}" readonly/>
						<span class="input-group-btn">
							<button class="btn btn-default" type="submit" ng-click="mes = mes+1">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</button>
						</span>
					</div>
					Ano:
					<div class="input-group col-xs-2">
						<span class="input-group-btn">
							<button class="btn btn-default" type="submit" ng-click="ano = ano-1">
								<span class="glyphicon glyphicon-chevron-left"></span>
							</button>
						</span>
						<input type="text" name="ano" class="form-control text-center" ng-model="ano" ng-init="ano={{ $data['year'] }}" readonly/>
						<span class="input-group-btn">
							<button class="btn btn-default" type="submit" ng-click="ano = ano+1">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</button>
						</span>
					</div>
					{{ Form::close() }}
		</div>

		<table class="table table-hover" style="font-size:18px;">
			<thead>
				<tr class="active">
					<th class="col-md-1">Código</th>
					<th class="col-md-4">Produto</th>
					<th class="col-md-1">Solicitado</th>
					<th class="col-md-1">Produzido</th>
					<th class="col-md-1">Perda</th>
					<th class="col-md-2"></th>
				</tr>
			</thead>
			<tbody>
			@foreach ($ordensProducao as $ordemProducao)
				<tr data-toggle="modal" data-target="#modalItem" 
				ng-click="getItem('{{ $ordemProducao->data }}','{{ URL::route('producao.show',['id' => $ordemProducao->id]) }}')"
				@if($ordemProducao->concluido)
					class="text-muted active"
					@endif
				>
					<td role="button" >{{ $ordemProducao->codigo }}</td>
					<td role="button" >{{ $ordemProducao->produto->nome }}</td>
					<td role="button" >{{ $ordemProducao->qtd_inicial." ".$ordemProducao->produto->unidade->abreviatura }}</td>
					<td role="button" >{{ $ordemProducao->qtd_final." ".$ordemProducao->produto->unidade->abreviatura }}</td>
					<td role="button" >
						@if($ordemProducao->concluido && ($ordemProducao->qtd_inicial != 0) )
							{{ (($ordemProducao->qtd_final/$ordemProducao->qtd_inicial-1)*100)."%" }}
						@else
							--
						@endif
					</td>
					<td>
					@if(!$ordemProducao->concluido)
						<a href="{{URL::route('producao.concluir',$ordemProducao->id)}}" class="btn btn-warning btn-sm">
							<span class="glyphicon glyphicon-ok"></span>
						</a>
						<a href="{{URL::route('producao.edit',$ordemProducao->id)}}" class="btn btn-primary btn-sm">
							<span class="glyphicon glyphicon-pencil"></span>
							Editar
						</a>
						{{ Form::open(array('method' => 'DELETE', 'route' => array('producao.destroy', $ordemProducao->id), 'class' => 'force-inline-block')) }}                       
							<button type="submit" class="btn btn-danger  btn-sm">
								<span class="glyphicon glyphicon-remove"></span>
								Deletar
							</button>
						{{ Form::close() }}
					@else
						<button type="button" class="btn btn-default btn-sm" disabled>	
							<span class="glyphicon glyphicon-ok"></span>
						</button>
						<a href="{{URL::route('producao.edit',$ordemProducao->id)}}" class="btn btn-primary btn-sm">
							<span class="glyphicon glyphicon-pencil"></span>
							Editar
						</a>
						{{ Form::open(array('method' => 'DELETE', 'route' => array('producao.destroy', $ordemProducao->id), 'class' => 'force-inline-block')) }}                       
							<button type="submit" class="btn btn-danger  btn-sm">
								<span class="glyphicon glyphicon-remove"></span>
								Deletar
							</button>
						{{ Form::close() }}
					@endif
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		
		<div class="panel-footer" style="text-align:right;"><b>Total Ordens de Produção nesse mês: {{  $monthCount }}</b></div>
	</div>

	<div class="row">
		{{ Form::open(array('url' => '#', 'method' => 'GET')) }}
			<input type="text" name='ano' ng-model="ano" class="hidden"/>
			<input type="text" name='mes' ng-model="mes" class="hidden"/>
			<div class="input-group col-xs-2 col-xs-offset-5">
				<span class="input-group-btn">
					<button class="btn btn-default" type="submit" ng-click="page = page-1">
						<span class="glyphicon glyphicon-chevron-left"></span>
					</button>
				</span>
				<input type="text" name="pag" class="form-control text-center" ng-model="page" ng-init="page={{ $pgNum }}" readonly/>
				<span class="input-group-btn">
					<button class="btn btn-default" type="submit" ng-click="page = page+1">
						<span class="glyphicon glyphicon-chevron-right"></span>
					</button>
				</span>
			</div>
		{{ Form::close() }}
	</div>
	<br />

	<!-- Modal Item -->
	<div class="modal fade" id="modalItem" tabindex="-1" role="dialog" 
	aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="modalLabel">[[ modalTitle ]]</h4>
				</div>
				<div class="modal-body" ng-bind-html="item">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>				
			</div>
		</div>
	</div>
</div>

@stop