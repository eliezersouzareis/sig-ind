
@extends('main')

@section('content')
<div ng-controller="SelecionarProduto" ng-init="produtoSelecionado={id:{{$ordemProducao->produto_id}}, 
	nome:'{{$ordemProducao->produto->nome}}', unidade:{ abreviatura: '{{$ordemProducao->produto->unidade->abreviatura}}' } }">
	<script type="text/javascript">

	</script>
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Editar Ordem de Produção</b>
		</div>
		<div class="panel-body">
			{{ Form::model($ordemProducao, array('method' => 'PATCH',
			 'route' => array('producao.update', $ordemProducao->id)
			 )) }}
			{{ Form::open(array( 'route' => 'producao.store' )) }}
			{{ Form::text('produto_id', null, array('id'=>'produto_id', 'class' => 'form-control hidden', 'ng-model' => 'produtoSelecionado.id')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							{{ Form::label('produto', 'Produto:') }}
							@if(!$ordemProducao->concluido)
							<div class="input-group" data-toggle="modal" data-target="#produtoModal">
								{{ Form::text('produto', null,array('class' => 'form-control',
								 'readonly', 'role' => 'button', 'ng-model' => 'produtoSelecionado.nome')) }}
								<div class="input-group-addon" role="button">
									<span class="glyphicon glyphicon-plus"></span>
								</div>
							</div>
							@else
							<div class="input-group">
								{{ Form::text('produto', $ordemProducao->produto->nome,array('class' => 'form-control',
								 'disabled')) }}
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-ban-circle"></span>
								</div>
							</div>		
							@endif
						</div>
						<div class="col-md-3">
							{{ Form::label('qtd_inicial', 'Quantidade Inicial') }}
							<div class="input-group">
								@if(!$ordemProducao->concluido)
									{{ Form::text( 'qtd_inicial', null, array('class' => 'form-control' ) ) }}
									<span class="input-group-addon" ng-bind="produtoSelecionado.unidade.abreviatura">
									</span>
								@else
									{{ Form::text( 'qtd_inicial', null, array('class' => 'form-control', 'disabled' ) ) }}
								<span class="input-group-addon" >
										{{ $ordemProducao->produto->unidade->abreviatura }}
								</span>
								@endif
							</div>
						</div>		            	
						<div class="col-md-3">
							@if($ordemProducao->concluido)
								{{ Form::label('qtd_final', 'Quantidade Final') }}
								<div class="input-group">
									{{ Form::text( 'qtd_final', null, array('class' => 'form-control' ) ) }}
									<span class="input-group-addon">
										{{ $ordemProducao->produto->unidade->abreviatura }}
									</span>
								</div>
							@endif
						</div>
					</div>
				</div>
				<br/>
				<a href="{{ URL::route('producao.index') }}" class="btn btn-warning">Fechar</a>
				{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>
	<div class="modal fade" id="produtoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Produto</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<tr class="active">
						<th>Nome</th>
						<th>Unidade</th>
						<th></th>
					</tr>

					<tr ng-repeat="produto in produtos | filter:search">
						<td>[[produto.nome]]</td>
						<td>[[produto.unidade.nome]]</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getProduto(produto.id, produto.nome);" data-dismiss="modal">Selecionar</button></td>
					</tr>
				</table>

				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>

@stop