@extends('main')

@section('content')
<button type="button" class="btn btn-default btn-sm" onclick="url('{{ URL::route('clientes.index') }}')">
	<span class="glyphicon glyphicon-chevron-left"></span>
	Voltar
</button>
<br />

<div class="panel panel-default">
	<div class="panel-heading">
		<b>Visualizar Cliente</b>
	</div>
	<div class="panel-body">
		<h4 class="page-header" style="margin-top:1.5em;"> Dados </h4>
		<div class="row">
			<div class="col-md-4 ">
				<b>Nome: </b><br />
				{{ $cliente->nome }}
			</div>
			<div class="col-md-4 ">
				<b>Nome Fantasia </b><br />
				{{ $cliente->nome_fantasia }}
			</div>
			<div class="col-md-4 ">
				<b>CPF/CNPJ: </b><br />
				{{ $cliente->cpf_cnpj }}
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 ">
				<b>Telefone: </b><br />
				{{ $cliente->telefone_fixo }}
			</div>
			<div class="col-md-4 ">
				<b>Celular: </b><br />
				{{ $cliente->telefone_celular }}
			</div>
			<div class="col-md-4 ">
				<b>Email: </b><br />
				{{ $cliente->email }}
			</div>
		</div>
		<h4 class="page-header"> Endereço Principal <small>Matriz</small>
			@if($cliente->endereco_id)
			<span class="pull-right col-md-1">
				<a href="{{ URL::route('clientes.enderecos.edit', [$cliente->id,$cliente->matriz->id]) }}"
					class="btn btn-primary btn-xs btn-block">
					<span class="glyphicon glyphicon-pencil"></span>
					Editar
				</a>
			</span>
			@else
			<span class="pull-right col-md-1">
				<a href="{{ URL::route('clientes.enderecos.create', $cliente->id) }}"
					class="btn btn-primary btn-xs btn-block">
					<span class="glyphicon glyphicon-plus"></span>
					Adicionar
				</a>
			</span>
			@endif
		</h4>
		@if($cliente->endereco_id)
		<div class="row">
			<div class="col-md-8 ">
				<b>Rua: </b><br />
				{{ $cliente->matriz->logradouro }}
			</div>
			<div class="col-md-4 ">
				<b>Número: </b><br />
				{{ $cliente->matriz->numero }}
			</div>
			<div class="col-md-6 ">
				<b>Complemento: </b><br />
				{{ $cliente->matriz->complemento }}
			</div>
			<div class="col-md-6 ">
				<b>Referencia </b><br />
				{{ $cliente->matriz->referencia }}
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 ">
				<b>Bairro: </b><br />
				{{ $cliente->matriz->bairro }}
			</div>
			<div class="col-md-2 ">
				<b>CEP: </b><br />
				{{ $cliente->matriz->cep }}
			</div>
			<div class="col-md-5 ">
				<b>Cidade: </b><br />
				{{ $cliente->matriz->cidade }}
			</div>
			<div class="col-md-2 ">
				<b>Estado: </b><br />
				{{ $cliente->matriz->estado }}
			</div>
		</div>
		@endif
		<h4 class="page-header"> Outros Endereços <small>Filiais</small>
			<span class="pull-right col-md-1">
				<a href="{{ URL::route('clientes.enderecos.create',$cliente->id) }}"
					class="btn btn-primary btn-xs">
					<span class="glyphicon glyphicon-plus"></span>
					Cadastrar
				</a>
			</span>
		</h4>

		@foreach( $cliente->filiais as $filial)
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-5">
					<b>Nome: </b><br />
					{{ $filial->nome }}
				</div>
				<div class="col-md-3">
					<b>Contato: </b><br />
					{{ $filial->contato }}
				</div>
				<div class="col-md-3">
					<b>Telefone: </b><br />
					{{ $filial->telefone }}
				</div>
				<div class="col-md-1">
					<a href="{{ URL::route('clientes.enderecos.edit', [$cliente->id,$filial->id]) }}"
						class="btn btn-primary btn-xs btn-block">
						<span class="glyphicon glyphicon-pencil"></span>
						Editar
					</a>
					{{ Form::open(array('method'=> 'DELETE','route'=>array('clientes.enderecos.destroy',$cliente->id,$filial->id)
						)) }}
					<button type="submit"
						class="btn btn-danger btn-xs btn-block">
						<span class="glyphicon glyphicon-remove"></span>
						Deletar
					</button>
					{{ Form::close() }}
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<b>Rua: </b><br />
					{{ $filial->logradouro }}
				</div>
				<div class="col-md-4">
					<b>Número: </b><br />
					{{ $filial->numero }}
				</div>
				<div class="col-md-6">
					<b>Complemento: </b><br />
					{{ $filial->complemento }}
				</div>
				<div class="col-md-6">
					<b>Referencia: </b><br />
					{{ $filial->referencia }}
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<b>Bairro: </b><br />
					{{ $filial->bairro }}
				</div>
				<div class="col-md-2">
					<b>CEP: </b><br />
					{{ $filial->cep }}
				</div>
				<div class="col-md-5">
					<b>Cidade: </b><br />
					{{ $filial->cidade }}
				</div>
				<div class="col-md-2">
					<b>Estado: </b><br />
					{{ $filial->estado }}
				</div>
			</div>
		</div>
			<br />
		@endforeach
	</div>
</div>


@stop