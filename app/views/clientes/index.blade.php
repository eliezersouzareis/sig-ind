@extends('main')

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Clientes</b>
			<div class="pull-right">
				<a href="{{ URL::route('clientes.create') }}" class="btn btn-default btn-xs">Adicionar Novo</a>
			</div>
		</div>
		<div class="panel-body">
			<input type="search" class="form-control input-sm" placeholder="Buscar..."> 
		</div>
		<table class="table table-hover">
			<thead>
				<tr class="active">
					<th class="col-md-3">Nome</th>
					<th class="col-md-1">CPF/CNPJ</th>
					<th class="col-md-1">Telefone</th>
					<th class="col-md-1">Celular</th>
					<th class="col-md-4">Endereço</th>
					<th class="col-md-2" colspan="3">Opções</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($clientes as $cliente)
			<tr>	        
				<td>
					{{ $cliente->nome }}</td>
				<td>
					{{ $cliente->cpf_cnpj }}</td>
				<td>
					{{ $cliente->telefone_fixo }}</td>
				<td>
					{{ $cliente->telefone_celular }}</td>
				<td>
					{{ ($cliente->endereco_id)?$cliente->matriz->logradouro.", ".$cliente->matriz->numero."/".$cliente->matriz->cidade." - ".$cliente->matriz->estado:"Não há um endereço principal" }}
				</td>
				<td>
					<a href="{{ URL::route('clientes.show', $cliente->id) }}" class="btn btn-success btn-sm">
						<span class="glyphicon glyphicon-eye-open"></span>
						Ver
					</a>
				</td>
				<td>
					<a href="{{ URL::route('clientes.edit', $cliente->id) }}" class="btn btn-primary btn-sm">
						<span class="glyphicon glyphicon-pencil"></span>
						Editar
					</a>
				</td>
				<td>
					{{ Form::open(array('method' => 'DELETE', 'route' => array('clientes.destroy', $cliente->id))) }}                       
						<button type="submit" class="btn btn-danger btn-sm">
							<span class="glyphicon glyphicon-remove"></span>
								Deletar
						</button>
					{{ Form::close() }}
				</td>
			</tr>
			@endforeach
			</tbody>
		</table>
		
		<div class="panel-footer" style="text-align: right;" ><b>Total de Clientes Cadastrados: {{ $clientes->count() }}</b></div>
	</div>    
@stop