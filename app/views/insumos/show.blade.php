<div class="row">
	<div class="col-xs-4"><b>Unidade Primária</b></div>
	<div class="col-xs-8">{{ $insumo->unidadePrimaria->nome }}</div>
</div>
<br />
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Tabela de Conversão</h3>
	</div>
		<table class="table">
			<tr class="active">
				<th>Unidade</th>
				<th>Quantidade</th>
			</tr>
		@foreach($insumo->unidades as $row)
			<tr>
				<td>{{ $row->nome }}</td>
				<td>{{ $row->pivot->qtd }}&nbsp;{{ $row->abreviatura }}</td>
			</tr>
		@endforeach
		</table>
</div>