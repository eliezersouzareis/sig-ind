@extends('main')

@section('content')
<div ng-controller="MostrarItem">
	<div class="panel panel-default">
		<div class="panel-heading">
			<b style="font-size:24px;">Insumos</b>
			<div class="pull-right">
				<a href="{{ URL::route('insumos.create') }}" class="btn btn-default">Adicionar Novo</a>
			</div>
		</div>
		<table class="table table-hover" style="font-size:18px;">
			<thead>
			<tr class="active">
				<th class="col-md-8">Insumo</th>
				<th class="col-md-3">Unidade de Medida</th>
				<th class="col-md-1" colspan="2">Opções</th>
			</tr>
			</thead>
			<tbody>
			@foreach ($insumos as $insumo)
			<tr data-toggle="modal" data-target="#modalItem"
				ng-click="getItem('{{ $insumo->nome }}','{{ URL::route('insumos.show',['id' => $insumo->id]) }}')">
				<td role="button">{{ $insumo->nome }}</td>
				<td role="button">{{ $insumo->unidadePrimaria->nome }}</td>
				<td>
					<a href="{{ URL::route('insumos.edit',$insumo->id) }}" class="btn btn-primary btn-sm">
						<span class="glyphicon glyphicon-pencil"></span>
						Editar
					</a>
				</td>
				<td>
					{{ Form::open(array('method' => 'DELETE', 'route' => array('insumos.destroy', $insumo->id))) }}                       
						<button type="submit" class="btn btn-danger btn-sm">
							<span class="glyphicon glyphicon-remove"></span>
							Deletar
						</button>
					{{ Form::close() }}
				</td>
			</tr>
			@endforeach
			</tbody>
		</table>
		
		<div class="panel-footer" style="text-align:right;"><b>Total de Insumos Cadastrados: <?php  echo DB::table('insumos')->count(); ?></b></div>
	</div>

	<!-- Modal Item -->
	<div class="modal fade" id="modalItem" tabindex="-1" role="dialog" 
	aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="modalLabel">[[ modalTitle ]]</h4>
				</div>
				<div class="modal-body" ng-bind-html="item">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>				
			</div>
		</div>
	</div>
</div>	

@stop