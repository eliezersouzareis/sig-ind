@extends('main')

@section('content')
<div ng-controller="AdicionarInsumo">
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Adicionar Insumo</b>
		</div>
		<div class="panel-body">
			{{ Form::open(array('route' => 'insumos.store','name' => 'insForm', 'ng-submit' => 'formValidate()', 'novalidate')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-12 form-group" ng-class="{ 'has-error': insForm.nome.$invalid && !insForm.nome.$pristine }">
							<label for="nome" class="control-label">Nome:</label>
							<input type="text" name="nome" class="form-control" ng-minlength="1" ng-model="nome" required/>
							<p ng-show="insForm.nome.$invalid && !insForm.nome.$pristine" class="help-block">Campo obrigatório</p>
						</div>
					</div>
					<input type="text" name="unidade_primaria" class="hidden" ng-model="unidadePrimaria.id" ng-minlength="1" readonly required/>
					<br />				
					<div class="row">
						<div class='col-md-12'>
							<div class="panel panel-default">
								<div class="panel-heading">Tabela de Conversão</div>
								<div id="listaInsumos" class="panel-body">
									<table class="table">
										<tbody>
										<tr class="active">
											<th>Unidade</th>
											<th>Corresponde</th>
											<th></th>
										</tr>
										<tr ng-show="unidadePrimaria" class="active">
											<td ng-bind="unidadePrimaria.nome"></td>
											<td>
												<input class="form-control hidden" type="text" name="unidades[ 0 ][unidade_id]"  value="[[unidadePrimaria.id]]" readonly/>
												<div class="input-group">
													<input class="form-control"  type="text" name="unidades[ 0 ][qtd]" value="[[unidadePrimaria.qtd]]" readonly/>
													<div class="input-group-addon" ng-bind="unidadePrimaria.abreviatura"></div>
												</div>
											</td>
											<td><button type="button" class="btn btn-danger btn-sm" ng-click="delPrimaria()">Remover</button></td>
										</tr>
										<tr ng-repeat="unidadeSelecionada in unidadesSelecionadas">
											<td>[[ unidadeSelecionada.nome ]]</td>
											<td>
												<input class="form-control hidden" type="text" name="unidades[ [[unidadeSelecionada.id]] ][unidade_id]"  value="[[unidadeSelecionada.id]]" readonly/>
												<div class="input-group">
													<input class="form-control" type="text" name="unidades[ [[unidadeSelecionada.id]] ][qtd]" ng-model="unidadeSelecionada.qtd" ng-minlength="1" required/>
													<div class="input-group-addon">[[ unidadeSelecionada.abreviatura ]]</div>
												</div>
											</td>
											<td><button type="button" class="btn btn-danger btn-sm" ng-click="delUnidade(unidadeSelecionada.id)">Remover</button></td>
										</tr>
										</tbody>
									</table>
								</div>
								<div class="panel-footer">
									<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#unidadeModal" ng-class="{ 'btn-danger': !insForm.unidade_primaria.$valid }">
										Selecionar Unidade <span ng-show="!unidadePrimaria"> Primária </span>
									</button>
									<span ng-show="!unidadePrimaria && !insForm.nome.$pristine" class="text-danger"> Seleção obrigatória</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br/>
				<a href="{{ URL::route('insumos.index') }}" class="btn btn-warning">Fechar</a>
				<button type="submit" class="btn btn-primary" ng-disabled="insForm.$invalid">Salvar</button>
			</form>
		</div>
	</div>
	<div class="modal fade" id="unidadeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Unidade</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
				   <thead>
					<tr class="active">
						<th>Unidade</th>
						<th>Abreviatura</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
					<tr ng-repeat="unidade in unidades | filter:search">
						<td>[[unidade.nome]]</td>
						<td>[[unidade.abreviatura]]</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getUnidade(unidade.id);" data-dismiss="modal">Selecionar</button></td>
					</tr>
					</tbody>
				</table>

				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>

@stop