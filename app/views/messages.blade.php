@if(Session::has('message'))
	<div class="alert alert-info hidden-print alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<p>
			<span class="glyphicon glyphicon-info-sign"></span>
			{{ Session::get('message') }}
		</p>
	</div>
@endif

@if(Session::has('flash_notice'))
	<div id="flash_notice" class="alert alert-info hidden-print alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		{{ Session::get('flash_notice') }}
	</div>
@endif

@if(Session::has('success'))
	<div class="alert alert-success hidden-print alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<p>
			<span class="glyphicon glyphicon-ok-sign"></span>
			{{ Session::get('success') }}
		</p>
	</div>
@endif

@if($errors->any())
	<div class="alert alert-danger hidden-print alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		@foreach($errors->all() as $error)
		<p>
			<span class="glyphicon glyphicon-alert"></span>
			{{ $error }}
		</p>
		@endforeach
		</div>
@endif

@if (Session::has('fail'))
	<div class="alert alert-danger hidden-print alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<p>
			<span class="glyphicon glyphicon-remove-sign"></span>
			{{ Session::get('fail') }}
		</p>
	</div>
@endif

@if(Session::has('warning'))
	<div class="alert alert-warning hidden-print alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<p>
			<span class="glyphicon glyphicon-warning-sign"></span>
			{{ Session::get('warning') }}
		</p>
	</div>
@endif