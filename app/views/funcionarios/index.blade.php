@extends('main')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Funcionarios</b>
			<div class="pull-right">
				<a href="{{ URL::route('funcionarios.create') }}" class="btn btn-default btn-xs">Adicionar Novo</a>
			</div>
		</div>
		<div class="panel-body">
			<input type="search" class="form-control input-sm" placeholder="Buscar..."> 
		</div>
		<table class="table table-hover">
			<thead>
				<tr class="active">
					<th>Nome</th>
					<th>CPF</th>
					<th>Telefone</th>
					<th>Celular</th>
					<th>Cargo</th>
					<th colspan="2">Opções</th>
				</tr>	
			</thead>
			<tbody>
			@foreach ($funcionarios as $funcionario)
				<tr>
					<td>{{ $funcionario->nome }}</td>
					<td>{{ $funcionario->cpf }}</td>
					<td>{{ $funcionario->telefone_fixo }}</td>
					<td>{{ $funcionario->telefone_celular }}</td>
					<td>{{ DB::table('cargos')->where('id', $funcionario->cargo_id)->pluck('nome'); }}</td>
					<td width="50">
						<a href="{{ URL::route('funcionarios.edit', $funcionario->id) }}" class="btn btn-primary btn-sm">
							<span class="glyphicon glyphicon-pencil"></span>
							Editar
						</a>
					</td>
					<td width="60">
						{{ Form::open(array('method' => 'DELETE', 'route' => array('funcionarios.destroy', $funcionario->id))) }}                       
							<button type="submit" class="btn btn-danger btn-sm">
								<span class="glyphicon glyphicon-remove"></span>
									Deletar
							</button>
						{{ Form::close() }}
					</td>
				</tr>	
			@endforeach
			</tbody>
		</table>
		
		<div class="panel-footer" style="text-align: right;" ><b>Total Funcionarios Cadastrados: {{ $funcionarios->count() }}</b></div>
	</div>    
@stop