@extends('main')

@section('content')

<script>
jQuery(function($){
	$(".telefone-mask").mask("(99)9999-9999");
	$(".cep-mask").mask("99999-999");
	$(".cpf-mask").mask("999.999.999-99");
});

</script>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h4><b>Adicionar Funcionario</b></h4>
		</div>
		<div class="panel-body">
			{{ Form::open(array('route' => 'funcionarios.store')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-7">
							{{ Form::label('nome', 'Nome:') }}
							{{ Form::text('nome', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-5">
							{{ Form::label('cpf', 'CPF:') }}
							{{ Form::text('cpf', null, array('class' => 'form-control cpf-mask')) }}
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-4">
							{{ Form::label('telefone_fixo', 'Telefone:') }}
							{{ Form::text('telefone_fixo', null, array('class' => 'form-control telefone-mask')) }}
						</div>
						<div class="col-md-4">
							{{ Form::label('telefone_celular', 'Celular:') }}
							{{ Form::text('telefone_celular', null, array('class' => 'form-control telefone-mask')) }}
						</div>
						<div class="col-md-4">
							{{ Form::label('email', 'Email:') }}
							{{ Form::text('email', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-3">
							{{ Form::label('cargo_id', 'Cargo:') }}
							{{ Form::select('cargo_id', $cargos_options, null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2" id="div_caminhao" style="display:none;">
							{{ Form::label('veiculo_id', 'Caminhao:') }}
							{{ Form::select('veiculo_id', $veiculo_options, 'motorista', array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2"> 
							{{ Form::label('data_admissao', 'Data de admissão:') }}
							{{ Form::input('date','data_admissao', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2">
							{{ Form::label('salario', 'Salário (R$):') }}
							{{ Form::text('salario', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4">
							{{ Form::label('observacores', 'Observações:') }}
							{{ Form::text('observacoes', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<br/>
				<fieldset>
					<legend> Endereço </legend>
					<div class="row">
						<div class="col-md-9">
							{{ Form::label('endereco[logradouro]', 'Rua:') }}
							{{ Form::text('endereco[logradouro]', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::label('endereco[numero]', 'Numero:') }}
							{{ Form::text('endereco[numero]', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							{{ Form::label('endereco[complemento]', 'Complemento:') }}
							{{ Form::text('endereco[complemento]', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6">
							{{ Form::label('endereco[referencia]', 'Referencia:') }}
							{{ Form::text('endereco[referencia]', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-3">
							{{ Form::label('endereco[bairro]', 'Bairro:') }}
							{{ Form::text('endereco[bairro]', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2">
							{{ Form::label('endereco[cep]', 'CEP:') }}
							{{ Form::text('endereco[cep]', null, array('class' => 'form-control cep-mask')) }}    
						</div>
						<div class="col-md-5">
							{{ Form::label('endereco[cidade]', 'Cidade:') }}
							{{ Form::text('endereco[cidade]', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2">
							{{ Form::label('endereco[estado]', 'Estado:') }}
							{{ Form::text('endereco[estado]', null, array('class' => 'form-control')) }}
						</div>
					</div>
				</fieldset>
				</div>
				<br/>
				<a href="{{ URL::route('funcionarios.index') }}" class="btn btn-warning">Fechar</a>
				{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>

@stop