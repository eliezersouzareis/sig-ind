@extends('main')

@section('content')
	<div class="panel panel-default" style="font-size:18px">
		<div class="panel-heading">
			<b>Veículos</b>
			<div class="pull-right">
				<a href="{{ URL::route('veiculos.create') }}" class="btn btn-default btn-xs">Adicionar Novo</a>
			</div>
		</div>
		<table class="table table-hover">
			<thead>
				<tr class="active">
					<th class="col-md-2">Placa</th>
					<th class="col-md-8"></th>
					<th class="col-md-1" colspan="2">Opções</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($veiculos as $veiculo)
				<tr>
					<td>{{ $veiculo->placa }}</td>
					<td>{{ $veiculo->descricao }}</td>
					<td>
						<a href="{{ URL::route('veiculos.edit', $veiculo->id) }}" class="btn btn-primary btn-sm">
						<span class="glyphicon glyphicon-pencil"></span>
						Editar
					</a>
					</td>
					<td>
						{{ Form::open(array('method' => 'DELETE', 'route' => array('veiculos.destroy', $veiculo->id))) }}                       
							<button type="submit" class="btn btn-danger btn-sm">
								<span class="glyphicon glyphicon-remove"></span>
								Deletar
							</button>
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		
		<div class="panel-footer" style="text-align: right;" ><b>Total Veículos Cadastrados: {{ Veiculo::count() }}</b></div>
	</div>    
@stop