@extends('main')

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Editar Veículo</b>
		</div>
		<div class="panel-body">
			{{ Form::model($veiculo, array('method' => 'PATCH', 'route' =>
 array('veiculos.update', $veiculo->id))) }}{{ Form::open(array('route' => 'veiculos.store')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{{ Form::label('placa', 'Placa:') }}
							{{ Form::text('placa', null, array('class' => 'form-control', 'style' => 'text-transform:uppercase;')) }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							{{ Form::label('descricao', 'Descrição:') }}
							{{ Form::text('descricao', null, array('class' => 'form-control')) }}
						</div>
					</div>
				</div>
				<br/>
				<a href="{{ URL::route('veiculos.index') }}" class="btn btn-warning">Fechar</a>
				{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>

@stop