<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../../favicon.ico">

		<title>Login</title>

		<!-- Latest compiled and minified CSS -->
		{{ HTML::style('css/bootstrap.min.css') }}
		<!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">-->

	</head>

	<body style="background-color: #eee;">

		<div class="container" style="max-width:330px; padding:15px;">

		@if (Session::has('message'))
			<div class="alert alert-info hidden-print" role="alert">
				<p>
					<span class="glyphicon glyphicon-info-sign"></span>
					{{ Session::get('message') }}
				</p>
			</div>
		@endif

			{{ Form::open(array('url' => 'login')) }}

				<h3>Por Favor, Faça o Login</h3>
				
				{{ Form::label('username', 'Usuário') }}
				{{ Form::text('username', Input::old('username'), array('class' => 'form-control')) }}
				
				{{ Form::label('password', 'Senha') }}
				{{ Form::password('password', array('class' => 'form-control'))}}
				<br/>
				{{ Form::submit('Login', array('class' => 'btn btn-lg btn-primary btn-block')) }}
			{{ Form::close() }}

		</div> <!-- /container -->

	</body>
</html>