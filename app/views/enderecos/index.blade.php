@extends('main')

@section('content')

	<div class="panel panel-default">
	    <div class="panel-heading">
	        <b>Enderecos</b>
	        <div class="pull-right">
	            <a href="{{ URL::route('clientes.enderecos.create') }}" class="btn btn-default btn-xs">Adicionar Novo</a>
	        </div>
	    </div>
	    <div class="panel-body">
	        <input type="search" class="form-control input-sm" placeholder="Buscar..."> 
	    </div>
	    <table class="table table-hover">
	        <tr class="active">
	            <th>Nome da Obra</th>
	            <th>Cliente Proprietário</th>
	            <th>Mestre</th>
	            <th>Telefone</th>
	            <th colspan="2">Opções</th>
	        </tr>
	        @foreach ($clientes as $endereco)
	        <tr>
	            <td>{{ $endereco->nome }}</td>
	            <td>{{ '--' }}</td>
	            <td>{{ $endereco->mestre }}</td>
	            <td>{{ $endereco->telefone }}</td>
	            <td width="50">
	                {{ link_to_route('clientes.enderecos.edit', 'Editar', array($endereco->id), array('class' => 'btn btn-info btn-xs')) }}
	            </td>
	            <td width="60">
	            	{{ Form::open(array('method' => 'DELETE', 'route' => array('clientes.enderecos.destroy', $endereco->id))) }}                       
                    	{{ Form::submit('Deletar', array('class' => 'btn btn-danger btn-xs')) }}
                    {{ Form::close() }}
	            </td>
	        </tr>
	        @endforeach
	    </table>
	    
	    <div class="panel-footer" style="text-align:right;"><b>Total Obras Cadastrados: {{ Endereco::count() }}</b></div>
	</div>    
@stop