@extends('main')

@section('content')

<script>
jQuery(function($){
	$(".telefone-mask").mask("(99)9999-9999");
	$(".cep-mask").mask("99999-999");

	$('.input-cidade-text').on('focusout',function(){
		$('#estado').val("PR");
	});
});
</script>
<div ng-controller="SelecionarCliente">
	<div class="panel panel-default" ng-init="pegaCliente();">
		<div class="panel-heading">
			<b>Editar Endereco</b>
		</div>
		<div class="panel-body">
			{{ Form::model($endereco, array(
				'method' => 'PATCH',
				'route'  => array('clientes.enderecos.update', $cliente->id, $endereco->id)
			)) }}
 			{{ Form::open(array(
 				'route' => 'clientes.enderecos.store'
 			)) }}
				<div class="form-group">
					<div class=" well well-sm">
						<b>Proprietário:</b><br />
						<div class="row">
							<div class="col-md-6">
								<b>Cliente: </b><br />
								{{ $cliente->nome }}
							</div>
							<div class="col-md-6">
								<b>CPF/CNPJ: </b><br />
								{{ $cliente->cpf_cnpj }}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							{{ Form::label('nome', 'Nome:') }}
							{{ Form::text('nome', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-6">
							{{ Form::label('contato', 'Contato:') }}
							{{ Form::text('contato', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6">
							{{ Form::label('telefone', 'Telefone:') }}
							{{ Form::text('telefone', null, array('class' => 'form-control telefone-mask')) }}
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-9">
							{{ Form::label('logradouro', 'Rua:') }}
							{{ Form::text('logradouro', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::label('numero', 'Numero:') }}
							{{ Form::text('numero', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							{{ Form::label('complemento', 'Complemento:') }}
							{{ Form::text('complemento', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6">
							{{ Form::label('referencia', 'Referencia:') }}
							{{ Form::text('referencia', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-3">
							{{ Form::label('bairro', 'Bairro:') }}
							{{ Form::text('bairro', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2">
							{{ Form::label('cep', 'CEP:') }}
							{{ Form::text('cep', null, array('class' => 'form-control cep-mask')) }}    
						</div>
						<div class="col-md-5">
							{{ Form::label('cidade', 'Cidade:') }}
							{{ Form::text('cidade', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2">
							{{ Form::label('estado', 'Estado:') }}
							{{ Form::text('estado', null, array('class' => 'form-control')) }}
						</div>
					</div>
				</div>
				<br/>
				<a href="{{ URL::route('clientes.show',$cliente->id) }}" class="btn btn-warning">Fechar</a>
				{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>




	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Cliente</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<tr class="active">
						<th>Nome</th>
						<th>CPF/CNPJ</th>
						<th>Telefone</th>
						<th colspan="2">Opções</th>
					</tr>

					<tr ng-repeat="c in clientes | filter:search">
						<td>[[c.nome]]</td>
						<td>[[c.cpf_cnpj]]</td>
						<td>[[c.telefone_fixo]]</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="pegaCliente(c.nome,c.id);" data-dismiss="modal">Selecionar</button></td>
					</tr>
				</table>

				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>

@stop