@extends('main')

@section('content')
<ul class="nav nav-tabs" id="tabulacao">
	<li role="presentation"><a href="{{ URL::route('config.usuarios.index') }}">Usuários</a></li>
	<li role="presentation"><a href="{{ URL::route('config.unidades.index') }}">Unidades</a></li>
	<li role="presentation"><a href="{{ URL::route('config.backup.index') }}">Backup</a></li>
	<li role="presentation"><a href="#">Miscelânea</a></li>
</ul>
<br/>
	@yield('tabContent')

@stop