@extends('main')

@section('content')
<style type="text/css">
@media print{
	.container {
		width: 100% !important;
		margin: 0 !important;
	}
}
</style>
<div class="panel panel-default hidden-print">
	<div class="panel-heading">
		<div class="row">
			<div class="col-md-7">
				<h4>Pedido {{ $pedido->codigo }}</h4>
			</div>
			<div class="col-md-5">
				<button class="btn btn-default pull-right" onclick="javascript:window.print()">
					<span class="glyphicon glyphicon-print">
					</span>
				</button>
				@if(!$pedido->efetivado)
				{{ Form::open(array('method' => 'PATCH', 'route' => array('pedidos.efetivar', $pedido->id))) }}
				<input type="checkbox" name="efetivado" value="on" checked="checked" class="hidden">                       
				{{ Form::submit('Efetivar Venda', array('class' => 'btn btn-default pull-right')) }}
				{{ Form::close() }}
				@endif
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-6">
					<b>Cliente:</b><br />
					{{ $pedido->cliente->nome }}
				</div>
				<div class="col-md-6">
					<b>Nome Fantasia:</b><br />
					{{ $pedido->cliente->nome_fantasia }}
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<b>CPF/CNPJ:</b><br />
					{{ $pedido->cliente->cpf_cnpj }}
				</div>
				<div class="col-md-3">
					<b>Telefone Fixo:</b><br />
					{{ $pedido->cliente->telefone_fixo }}
				</div>
				<div class="col-md-3">
					<b>Telefone Celular:</b><br />
					{{ $pedido->cliente->telefone_celular }}
				</div>
			</div>
		</div>
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-12">
					<b> Endereço de Entrega: </b>{{ $pedido->entrega->endereco->nome }}
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<b>Rua:</b><br />
					{{ $pedido->entrega->endereco->logradouro }}
				</div>
				<div class="col-md-3">
					<b>Número:</b><br />
					{{ $pedido->entrega->endereco->numero }}
				</div>
				<div class="col-md-3">
					<b>Complemento:</b><br />
					{{ $pedido->entrega->endereco->complemento }}
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<b>Bairro:</b><br />
					{{ $pedido->entrega->endereco->bairro }}
				</div>
				<div class="col-md-3">
					<b>CEP</b><br />
					{{ $pedido->entrega->endereco->cep }}
				</div>
				<div class="col-md-3">
					<b>Cidade:</b><br />
					{{ $pedido->entrega->endereco->cidade }}
				</div>
				<div class="col-md-3">
					<b>Estado:</b><br />
					{{ $pedido->entrega->endereco->estado }}
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<b>Contato/Recebedor</b><br />
					{{ ($pedido->entrega->endereco->contato)?$pedido->entrega->endereco->contato:'Não informado' }}
				</div>
			</div>
		</div>
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-12">
					<b>Responsaveis pela entrega:</b>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<b>Funcionário:</b><br/>
					{{ ($pedido->entrega->funcionario)?$pedido->entrega->funcionario->nome:'Não há' }}
				</div>
				<div class="col-md-6">
					<b>Veiculo:</b><br/>
					{{ ($pedido->entrega->veiculo)?$pedido->entrega->veiculo->placa:'Não há' }}
				</div>
			</div>
		</div>
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-12">
					<b>Produto(s):</b>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
				<table class="table table-default">
					<thead>
						<tr>
							<th class="col-md-2">Cód.</th>
							<th class="col-md-6">Prod. Nome</th>
							<th class="col-md-2">Quant.</th>
							<th class="col-md-2">Preço R$</th>
						</tr>
					</thead>
					<tbody>
				@foreach($pedido->produtos as $item)
					<tr>
						<td>
							{{$item->codigo}}
						</td>
						<td>
							{{$item->nome}}
						</td>
						<td>
							{{$item->pivot->qtd." ".$item->unidade->abreviatura}}
						</td>
						<td>
							{{$item->pivot->preco}}
						</td>
					</tr>
				@endforeach
					</tbody>
				</table>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<b>Total R$</b><br />
					{{$pedido->total}}
				</div>
			</div>

		</div>
	</div>
</div>
<div class="container-fluid">
	@include('pedidos.print', array('pedido'=>$pedido))
</div>
@stop