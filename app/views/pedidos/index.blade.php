@extends('main')

@section('content')
	<style type="text/css">
	@media print{
		.container {
			width: 100% !important;
			margin: 0 !important;
		}
	}
	</style>
<div ng-controller="MostrarPedido">
	<div class="hidden-print">

	<div class="panel panel-default hidden-print">
		<div class="panel-heading">
			Pedidos	
			<div class="pull-right">
				<a href="{{ URL::route('pedidos.create') }}" class="btn btn-default btn-xs">Novo Pedido</a>
			</div>
		</div>
		<div class="panel-body form-inline text-center">
			{{ Form::open(array('url'=> '#','method'=>'GET')) }}
				Mês:
				<div class="input-group col-xs-2">
					<span class="input-group-btn">
						<button class="btn btn-default" type="submit" ng-click="mes = mes-1">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</button>
					</span>
					<input type="text" name="mes" class="form-control text-center" ng-model="mes" ng-init="mes={{ $data['month'] }}" readonly/>
					<span class="input-group-btn">
						<button class="btn btn-default" type="submit" ng-click="mes = mes+1">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</button>
					</span>
				</div>
				Ano:
				<div class="input-group col-xs-2">
					<span class="input-group-btn">
						<button class="btn btn-default" type="submit" ng-click="ano = ano-1">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</button>
					</span>
					<input type="text" name="ano" class="form-control text-center" ng-model="ano" ng-init="ano={{ $data['year'] }}" readonly/>
					<span class="input-group-btn">
						<button class="btn btn-default" type="submit" ng-click="ano = ano+1">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</button>
					</span>
				</div>
				{{ Form::close() }}
		</div>

		<table class="table table-hover">
			<thead>
				<tr class="active">
					<th class="col-md-1">Nº</th>
					<th class="col-md-1">Data</th>
					<th class="col-md-4">Cliente</th>
					<th class="col-md-1">Produto</th>
					<th class="col-md-2">Total (R$)</th>
					<th class="col-md-3"></th>
				</tr>
			</thead>
			@foreach ($pedidos as $pedido)
			<tr>
				<td>{{ $pedido->codigo }}</td>
				<td>{{ date("d/m/Y",strtotime($pedido->data)) }}</td>
				<td>{{ $pedido->cliente->nome }}</td>
				<td>{{ (isset($pedido->produtos[0]))?$pedido->produtos[0]->codigo:"" }}</td>
				<td>{{ $pedido->total }}</td>
				<td>
					<button type="button" ng-click="printPedido({{ $pedido->id }})" class="btn btn-default btn-sm">
						<span class="glyphicon glyphicon-print"></span>
					</button>
					<a href="{{ URL::route('pedidos.show', $pedido->id) }}" class="btn btn-success btn-sm">
						<span class="glyphicon glyphicon-eye-open"></span>
						Ver
					</a>
					@if(!$pedido->efetivado)
						<a href="{{ URL::route('pedidos.edit', $pedido->id) }}" class="btn btn-primary btn-sm">
							<span class="glyphicon glyphicon-pencil"></span>
							Editar
						</a>
						{{ Form::open(array( 'method'	=> 'DELETE', 
											 'route'	=> array('pedidos.destroy', $pedido->id),
											 'class'	=> 'force-inline-block')) }}                       
							<button type="submit" class="btn btn-danger btn-sm">
								<span class="glyphicon glyphicon-remove"></span>
								Deletar
							</button>
						{{ Form::close() }}
					@else
						{{ Form::open(array( 'method'	=> 'PATCH', 
											 'route'	=> array('pedidos.cancelar', $pedido->id),
											 'class'	=> 'force-inline-block')) }}                       
							<button type="submit" class="btn btn-danger btn-sm">
								<span class="glyphicon glyphicon-remove-sign"></span>
								Cancelar
							</button>
						{{ Form::close() }}
					@endif
				</td>
			</tr>
			@endforeach
		</table>

		<div class="panel-footer text-right">
			<b>Total de pedidos no mês: {{ $monthCount }}</b>
		</div>
	</div>
	<div class="row">
		{{ Form::open(array('url' => '#', 'method' => 'GET')) }}
			<input type="text" name='ano' ng-model="ano" class="hidden"/>
			<input type="text" name='mes' ng-model="mes" class="hidden"/>
			<div class="input-group col-xs-2 col-xs-offset-5">
				<span class="input-group-btn">
					<button class="btn btn-default" type="submit" ng-click="page = page-1">
						<span class="glyphicon glyphicon-chevron-left"></span>
					</button>
				</span>
				<input type="text" name="pag" class="form-control text-center" ng-model="page" ng-init="page={{ $pgNum }}" readonly/>
				<span class="input-group-btn">
					<button class="btn btn-default" type="submit" ng-click="page = page+1">
						<span class="glyphicon glyphicon-chevron-right"></span>
					</button>
				</span>
			</div>
		{{ Form::close() }}
	</div>
	<br />
	<div ng-bind-html="pedido" class="container-fluid">
	</div>
</div>
@stop