@extends('main')

@section('content')

<div  ng-controller="AdicionarPedido" ng-init="pedido_id={{$pedido->id}};cliente_id={{$pedido->cliente->id}};endereco_id={{$pedido->entrega->endereco->id}};total_pedido={{$pedido->total}}">
	{{ Form::model($pedido, array('method' => 'PATCH', 'route' =>array('pedidos.update', $pedido->id))) }}
	{{ Form::open(array('route' => 'pedidos.store')) }}
	{{ Form::hidden('id', $pedido->id, array( 'id' => 'pedidoId', 'readonly' => 'readonly' )) }}
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="row">
				<div class="col-md-7">
					<h2><b>Novo Pedido</b></h2>
				</div>
				<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon">
							Pedido Nº
						</span>
						{{ Form::text('codigo', null, array('class' => 'form-control ')) }}
					</div>
				</div>
				<div class="col-md-3">
					<div class="input-group">
						<span class="input-group-addon">
							Data:
						</span>
						{{ Form::input('date','data', date('Y-m-d'), array('class' => 'form-control input-sm')) }}
					</div>
				</div>
				<div class="col-md-2">
					<div class="input-group">
						<span class="input-group-addon">
							Hora:
						</span>
						{{ Form::input('time','hora', null, array('class' => 'form-control input-sm')) }}
					</div>
				</div>
				</div>
		</div>
		<div class="panel-body">
				<div class="form-group">
					<h4 class="page-header" style="margin-top:1.5em">Cliente</h4>
					<div class="row">
						<div class="col-md-12">
							{{ Form::label('', 'Nome') }}
							<div class="input-group">
								<input type="text" class="form-control" disabled ng-model="cliente.nome">
								<span class="input-group-btn">
									<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#clientesModal">Selecionar...</button>
								</span>
							</div><!-- /input-group -->
						</div>
						{{ Form::text('cliente_id', null, array('class' => 'form-control hidden', 'ng-model' => 'cliente.id')) }}
						<div class="col-md-6">
							{{ Form::label('', 'CNPJ/CPF') }}
							{{ Form::text('', null, array('class' => 'form-control', 'readonly','ng-model' => 'cliente.cpf_cnpj')) }}
						</div>
						<div class="col-md-6">
							{{ Form::label('', 'Telefone') }}
							{{ Form::text('', null, array('class' => 'form-control', 'readonly','ng-model' => 'cliente.telefone_fixo')) }}
						</div>
					</div>
					<h4 class="page-header">Endereço de entrega</h4>
					<div class="row">
						<div class="col-md-9">
							{{ Form::text('entrega[endereco_id]', 0, array('class' => 'form-control hidden','ng-model' => 'endereco.id')) }}
							{{ Form::label('', 'Nome') }}
							<div class="input-group">
								<input type="text" class="form-control" disabled ng-model="endereco.nome">
								<span class="input-group-btn">
									<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#enderecosModal" ng-click="getObras();">Selecionar...</button>
								</span>
							</div><!-- /input-group -->
						</div>
						<div class="col-md-3">
							{{ Form::label('', 'Receptor') }}
							{{ Form::text('', null, array('class' => 'form-control', 'readonly','ng-model' => 'endereco.contato')) }}
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-6">
							{{ Form::label('', 'Rua') }}
							{{ Form::text('', null, array('class' => 'form-control', 'readonly','ng-model' => 'endereco.logradouro')) }}
						</div>
						<div class="col-md-3">
							{{ Form::label('', 'Nº') }}
							{{ Form::text('', null, array('class' => 'form-control', 'readonly','ng-model' => 'endereco.numero')) }}
						</div>
						<div class="col-md-3">
							{{ Form::label('', 'Bairro') }}
							{{ Form::text('', null, array('class' => 'form-control', 'readonly','ng-model' => 'endereco.bairro')) }}
						</div>
						<div class="col-md-8">
							{{ Form::label('', 'Cidade') }}
							{{ Form::text('', null, array('class' => 'form-control', 'readonly','ng-model' => 'endereco.cidade')) }}
						</div>
						<div class="col-md-1">
							{{ Form::label('', 'UF') }}
							{{ Form::text('', null, array('class' => 'form-control', 'readonly','ng-model' => 'endereco.estado')) }}
						</div>
						<div class="col-md-3">
							{{ Form::label('', 'CEP') }}
							{{ Form::text('', null, array('class' => 'form-control', 'readonly','ng-model' => 'endereco.cep')) }}
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h4 class="page-header">Responsavel Entrega</h4>
						</div>
						<div class="col-md-6">
							{{ Form::label('entrega[funcionario_id]', 'Motorista') }}
							{{ Form::select('entrega[funcionario_id]', $funcionarios, null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6">
							{{ Form::label('entrega[veiculo_id]', 'Veículo') }}
							{{ Form::select('entrega[veiculo_id]', $veiculos, null, array('class' => 'form-control')) }}
						</div>
					</div>
					<br/>
					<hr/>

					@if(!Input::has('id'))
					<div class="row">
						<div class="col-md-6">
							{{ Form::label('','Produto')}}
							<div class="input-group">
								{{ Form::text('', null, array('class' => 'form-control', 'ng-model' => 'produto.nome','readonly')) }}
								<span class="input-group-btn">
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#produtosModal">
										<span class="glyphicon glyphicon-search"></span>
									</button>	
									</span>
								</span>
							</div>
						</div>
						<div class="col-md-3">
							{{ Form::label('','Cód.Produto')}}
							{{ Form::text('', null, array('class' => 'form-control','ng-model' => 'produto.codigo' ,'readonly')) }}
						</div>
						<div class="col-md-3">
							{{ Form::label('produtos[ [[produto.id]] ][qtd]', 'Quantidade') }}
							<div class="input-group">
								{{ Form::text('produtos[ [[produto.id]] ][qtd]', null, array('class' => 'form-control', 'ng-change' => 'total()', 'ng-model' => 'produto.qtd')) }}
								<span class="input-group-addon">
									[[ produto.unidade.abreviatura || "un" ]]
								</span>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-4">
								<label for="produtos[ [[produto.id]] ][preco]">Valor por [[ produto.unidade.abreviatura || "un" ]]</label>
								<div class="input-group">
									<span class="input-group-addon">
										R$
									</span>
									{{ Form::text('produtos[ [[produto.id]] ][preco]', null, array('class' => 'form-control', 'ng-model' => 'produto.preco')) }}
								</div>
							</div>
							<div class="col-md-4">
								<label for="taxa_faltante">Taxa por [[ produto.unidade.abreviatura || "un" ]] Faltante</label>
								<div class="input-group">
									<span class="input-group-addon">
										R$
									</span>
									{{ Form::text('taxa_faltante', null, array('class' => 'form-control', 'ng-model' => 'taxa','ng-init' => ' taxa = total_pedido - (produto.preco*produto.qtd) ')) }}
								</div>
							</div>
							<div class="col-md-4">
								{{ Form::label('total', 'Valor Total') }}
								<div class="input-group">
									<span class="input-group-addon">
										R$
									</span>
									{{ Form::text('total', "[[ total() ]]", array('class' => 'form-control', 'readonly')) }}
								</div>
							</div>
					</div>
					<br />
					<div class="panel panel-default">
						<div class="panel-heading">
							Composição do Produto
						</div>
						<table class="table">
							<tbody>
								<tr ng-repeat="ingrediente in produto.composicao">
									<td>[[ingrediente.insumo.nome]]</td>
								</tr>
							</tbody>
						</table>
					</div>
					@else
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Produtos
								</div>
								<table class="table table-hover">
									<thead>
										<tr class="active">
											<th class="col-md-1">
												Cód.
											</th>
											<th class="col-md-6">
												Produto
											</th>
											<th class="col-md-2">
												Quant.
											</th>
											<th class="col-md-2">
												Valor
											</th>
											<th class="col-md-1"></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="produto in produtosSelecionados">
											<td>
												[[ produto.codigo ]]
											</td>
											<td>
												[[ produto.nome ]]
											</td>
											<td>
												<div class="input-group">
													<input type="text" class="form-control" name="produtos[ [[produto.id]] ][qtd]" ng-model="produto.qtd"/>
													<span class="input-group-addon">
														[[ produto.unidade.abreviatura ]]
													</span>
												</div>
											</td>
											<td>
												<div class="input-group">
													<span class="input-group-addon">
														R$
													</span>
													<input type="text" class="form-control" name="produtos[ [[produto.id]] ][preco]" ng-model="produto.preco"/>
												</div>
											</td>
											<td>
												<button type="button" ng-click="delProduto(produto.id);" class="btn btn-danger btn-sm">
													<span class="glyphicon glyphicon-remove">
													</span>
												</button>
											</td>
										</tr>
									</tbody>
								</table>
								<div class="panel-footer">
									<div class="row">
										<div class="col-md-2">
											<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#produtosModalv2">
												Adicionar Produto
											</button>
										</div>
										<div class="col-md-5 col-md-offset-5 form-inline">
											<div class="form-group">
												{{ Form::label('total', 'Valor Total:') }}
												<div class="input-group">
													<span class="input-group-addon">
														R$
													</span>
													<input type="text" name="total" class="form-control" value="[[ calcTotal() ]]"/>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endif
				</div>
				<br />
				<label>
					<input type="checkbox" name="efetivado" checked="checked"/>
					Venda Efetivada
				</label>
				<hr />
				<a href="{{ URL::route('pedidos.index') }}" class="btn btn-warning">Fechar</a>
				{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>
<br/><br/><br/>


	<!-- Modal Clientes-->
	<div class="modal fade" id="clientesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-focus="page = 0">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Cliente</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<tr class="active">
						<th>Nome</th>
						<th>CPF/CNPJ</th>
						<th>Telefone</th>
						<th colspan="2">Opções</th>
					</tr>

					<tr ng-repeat="cliente in clientes | filter:search | limitTo:6:page" ng-init="page = 0;">
						<td>[[cliente.nome]]</td>
						<td>[[cliente.cpf_cnpj]]</td>
						<td>[[cliente.telefone_fixo]]</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getCliente(cliente.id);" data-dismiss="modal">Selecionar</button></td>
					</tr>
				</table>
				<div class="row">
					<div class="col-xs-1 col-xs-offset-4">
						<button type="button" class="btn btn-default" ng-click="page = page-6" ng-disabled="page == 0">
							<span class="glyphicon glyphicon-menu-left"></span>
						</button>
					</div>
					<div class="col-xs-2 text-center">
						[[ page ]]/[[clientes.length]]
					</div>
					<div class="col-xs-1">
						<button type="button" class="btn btn-default" ng-click="page = page+6" ng-disabled="(page+6)>clientes.length">
							<span class="glyphicon glyphicon-menu-right"></span>
						</button>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>


	<!-- Modal Obras -->
	<div class="modal fade" id="enderecosModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-focus="page = 0">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Endereço</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<tr class="active">
						<th>Nome</th>
						<th>Contato</th>
						<th>Telefone</th>
						<th>Endereço</th>
						<th colspan="2">Opções</th>
					</tr>

					<tr ng-repeat="endereco in enderecos | filter:search | limitTo:6:page">
						<td>[[endereco.nome]]</td>
						<td>[[endereco.contato]]</td>
						<td>[[endereco.telefone]]</td>
						<td>[[endereco.logradouro]], [[endereco.numero]], [[endereco.bairro]]/[[endereco.cidade]]-[[endereco.estado]]</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getEndereco(endereco.id);" data-dismiss="modal">Selecionar</button></td>
					</tr>
				</table>
				<div class="row">
					<div class="col-xs-1 col-xs-offset-4">
						<button type="button" class="btn btn-default" ng-click="page = page-6" ng-disabled="page == 0">
							<span class="glyphicon glyphicon-menu-left"></span>
						</button>
					</div>
					<div class="col-xs-1 col-xs-offset-2">
						<button type="button" class="btn btn-default" ng-click="page = page+6">
							<span class="glyphicon glyphicon-menu-right"></span>
						</button>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Serviço / o produto é um servico -->
	<div class="modal fade" id="produtosModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-focus="page = 0">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Produto</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<thead>
						<tr class="active">
							<th>Cód</th>
							<th>Nome</th>
							<th>Quantidade em Estoque</th>
							<th></th>
						</tr>
					</thead>	
					<tbody>
					<tr ng-repeat="produto in produtos | filter:search | limitTo:6:page">
						<td>[[ produto.codigo ]]</td>
						<td>[[ produto.nome ]]</td>
						<td>
							[[ produto.qtd_estoque ]]&nbsp;[[ produto.unidade.abreviatura ]]
						</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="get1Produto(produto.id);" data-dismiss="modal">Selecionar</button></td>
					</tr>	
					</tbody>
				</table>
				<div class="row">
					<div class="col-xs-1 col-xs-offset-4">
						<button type="button" class="btn btn-default" ng-click="page = page-6" ng-disabled="page == 0">
							<span class="glyphicon glyphicon-menu-left"></span>
						</button>
					</div>
					<div class="col-xs-1 col-xs-offset-2">
						<button type="button" class="btn btn-default" ng-click="page = page+6" ng-disabled="(page+6)>produtos.length">
							<span class="glyphicon glyphicon-menu-right"></span>
						</button>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal Produtos v2-->
	<div class="modal fade" id="produtosModalv2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Produto</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<thead>
						<tr class="active">
							<th>Cód</th>
							<th>Nome</th>
							<th>Quantidade em Estoque</th>
							<th></th>
						</tr>
					</thead>	
					<tbody>
					<tr ng-repeat="produto in produtos | filter:search | limitTo:6:page">
						<td>[[ produto.codigo ]]</td>
						<td>[[ produto.nome ]]</td>
						<td>
							[[ produto.qtd_estoque ]]&nbsp;[[ produto.unidade.abreviatura ]]
						</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getProduto(produto.id);" data-dismiss="modal">Selecionar</button></td>
					</tr>	
					</tbody>
				</table>
				<div class="row">
					<div class="col-xs-1 col-xs-offset-4">
						<button type="button" class="btn btn-default" ng-click="page = page-6" ng-disabled="page == 0">
							<span class="glyphicon glyphicon-menu-left"></span>
						</button>
					</div>
					<div class="col-xs-1 col-xs-offset-2">
						<button type="button" class="btn btn-default" ng-click="page = page+6" ng-disabled="(page+6)>produtos.length">
							<span class="glyphicon glyphicon-menu-right"></span>
						</button>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>


</div>


@stop