@extends('main')

@section('content')

<div ng-controller="AdicionarFormaPagamento">
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Forma de Pagamento</b>
		</div>
		<div class="panel-body">
			<div class="well">
				<div class="row">
					<div class="col-md-2">
						<b>Data da conta:</b><br/>
						{{ date('d/m/Y', strtotime($conta->data)) }}
					</div>
					<div class="col-md-4">
						<b>Fornecedor:</b><br/>
						{{ $conta->fornecedor }}
					</div>
					<div class="col-md-3">
						<b>Categoria da Conta:</b><br/>
						{{ $conta->categoria->codigo.' - '.$conta->categoria->nome}}
					</div>
					<div class="col-md-3" ng-init="totalConta = {{ $conta->valor_total }}">
						<b>Valor total:</b><br/>
						R$ {{ $conta->valor_total }}
					</div>
				</div>
			</div>
			{{ Form::open(['route'=>['contaspagar.parcelas.store',$conta->id]]) }}
			<input type="text" class="hidden" name="contapagar_id" value="{{$conta->id}}" ng-init="contapagar_id = {{$conta->id}}"/>
			<h4 class="page-header">Parcelas</h4>
			<fieldset>
					<div class="row">
						<div class="col-md-3">
							<b>
								Entrada:
							</b>
							<div class="input-group">
								<span class="input-group-addon">
									R$
								</span>
						 		<input type="text" class="form-control" ng-model="valorEntrada"/> 
						 	</div>
						</div>
						<div class="col-md-3">
							<b>
								Nº parcelas:
							</b>
							<div class="input-group">
						 		<input type="text" class="form-control" ng-model="numParcelas"/>
								<span class="input-group-addon">
									x
								</span>
							</div>
						</div>		
						<div class="col-md-3">		
							<b>
								Forma de Pagamento:
							</b>
							<div class="input-group">
						 		<input type="text" class="form-control" value="[[formaPagamentoSel ? formaPagamentoSel.codigo +' - '+ formaPagamentoSel.nome : '']]" />
								<span class="input-group-btn">
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#formaPagamentoModal">
										<span class="glyphicon glyphicon-search">
										</span>
									</button>
								</span>
							</div>	
						</div>
						<div class="col-md-3">
						<br />		
							 <button type="button" class="btn btn-primary" ng-click="gerarParcelas()">
							 	Gerar Parcelas
							 </button>
						</div>
					</div>
					<hr />
					<table class="table table-default">
						<thead>
							<tr>
								<th class="col-md-2"> Data Vencimento</th>
								<th class="col-md-4"> Valor da Parcela</th>
								<th class="col-md-4"> Forma de Pagamento</th>
								<th class="col-md-2">
									<button type="button" class="btn btn-primary btn-sm" ng-click="addParcela()">
										<span class="glyphicon glyphicon-plus"></span>
										Adicionar Parcela
									</button>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="parcela in parcelas">
								<td>
									<input type="date" id="[['parc'+parcela.id]]" name="parcela[ [[parcela.id]] ][data_vencimento]" class="form-control" ng-model="parcela.data_vencimento"/>
								</td>
								<td>
									<div class="input-group">
										<span class="input-group-addon">R$</span>
										<input type="text" name="parcela[ [[parcela.id]] ][valor]" class="form-control" ng-model="parcela.valor"/>
									</div>
								</td>
								<td>
									<input type="text" name="parcela[ [[parcela.id]] ][forma_pagamento_id]" class="hidden" ng-model="parcela.formaPagamento.id" />
									<div class="input-group">
										<input type="text" class="form-control" value="[[ parcela.formaPagamento? parcela.formaPagamento.codigo +' - '+ parcela.formaPagamento.nome : '']]" readonly/>
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary" ng-click="openFormaPagamentoParcelaModal(parcela.id)">
												<span class="glyphicon glyphicon-search">
												</span>
											</button>
										</span>
									</div>
								</td>
								<td>
									<button type="button" class="btn btn-sm btn-danger" ng-click="delParcela(parcela.id)">
										<span class="glyphicon glyphicon-remove"></span>
										Remover
									</button>
								</td>
							</tr>			
						</tbody>
					</table>
					<div class="row">
						<div class="col-md-3 col-md-offset-9">
							<b> Total Parcelas:</b>
							<div class="input-group">
								<span class="input-group-addon">R$</span>
								<input type="text" class="form-control"  value="[[ sumParcelas() ]]"/>
							</div>
						</div>
					</div>

			</fieldset>
			<button type="submit" class="btn btn-primary">Salvar</button>
			{{ Form::close() }}				
		</div>
	</div>

<div class="modal fade" id="formaPagamentoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-focus="page = 0">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" ng-click="clearFormaPagamento()"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Forma de Pagamento</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<thead>
						<tr class="active">
							<th>Cód</th>
							<th>Nome</th>
							<th></th>
						</tr>
					</thead>	
					<tbody>
					<tr ng-repeat="formaPagamento in formasPagamento | filter:search | limitTo:6:page">
						<td>[[ formaPagamento.codigo ]]</td>
						<td>[[ formaPagamento.nome ]]</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getFormaPagamento(formaPagamento.id);">Selecionar</button></td>
					</tr>	
					</tbody>
				</table>
				<div class="row">
					<div class="col-xs-1 col-xs-offset-4">
						<button type="button" class="btn btn-default" ng-click="page = page-6" ng-disabled="page == 0">
							<span class="glyphicon glyphicon-menu-left"></span>
						</button>
					</div>
					<div class="col-xs-1 col-xs-offset-2">
						<button type="button" class="btn btn-default" ng-click="page = page+6" ng-disabled="(page+6)>formasPagamento.length">
							<span class="glyphicon glyphicon-menu-right"></span>
						</button>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal" ng-click="clearFormaPagamento()">Fechar</button>
				</div>
			</div>
		</div>
	</div>

<div class="modal fade" id="formaPagamentoParcelaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-focus="page = 0">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" ng-click="clearFormaPagamento()"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Forma de Pagamento da Parcela</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<thead>
						<tr class="active">
							<th>Cód</th>
							<th>Nome</th>
							<th></th>
						</tr>
					</thead>	
					<tbody>
					<tr ng-repeat="formaPagamento in formasPagamento | filter:search | limitTo:6:page">
						<td>[[ formaPagamento.codigo ]]</td>
						<td>[[ formaPagamento.nome ]]</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getFormaPagamentoParcela( parcelaId,formaPagamento.id);">Selecionar</button></td>
					</tr>	
					</tbody>
				</table>
				<div class="row">
					<div class="col-xs-1 col-xs-offset-4">
						<button type="button" class="btn btn-default" ng-click="page = page-6" ng-disabled="page == 0">
							<span class="glyphicon glyphicon-menu-left"></span>
						</button>
					</div>
					<div class="col-xs-1 col-xs-offset-2">
						<button type="button" class="btn btn-default" ng-click="page = page+6" ng-disabled="(page+6)>formasPagamento.length">
							<span class="glyphicon glyphicon-menu-right"></span>
						</button>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal" ng-click="clearFormaPagamento()">Fechar</button>
				</div>
			</div>
		</div>
	</div>

<div class="modal fade" id="categoriasModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-focus="page = 0">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Selecionar Categoria</h4>
			</div>

			<div class="modal-body">
				<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
			</div>
			
			<table class="table table-hover">
				<thead>
					<tr class="active">
						<th>Cód</th>
						<th>Nome</th>
						<th></th>
					</tr>
				</thead>	
				<tbody>
				<tr ng-repeat="categoria in categoriasConta | filter:search | limitTo:6:page">
					<td>[[ categoria.codigo ]]</td>
					<td>[[ categoria.nome ]]</td>
					<td><button type="button" class="btn btn-primary btn-xs" ng-click="getCategoria(categoria.id)" data-dismiss="modal">Selecionar</button></td>
				</tr>	
				</tbody>
			</table>
			<div class="row">
				<div class="col-xs-1 col-xs-offset-4">
					<button type="button" class="btn btn-default" ng-click="page = page-6" ng-disabled="page == 0">
						<span class="glyphicon glyphicon-menu-left"></span>
					</button>
				</div>
				<div class="col-xs-1 col-xs-offset-2">
					<button type="button" class="btn btn-default" ng-click="page = page+6" ng-disabled="(page+6)>categoriasConta.length">
						<span class="glyphicon glyphicon-menu-right"></span>
					</button>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>

</div>

@stop