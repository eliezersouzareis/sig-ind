@extends('main')

@section('content')
<div ng-controller="PagarParcela">
<div class="panel panel-default">
		<div class="panel-heading">
			<b>Pagar Parcela</b>
		</div>
		<div class="panel-body">
			<div class="well">
				<div class="row">
					<b> Conta a Pagar </b> <br />
						<div class="col-md-1">
							<b>Data:</b></br>
							{{ date('d/m/Y',strtotime($conta->data)) }}
						</div>
						<div class="col-md-5">
							<b>Fornecedor:</b></br>
							{{ $conta->fornecedor }}
						</div>
						<div class="col-md-3">
							<b>Categoria da Conta:</b></br>
							{{ $conta->categoria->codigo." - ".$conta->categoria->nome  }}
						</div>
						<div class="col-md-3">
							<b>Valor total:</b></br>
							R$ {{ $conta->valor_total }}
						</div>
				</div>
			</div>
			{{ Form::open(array('url' => '/contaspagar/'.$conta->id.'/parcelas/'.$parcela->id.'/pagar', 'method'=>'PATCH')) }}
			<div class="row" ng-init="forma_pagamento_id = {{$parcela->forma_pagamento_id}}">
				<div class="col-md-2">
					<label for="data_pagamento">Data de pagamento:</label>
					<input type="date" class="form-control" name="data_pagamento" value="{{date('Y-m-d')}}"/>
				</div>
				<div class="col-md-3">
					<label for="valor-pago">Valor pago:</label>
					<div class="input-group">
						<span class="input-group-addon">
							R$
						</span>
						<input type="text" class="form-control" name="valor_pago" value="{{$parcela->valor}}"/>
					</div>
				</div>
				<div class="col-md-4">
					<label>Forma de Pagamento:</label>
					<input type="text" class="hidden" name="forma_pagamento_id" ng-model="formaPagamentoSel.id"/>
					<div class="input-group">
						<input type="text" class="form-control" value="[[ (formaPagamentoSel) ? formaPagamentoSel.codigo+' - '+formaPagamentoSel.nome : '' ]]" readonly/>
						<div class="input-group-btn">
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#formaPagamentoModal">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<label>Caixa:</label>
					<input type="text" class="hidden" name="caixa_id" ng-model="caixaSel.id"/>
					<div class="input-group">
						<input type="text" class="form-control" ng-model="caixaSel.nome" readonly/>
						<div class="input-group-btn">
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#caixaModal">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</div>
					</div>
				</div>
			</div>
			<br />
			<div  ng-show="formaPagamentoSel.id == 8 || formaPagamentoSel.id == 15">
				<div class="col-md-12">
					<table class="table">
						<thead>
							<tr class="active">
								<th class="col-md-3">Banco:</th>
								<th class="col-md-2">Número do Cheque:</th>
								<th class="col-md-3">Valor:</th>
								<th class="col-md-2">Vencimento</th>
								<th class="col-md-2">
									<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#chequesModal">
										<span class="glyphicon glyphicon-search"></span>
										Buscar Cheque
									</button>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="cheque in chequesSel">
								<td>
									<input type="text" name="cheque[][id]" ng-model="cheque.id" class="hidden" readonly/>
									[[ cheque.banco ]]
								</td>
								<td>
									[[ cheque.numero_cheque ]]
								</td>
								<td>
									R$ [[ cheque.valor ]]
								</td>
								<td>
									[[ cheque.vencimento ]]
								</td>
								<td>
									<button type="button" class="btn btn-danger btn-sm" ng-click="removeCheque(cheque.id)">
										<span class="glyphicon glyphicon-remove"></span>
									</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>			
			<hr />
			<a href="{{ URL::previous() }}" class="btn btn-warning">Cancelar</a>
			{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
		</div>
	{{ Form::close() }}
</div>

	<div class="modal fade" id="chequesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-focus="page = 0">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Cheque</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<thead>
						<tr class="active">
							<th>Banco</th>
							<th>Número do Cheque</th>
							<th>Valor</th>
							<th>
								<button type="button" class="btn btn-primary btn-xs" data-dismiss="modal" data-toggle="modal" data-target="#chequeProprioModal">
									<span class="glyphicon glyphicon-plus"></span>
									Cheque próprio
								</button>
							</th>
						</tr>
					</thead>	
					<tbody>
					<tr ng-repeat="cheque in cheques | filter:search | limitTo:6:page">
						<td>[[ cheque.banco ]]</td>
						<td>[[ cheque.numero_cheque ]]</td>
						<td>R$ [[ cheque.valor ]]</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getCheque(cheque.id);" data-dismiss="modal">Selecionar</button></td>
					</tr>	
					</tbody>
				</table>
				<div class="row">
					<div class="col-xs-1 col-xs-offset-4">
						<button type="button" class="btn btn-default" ng-click="page = page-6" ng-disabled="page == 0">
							<span class="glyphicon glyphicon-menu-left"></span>
						</button>
					</div>
					<div class="col-xs-1 col-xs-offset-2">
						<button type="button" class="btn btn-default" ng-click="page = page+6" ng-disabled="(page+6)>caixas.length">
							<span class="glyphicon glyphicon-menu-right"></span>
						</button>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
  
	<div class="modal fade" id="formaPagamentoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-focus="page = 0">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Forma de Pagamento</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<thead>
						<tr class="active">
							<th>Cód</th>
							<th>Nome</th>
							<th></th>
						</tr>
					</thead>	
					<tbody>
					<tr ng-repeat="formaPagamento in formasPagamento | filter:search | limitTo:6:page">
						<td>[[ formaPagamento.codigo ]]</td>
						<td>[[ formaPagamento.nome ]]</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getFormaPagamento(formaPagamento.id);">Selecionar</button></td>
					</tr>	
					</tbody>
				</table>
				<div class="row">
					<div class="col-xs-1 col-xs-offset-4">
						<button type="button" class="btn btn-default" ng-click="page = page-6" ng-disabled="page == 0">
							<span class="glyphicon glyphicon-menu-left"></span>
						</button>
					</div>
					<div class="col-xs-1 col-xs-offset-2">
						<button type="button" class="btn btn-default" ng-click="page = page+6" ng-disabled="(page+6)>formasPagamento.length">
							<span class="glyphicon glyphicon-menu-right"></span>
						</button>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="caixaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-focus="page = 0">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Caixa</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<thead>
						<tr class="active">
							<th>Caixa</th>
							<th>Saldo</th>
							<th></th>
						</tr>
					</thead>	
					<tbody>
					<tr ng-repeat="caixa in caixas | filter:search | limitTo:6:page">
						<td>[[ caixa.nome ]]</td>
						<td>R$ [[ caixa.saldo ]]</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getCaixa(caixa.id);" data-dismiss="modal">Selecionar</button></td>
					</tr>	
					</tbody>
				</table>
				<div class="row">
					<div class="col-xs-1 col-xs-offset-4">
						<button type="button" class="btn btn-default" ng-click="page = page-6" ng-disabled="page == 0">
							<span class="glyphicon glyphicon-menu-left"></span>
						</button>
					</div>
					<div class="col-xs-1 col-xs-offset-2">
						<button type="button" class="btn btn-default" ng-click="page = page+6" ng-disabled="(page+6)>caixas.length">
							<span class="glyphicon glyphicon-menu-right"></span>
						</button>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div> 

	<div class="modal fade" id="chequeProprioModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-focus="page = 0">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Cheque Próprio</h4>
				</div>

				{{ Form::open() }}
				<div class="modal-body">
					<label for="banco">Banco:</label>
					<input type="text" name="banco" class="form-control" ng-model="chequeProprio.banco"/>
					<label for="numero_cheque">Número do Cheque:</label>
					<input type="text" name="numero_cheque" class="form-control" ng-model="chequeProprio.numero_cheque"/>
					<label for="valor">Valor:</label>
					<div class="input-group">
						<span class="input-group-addon">R$ </span>
						<input type="text" name="valor" class="form-control" ng-model="chequeProprio.valor"/>
					</div>
					<label for="vencimento">Vencimento:</label>
					<input type="date" name="vencimento" class="form-control" ng-model="chequeProprio.vencimento"/>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="submitProprio()">Salvar</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>	 
</div>
  @stop