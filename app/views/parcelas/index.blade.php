@extends('main')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Visualizar Todas Parcelas</b>
		</div>
		<table class="table table-hover">
			<tr class="active">
				<th>Data de Vencimento</th>
				<th>Valor da Parcela</th>
				<th>Opções</th>
			</tr>
			@foreach ($parcelas as $parcela)
			<tr>
				<td>{{ date("d/m/Y",strtotime($parcela->data_vencimento)) }}</td>
				<td>{{ $parcela->valor }}</td>
				<td>
					<button type="button" class="btn btn-primary btn-sm">
						<span class="glyphicon glyphicon-pencil"></span>
					</button>
				</td>
			</tr>
			@endforeach
		</table>
		
		<div class="panel-footer" style="text-align:right;"><b>Total de parcelas Cadastradas: {{ Parcela::count() }}</b></div>
	</div>    

@stop