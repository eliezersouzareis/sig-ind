@extends('main')

@section('content')
<div ng-controller="EditarParcela">
<div class="panel panel-default">
		<div class="panel-heading">
			<b>Nova Parcela</b>
		</div>
		<div class="panel-body">

 			{{ Form::open(array('route' => array('parcelas.edit', $parcela->id))) }}
			{{ Form::model($parcela, array('method' => 'PATCH', 'route' => array('parcelas.edit', $parcela->id))) }}
			<div class="row">
				<div class="col-md-3">
					{{ Form::label('data_vencimento', 'Vencimento da Parcela') }}
					{{ Form::input('date', 'data_vencimento', null, array('class' => 'form-control forma_pagamento')) }}
				</div>
				<div class="col-md-6" ng-init="formapagamentoId ={{$parcela->forma_pagamento_id}} ">
					<label>Forma de Pagamento:</label>
					<input type="text" class="hidden" name="forma_pagamento_id" ng-model="formaPagamentoSel.id"/>
					<div class="input-group">
						<input type="text" class="form-control" value="[[ (formaPagamentoSel) ? formaPagamentoSel.codigo+' - '+formaPagamentoSel.nome : '' ]]" readonly/>
						<div class="input-group-btn">
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#formaPagamentoModal">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					{{ Form::label('valor', 'Valor:') }}
					<div class="input-group">
						<span class="input-group-addon">
							R$
						</span>
						{{ Form::text('valor', null, array('class' => 'form-control')) }}
					</div>
				</div>
			</div>
			<br/>

		<a href="{{ URL::previous() }}" class="btn btn-warning">Cancelar</a>
		{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
		</div>
	{{ Form::close() }}
</div>

	<div class="modal fade" id="formaPagamentoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-focus="page = 0">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Selecionar Forma de Pagamento</h4>
				</div>

				<div class="modal-body">
					<input type="search" class="form-control input-sm" placeholder="Buscar..." ng-model="search"> 
				</div>
				
				<table class="table table-hover">
					<thead>
						<tr class="active">
							<th>Cód</th>
							<th>Nome</th>
							<th></th>
						</tr>
					</thead>	
					<tbody>
					<tr ng-repeat="formaPagamento in formasPagamento | filter:search | limitTo:6:page">
						<td>[[ formaPagamento.codigo ]]</td>
						<td>[[ formaPagamento.nome ]]</td>
						<td><button type="button" class="btn btn-primary btn-xs" ng-click="getFormaPagamento(formaPagamento.id);">Selecionar</button></td>
					</tr>	
					</tbody>
				</table>
				<div class="row">
					<div class="col-xs-1 col-xs-offset-4">
						<button type="button" class="btn btn-default" ng-click="page = page-6" ng-disabled="page == 0">
							<span class="glyphicon glyphicon-menu-left"></span>
						</button>
					</div>
					<div class="col-xs-1 col-xs-offset-2">
						<button type="button" class="btn btn-default" ng-click="page = page+6" ng-disabled="(page+6)>formasPagamento.length">
							<span class="glyphicon glyphicon-menu-right"></span>
						</button>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>  
</div>
  @stop