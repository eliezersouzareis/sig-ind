@extends('main')

@section('content')

<script>
jQuery(function($){
	$('.input-qtd').on('focusout',function(){
		var qtd = parseFloat($('.input-qtd').val());
		var valor = parseFloat($('.input-valor').val());
		$('.input-total').val(parseFloat(valor*qtd));
	});
	$('.input-valor').on('focusout',function(){
		var qtd = parseFloat($('.input-qtd').val());
		var valor = parseFloat($('.input-valor').val());
		$('.input-total').val(parseFloat(valor*qtd));
	});
});
</script>

<div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Editar Conta a Pagar</b>
		</div>
		<div class="panel-body">
			{{ Form::model($conta, array('method' => 'PATCH', 'route' =>
 array('contas.update', $conta->id))) }}{{ Form::open(array('route' => 'contas.store')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-2">
							{{ Form::label('data', 'Data da Conta:') }}
							{{ Form::input('date','data', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4">
							{{ Form::label('fornecedor', 'Fornecedor') }}
							{{ Form::text('fornecedor', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::label('conta', 'Conta') }}
							{{ Form::text('conta', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::label('sub_conta', 'Sub conta') }}
							{{ Form::text('sub_conta', null, array('class' => 'form-control')) }}
						</div>
					</div>
					<br/>
					<fieldset>
						<div class="row">
							<div class="col-md-12">
								<legend>Itens da Conta:</legend>	
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								{{ Form::label('nota', 'Nota:') }}
								{{ Form::text('nota', null, array('class' => 'form-control')) }}
							</div>
							<div class="col-md-3">
								{{ Form::label('valor', 'Valor Unitário:') }}
								{{ Form::text('valor', null, array('class' => 'form-control input-valor')) }}
							</div>
							<div class="col-md-3">
								{{ Form::label('quantidade', 'Quantidade:') }}
								{{ Form::text('quantidade', null, array('class' => 'form-control input-qtd')) }}
							</div>
							<div class="col-md-3">
								{{ Form::label('valor_total', 'Valor Total:') }}
								{{ Form::text('valor_total', null, array('class' => 'form-control input-total', 'readonly')) }}
							</div>
						</div>
					</fieldset>
					<hr/>
				</div>
				<a href="{{ URL::route('contas.index') }}" class="btn btn-warning">Cancelar</a>
				{{ Form::submit('Salvar e Adicionar Parcela', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>
</div>

@stop