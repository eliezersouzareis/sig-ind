@extends('main')

@section('content')

<div ng-controller="AdicionarContaReceber">
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Nova Conta a Receber</b>
		</div>
		<div class="panel-body">
			{{ Form::open(array('route' => 'contasreceber.store')) }}
				<div class="form-group">
					<div class="row">
						<div class="col-md-2">
							{{ Form::label('data', 'Data da Conta:') }}
							{{ Form::input('date','data', null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-10">
							<input type="text" class="hidden" name="cliente_id" ng-model="cliente.id" />
							<label>Cliente:</label>
							<div class="input-group">
								<input type="text" class="form-control" ng-model="cliente.nome" readonly/>
								<span class="input-group-btn">
									<button type="button" class="btn btn-primary">
										<span class="glyphicon glyphicon-search"></span>
									</button>
								</span>
							</div>
						</div>
					</div>
					<br/>
					<fieldset>
						<div class="row" ng-init="count=0;">
							<div class="col-md-12">
								<legend>
									Itens da Conta:
								</legend>	
							</div>
						</div>
						<table class="table">
							<thead>
								<tr class="active">
									<th class="col-md-6">Cód. Pedido</th>
									<th class="col-md-4">Valor</th>
									<th class="col-md-2">
										<button type="button" class="btn btn-primary btn-sm">
											<span class="glyphicon glyphicon-plus"></span>
											Selecionar Pedido
										</button>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="pedido in pedidosSel">
									<td>
										[[ pedido.codigo ]]
									</td>
									<td>
										R$ [[ pedido.total ]]	
									</td>
									<td>
										<button type="button" class='btn btn-danger btn-sm' ng-click="delPedido(pedido.id)">
											<span class="glyphicon glyphicon-remove"></span>
											Remover
										</button>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="row">
							<div class="col-md-1 col-md-offset-8">
								Total:
							</div>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
										R$
									</span>
									<input type="text" name="valor_total" class="form-control" value="[[ sum() ]]"/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-1 col-md-offset-8">
								Desconto:
							</div>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
										R$
									</span>
									<input type="text" name="desconto" class="form-control" value="" ng-model="desconto"/>
								</div>
							</div>
						</div>
					</fieldset>
					<hr/>
				</div>
				<a href="{{ URL::previous() }}" class="btn btn-warning">Cancelar</a>
				{{ Form::submit('Salvar e Adicionar Parcela', array('class' => 'btn btn-primary')) }}
			{{ Form::close() }}
		</div>
	</div>
</div>

@stop