@extends('main')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Contas a Receber</b>
			<div class="pull-right">
				<a href="{{ URL::route('contasreceber.create') }}" class="btn btn-default btn-xs">Nova Conta a Receber</a>
			</div>
		</div>
		<div class="panel-body">
		</div>

		<table class="table table-hover">
			<tr class="active">
				<th class="col-md-2">Data</th>
				<th class="col-md-4">Cliente</th>
				<th class="col-md-3">Valor</th>
				<th class="col-md-3">Opções</th>
			</tr>
			@foreach ($contas as $conta)
				<?php 
					$isEditable = true;
					$isPaid = false;
					$sum = 0; $count = 0;	
					foreach($conta->parcelas as $parcela)
					{
						if($parcela->data_pagamento && $isEditable)
						{
							$isEditable = false;
						}
						if($parcela->valor_pago)
						{
							$sum += $parcela->valor_pago;
							$count++;
						}
					}
					if($sum >= $conta->valor_total && $count == $conta->parcelas->count())
					{
						$isPaid = true;
					}
				?>			
				@if($isPaid)
					<tr class="active text-muted">
				@else
					<tr>
				@endif
				<td>{{ date("d/m/Y",strtotime($conta->data)) }}</td>
				<td>{{ $conta->cliente->nome }}</td>
				<td>{{ $conta->valor_total }}</td>
				<td>
					<a href="{{ URL::route('contasreceber.show', $conta->id) }}" class="btn btn-sm btn-success">
						<span class="glyphicon glyphicon-eye-open"></span>
						Ver
					</a>
					@if($isPaid)
						<span class="glyphicon glyphicon-ok"></span>
						Recebido
					@endif
					@if($isEditable)
					<a href="{{ URL::route('contasreceber.edit', $conta->id) }}" class="btn btn-sm btn-primary">
						<span class="glyphicon glyphicon-pencil"></span>
						Editar
					</a>
					{{ Form::open(array('method' => 'DELETE', 'route' => array('contasreceber.destroy', $conta->id), 'class' => 'force-inline-block')) }}                       
						<button type="submit" class="btn btn-danger btn-sm">
							<span class="glyphicon glyphicon-remove"></span>
							Remover
						</button>
					{{ Form::close() }}
					@endif	
						
				</td>
			</tr>
			@endforeach
		</table>
		
		<div class="panel-footer" style="text-align: right;"><b>Total de Contas Encontradas: {{ $contas->count() }}</b></div>
	</div>    

@stop