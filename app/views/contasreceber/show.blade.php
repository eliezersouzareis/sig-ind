@extends('main')

@section('content')
<div ng-controller="ViewContaPagar">

	<h4 class="page-header">
		Conta a Receber
	</h4>
	<div class="row">
		<div class="col-md-2">
			<b>Data:</b></br>
			{{ date('d/m/Y',strtotime($conta->data)) }}
		</div>
		<div class="col-md-6">
			<b>Cliente:</b></br>
			{{ $conta->cliente->nome }}
		</div>
		<div class="col-md-4">
			<b>Total:</b></br>
			R$ {{ $conta->valor_total }}
		</div>
	</div>
	</br>
	
	<h4 class="page-header">
		Itens da Conta
		<small>
			<a href="#itensConta" data-toggle="collapse" href="#itensConta" aria-expanded="true" aria-controls="itensConta"
				class="btn btn-xs btn-default">
				<span class="glyphicon glyphicon-chevron-down"></span>
			</a>
		</small>
	</h4>
	
	<div class="collapse in" id="itensConta">
		<table class="table table-default">
			<thead>
				<tr>
					<th class="col-md-3">Data do Pedido</th>
					<th class="col-md-6">Código</th>
					<th class="col-md-3">Valor</th>
				</tr>
			</thead>
			<tbody>
			@foreach($conta->itens as $item)
				<tr>
					<td>{{ date('d/m/Y',strtotime($item->data)) }}</td>
					<td>
						{{ $item->codigo }}
						<a href="{{ URL::route('pedidos.show', $item->id) }}" class="btn btn-xs btn-success">
							<span class="glyphicon glyphicon-eye-open"></span>
						</a>
					</td>
					<td>R$ {{ $item->total }}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
	</br>
	<h4 class="page-header">
		Parcelas
		<span class="pull-right">
			<a href="{{ URL::route('contasreceber.parcelas.create',$conta->id) }}" class="btn btn-sm btn-primary">
				<span class="glyphicon glyphicon-plus"></span>
				Criar Parcelas
			</a>
		</span>
	</h4>
	<?php
		$parcelas = $conta->parcelas->sortBy('data_pagamento');
		$pago  = $conta->parcelas->sum('valor_pago');
		$total = $conta->parcelas->sum('valor');

	?>
	<b>Pago:</b> R$ {{ $pago }} <b>Restante:</b> R$ {{ $total - $pago }}
	<table class="table table-default">
		<thead>
			<tr>
				<th class="col-md-2">Vencimento</th>
				<th class="col-md-3">Valor</th>
				<th class="col-md-2">Valor pago</th>
				<th class="col-md-3">Forma de Pagamento</th>
				<th class="col-md-2"></th>
			</tr>
		</thead>
		<tbody>
		@foreach($parcelas as $parcela)
			@if($parcela->data_pagamento)
				<tr class="active">
					<td class="text-muted">{{ date("d/m/Y", strtotime($parcela->data_vencimento)) }}</td>
					<td class="text-muted">R$ {{ $parcela->valor }}</td>
					<td class="text-muted">R$ {{ $parcela->valor_pago }}</td>
					<td class="text-muted">{{ $parcela->formaPagamento->codigo." ".$parcela->formaPagamento->nome }}</td>
					<td class="text-muted">
						<span class="glyphicon glyphicon-ok"></span>
						Recebido
					</td>
				</tr>
			@else
				<tr>
					<td>{{ date('d/m/Y', strtotime($parcela->data_vencimento)) }}</td>
					<td>R$ {{ $parcela->valor }}</td>
					<td></td>
					<td>{{ $parcela->formaPagamento->codigo.' '.$parcela->formaPagamento->nome }}</td>
					<td>
						<a role="button" class="btn btn-success btn-sm" href="{{ URL::route('contasreceber.parcelas.receber',[$conta->id,$parcela->id]) }}">
							<span class="glyphicon glyphicon-usd"></span>
							Receber
						</a>
						<a role="button" class="btn btn-primary btn-sm" href="{{ URL::route('contasreceber.parcelas.edit',[$conta->id,$parcela->id]) }}">
							<span class="glyphicon glyphicon-pencil"></span>
							Editar
						</a>
					</td>
				</tr>
			@endif
		@endforeach
		</tbody>
	</table>

</div>

@stop

