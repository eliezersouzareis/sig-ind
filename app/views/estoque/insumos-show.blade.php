<div class="row">
	<div class="col-xs-4"><b>Unidade Primária</b></div>
	<div class="col-xs-8">{{ $insumo->unidadePrimaria->nome }}</div>
</div>
<br />
<div class="panel panel-default">
		<table class="table">
			<tr class="active">
				<th>Unidade</th>
				<th>Quantidade Estoque</th>
			</tr>
		@foreach($insumo->unidades as $row)
			<tr>
				<td>{{ $row->nome }}</td>
				<td>{{ $row->pivot->qtd*$insumo->qtd_estoque }}&nbsp;{{ $row->abreviatura }}</td>
			</tr>
		@endforeach
		</table>
</div>