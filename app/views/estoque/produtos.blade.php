@extends('main')


@section('content')
	<div class="panel panel-default">
	    <div class="panel-heading">
	        <b style="font-size:24px;">Produtos</b>
	        <div class="pull-right">
	            <a href="{{ URL::route('producao.create') }}" class="btn btn-default">Criar Ordem de Produçao</a>
	            <a href="{{ URL::route('adicao_manual.create') }}" class="btn btn-default">Adicionar Manualmente</a>
	        </div>
	    </div>
	    <table class="table table-hover" style="font-size:18px;">
	        <tr class="active">
	            <th class="col-md-1">Cód</th>
	            <th class="col-md-8">Produto</th>
	            <th class="col-md-3" colspan="2">Quantidade em Estoque</th>
	        </tr>
	        @foreach ($produtos as $produto)
	        <tr>
	        	<td>{{ $produto->codigo }}</td>
	            <td>{{ $produto->nome }}</td>
	            <td>{{ $produto->qtd_estoque }}</td>
	            <td>{{ $produto->unidade->abreviatura }}</td>
	        </tr>
	        @endforeach
	    </table>
	    
	    <div class="panel-footer" style="text-align:right;"><b>Total Produtos Cadastrados: {{ Produto::count() }}</b></div>
	</div>
@stop