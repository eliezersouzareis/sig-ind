@extends('main')


@section('content')
<div ng-controller="MostrarItem">
	<div class="panel panel-default">
	    <div class="panel-heading">
	        <b style="font-size:24px;">Insumos</b>
	        <div class="pull-right">
	            <a href="{{ URL::route('compras.create') }}" class="btn btn-default">Registrar Compra</a>
	        </div>
	    </div>
	    <table class="table table-striped table-hover clearfix" style="font-size:18px;">
	    	<thead class="row">
		        <tr class="active">
		            <th class="col-md-9">Insumo</th>
		            <th class="col-md-3" colspan="2">Quantidade</th>
		        </tr>
		    </thead>
		    <tbody>
	        @foreach ($insumos as $insumo)
		        <tr data-toggle="modal" data-target="#modalItem"
	        	ng-click="getItem('{{ $insumo->nome }}','{{ URL::route('estoque.insumos.show',['id' => $insumo->id]) }}')" role="button">
		            <td>{{ $insumo->nome }}</td>
		            <td style="text-align: right;">{{ $insumo->qtd_estoque }}</td>
		            <td>{{ $insumo->unidadePrimaria->abreviatura }}</td>
		        </tr>
		    @endforeach
	        </tbody>
	    </table>	    
	    <div class="panel-footer" style="text-align:right;"><b>Total de Insumos Cadastrados: {{ Insumo::count() }}</b></div>
	</div>

	<!-- Modal Item -->
	<div class="modal fade" id="modalItem" tabindex="-1" role="dialog" 
 	aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="modalLabel">[[ modalTitle ]]</h4>
				</div>
				<div class="modal-body" ng-bind-html="item">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
				</div>				
			</div>
		</div>
	</div>
</div>

@stop