@extends('config')

@section('tabContent')
<script type="text/javascript">
	$("#tabulacao li a[href='{{ URL::route('config.backup.index') }}']").parent().addClass('active');;
</script>
	<div class="row">
	<h3 class="page-header">Criar backup</h3>
		<div class="panel panel-default">
			<div class="panel-body">
				<b>Última data de backup: </b>{{ $lastTime }}
			</div>
		</div>
		<a href="{{ URL::route('config.backup.create') }}" class="btn btn-primary btn-md">
			Fazer backup
		</a>
	</div>
	<br />
	<div class="row">
	<h3 class="page-header"> Restaurar um backup</h3>
		<div class="alert alert-danger alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>			
			<span class="glyphicon glyphicon-alert"></span>
			<b>ATENÇÃO!</b> ao restaurar um backup você está <b>SUBSTITUINDO</b> os seus dados pelos do arquivo selecionado
		</div>
		{{ Form::open(array('route' => 'config.backup.restore', 'files' => 'true')) }}
			{{ Form::file('arquivo', array('accept' => '.sql')) }}
			{{ Form::submit('Restaurar', array('class' => 'btn btn-primary','ng-click' => 'clicked = true;')) }}
		{{ Form::close() }}
	</div>
	<br />
	<div class="progress" ng-show="clicked">
		<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100'" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
			<span class="sr-only">please wait...</span>
		</div>
	</div>

	<br />
	<div class="row">
	<h3 class="page-header">Esvaziar banco de dados</h3>
		<div class="alert alert-danger alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>			
			<span class="glyphicon glyphicon-alert"></span>
			<b>ATENÇÃO!</b> você irá <b>APAGAR</b> os seus dados!
		</div>
		{{ Form::open(array('route'	=> array('config.backup.reset'))) }}                       
			{{ Form::submit('Esvaziar banco de dados', array('class' => 'btn btn-primary')) }}
		{{ Form::close() }}
	</div>
	<br />


@stop