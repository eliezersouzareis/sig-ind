<?php

class VeiculosController extends BaseController {

	public function index()
	{
		$veiculos = Veiculo::all();

		return View::make('veiculos.index', compact('veiculos'));
	}

	public function create()
	{
		return View::make('veiculos.create');
	}

	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Veiculo::$rules);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}

		Veiculo::create($input);

		return Redirect::route('veiculos.index');
	}

	public function show($id)
	{
		return Veiculo::find($id);
	}

	public function edit($id)
	{
		$veiculo = Veiculo::find($id);
		
		if (is_null($veiculo))
		{
			return Redirect::route('veiculos.index');
		}

		return View::make('veiculos.edit', compact('veiculo'));
	}

	public function update($id)
	{
		$input = Input::all();
 
		$veiculo = Veiculo::find($id);
		$veiculo->update($input);

		return Redirect::route('veiculos.index');
	}

	public function destroy($id)
	{
		Veiculo::find($id)->delete();
		return Redirect::route('veiculos.index');
	}

}