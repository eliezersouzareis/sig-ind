<?php

class ContasPagarController extends BaseController {

	public function index()
	{

		$contasPagar = ContaPagar::orderBy('data','desc')->get();

		return View::make('contaspagar.index', compact('contasPagar'));
	}

	public function create()
	{
		return View::make('contaspagar.create');
	}

	public function store()
	{
		$input = Input::all();
		if(Input::has('itens'))
			$itens = array_pull($input, 'itens');
		ContaPagar::create($input);
		$conta_id = DB::getPdo()->lastInsertId();
		
		$conta = ContaPagar::find($conta_id);

		if(Input::has('itens'))
		{
			
			foreach($itens as $item)
			{
				$contaItem = new ContaPagarItem;
				$contaItem->conta_pagar_id = $conta_id;
				$contaItem->nome = $item['nome'];
				$contaItem->valor = $item['valor'];
				$contaItem->qtd = $item['qtd'];
				$contaItem->save();
			}
		}

		return Redirect::route('contaspagar.parcelas.create',[$conta->id]);
	}

	public function show($conta)
	{	
		return View::make('contaspagar.show',compact('conta'));
	}

	public function edit($conta)
	{
		
		if (is_null($conta))
		{
			return Redirect::route('contaspagar.index');
		}

		return View::make('contaspagar.edit', compact('conta'));
	}

	public function update($conta)
	{
		$input = Input::all();
		//echo "<pre>";
		if(Input::has('itens'))
			$itens = array_pull($input, 'itens');

		//$conta = ContaPagar::find($id);
		$conta->update($input);
		$itensExistentes = $conta->itens;

		if(Input::has('itens'))
		{
			//e quando você possuir mais itens mas que na edição foram substituidos??
			$aux = $conta->itens->lists('id');			
				  //$conta->itens->keys()
			foreach($itens as $itemId => $item)
			{
				if($itensExistentes->contains((int)$itemId))
				{
					$pos = array_search((int)$itemId, $aux);
					if(!($pos === false)) 
					{
						array_splice($aux, $pos, 1);
					}
					$contaItem = ContaPagarItem::find($itemId);
					$contaItem->update($item);
				} else {
					$contaItem = new ContaPagarItem;
					$contaItem->conta_pagar_id = $conta->id;
					$contaItem->nome = $item['nome'];
					$contaItem->valor = $item['valor'];
					$contaItem->qtd = $item['qtd'];
					$contaItem->save();
				}
			}
			foreach($aux as $id)
			{
				ContaPagarItem::find($id)->delete();
			}
			
		}
		//Session::put('contapagar',$conta->id);
		return Redirect::route('contaspagar.parcelas.create',$conta->id);
/*

		
		if(Input::has('parcela'))
		{
			foreach($parcelas as $parcela)
			{
				if(!isset($parcela['data_vencimento']))
				{
					$parcela['data_vencimento'] = date('Y-m-d');
				}
				Parcela::create($parcela);
				$parcela_id = DB::getPdo()->lastInsertId();
				$conta->parcelas()->attach($parcela_id);
			}
		}*/

	}

	public function destroy($conta)
	{
		$conta->delete();
		return Redirect::route('contaspagar.index');
	}

}