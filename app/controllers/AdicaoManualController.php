<?php

class AdicaoManualController extends BaseController {

	public function index()
	{
		//
	}


	public function create()
	{
		return View::make('adicaomanual.create');
	}

	public function store()
	{
		$input = Input::all();
		$input['data'] = date('Y-m-d');
		AdicaoManual::create($input);
		$adMan_id = DB::getPdo()->lastInsertId();
		$adicaoManual = AdicaoManual::find($adMan_id);

		$produto = Produto::find($adicaoManual->produto_id);
		$produto->qtd_estoque += $adicaoManual->qtd;
		$produto->save();

		return Redirect::to('estoque/produtos');
	}

	public function show($id)
	{
		//
	}

	public function edit($id)
	{
		//
	}

	public function update($id)
	{
		//
	}

	public function destroy($id)
	{
		$adMan = AdicaoManual::find($id);
		$produto = Produto::find($adMan->produto_id);
		$produto->qtd_estoque -= $adMan->qtd;
		$produto->save();
		return Redirect::to('estoque/produtos');
	}

}