<?php

class ClientesController extends BaseController {

	public function index()
	{
		$clientes = Cliente::all();

		return View::make('clientes.index', compact('clientes'));
	}

	public function create()
	{
		return View::make('clientes.create');
	}

	public function store()
	{
		$input = Input::all();
		$endereco = array_pull($input, 'endereco');
		$validation = Validator::make($input, Cliente::$rules);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}
		$endereco['nome'] = 'Matriz';
		$endereco['telefone'] = $input['telefone_fixo'];
		$validation = Validator::make($endereco, Endereco::$rules);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}
		Cliente::create($input);
		$cliente_id = DB::getPdo()->lastInsertId();
		$cliente = Cliente::find($cliente_id);

		Endereco::create($endereco);
		$endereco_id = DB::getPdo()->lastInsertId();

		$cliente->endereco_id = $endereco_id;
		$cliente->save();

		return Redirect::route('clientes.index')->with('success','Cadastro realizado com sucesso.');
	}

	public function show($id)
	{
		$cliente = Cliente::find($id);
		return View::make('clientes.show', compact('cliente'));
	}

	public function edit($id)
	{
		$cliente = Cliente::find($id);
		$cliente['endereco'] = $cliente->matriz;
		
		if (is_null($cliente))
		{
			return Redirect::route('clientes.index');
		}

		return View::make('clientes.edit', compact('cliente'));
	}

	public function update($id)
	{
		$input = Input::all();
		$endereco = array_pull($input, 'endereco');
 
		$cliente = Cliente::find($id);
		$cliente->update($input);
		
		if($cliente->endereco_id)
		{
			$endereco_ = Endereco::find($cliente->endereco_id);
			$endereco_->update($endereco);
		}else{
			Endereco::create($endereco);
			$endereco_id = DB::getPdo()->lastInsertId();

			$cliente->endereco_id = $endereco_id;
			$cliente->save();			
		} 

		return Redirect::route('clientes.index');
	}

	public function destroy($id)
	{
		$cliente = Cliente::find($id);
		$filiais = $cliente->filiais;
		foreach ($filiais as $filial) 
		{
			$endereco_id = $filial->id;
			Endereco::find($endereco_id)->delete();
		}
		if($cliente->endereco_id)
			Endereco::find($cliente->endereco_id)->delete();
		$cliente->delete();
		return Redirect::route('clientes.index');
	}

}