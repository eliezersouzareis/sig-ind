<?php

class UsersController extends BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /users
	 *
	 * @return Response
	 */
	public function index()
	{
		$usuarios = User::orderBy('id')->get();
		return View::make('usuarios.index',compact('usuarios'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /users/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('usuarios.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /users
	 *
	 * @return Response
	 */
	public function store()
	{
		$user = Input::all();
		$user = array_except($user, array('confirmar'));
		$user['password'] = Hash::make($user['password']);

		User::create($user);
		$user_id = DB::getPdo()->lastInsertId();

		DB::update('update users set password = ? where id = ?',array($user['password'],$user_id));
		
		return Redirect::route('config.usuarios.index');
	}

	/**
	 * Display the specified resource.
	 * GET /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$usuario = User::find($id);
		return View::make('usuarios.show',compact('usuario'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /users/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$usuario = User::find($id);
		return View::make('usuarios.edit',compact('usuario'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = Input::all();
		$user = array_except($user, array('confirmar'));
		$user['password'] = Hash::make($user['password']);

		User::find($id)->update($user);
		
		DB::update('update users set password = ? where id = ?',array($user['password'],$id));
		
		return Redirect::route('config.usuarios.index');
	}
	
	/**
	 * Remove the specified resource from storage.
	 * DELETE /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		User::find($id)->delete();
		return Redirect::route('config.usuarios.index');
	}

}