
<?php

class ProdutosController extends BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /produtos
	 *
	 * @return Response
	 */
	public function index()
	{
		$produtos = Produto::orderBy('nome')->get();
		return View::make('produtos.index',compact('produtos'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /produtos/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$unidades =  Unidade::orderBy('nome')->get();
		$aux = array();
		foreach($unidades as $unidade)
		{
			$aux[$unidade->id] = $unidade->nome;
		};
		$unidades = $aux;
		return View::make('produtos.create',compact('unidades'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /produtos
	 *
	 * @return Response
	 */
	public function store()
	{	
		$input = Input::all();
		if(Input::has('insumos'))
			$insumos = array_pull($input, 'insumos');
		$input['qtd_estoque'] = 0.00;
		$validation = Validator::make($input, Produto::$rules);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}
		Produto::create($input);
		
		if(Input::has('insumos'))
		{
			$produto_id = DB::getPdo()->lastInsertId();
			$produto = Produto::find($produto_id);
			
			foreach($insumos as $insumo_id => $insumo)
			{
				$insumo_id =(int) $insumo_id;
				$produto->insumos()->attach($insumo_id, array('unidade_id' => (int)$insumo['unidade'],'qtd' => (float)$insumo['quantidade']));
			}
		}

		return Redirect::route('produtos.index');
	}

	/**
	 * Display the specified resource.
	 * GET /produtos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$produto = Produto::find($id);
        
        if (is_null($produto))
        {
            return '<pre>Produto não existe!</pre>';
        }		

		$unidades =  Unidade::all();
		
		return View::make('produtos.show', compact('produto','composicao','unidades'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /produtos/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

    	$produto = Produto::find($id);
        
        if (is_null($produto))
        {
            return Redirect::route('produtos.index');
        }

		$unidades =  Unidade::orderBy('nome')->get();
		$aux = array();
		foreach($unidades as $unidade)
		{
			$aux[$unidade->id] = $unidade->nome;
		};
		$unidades = $aux;
        
        return View::make('produtos.edit', compact('produto', 'unidades'));	

	}

	/**
	 * Update the specified resource in storage.
	 * PUT /produtos/{id}
	 *
	 * @param  int  $id1
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		if(Input::has('insumos'))
			$insumos = array_pull($input, 'insumos');
		
		$produto = Produto::find($id);
		$produto->update($input);

		$produto->insumos()->detach();
		if(Input::has('insumos'))
			foreach($insumos as $insumo_id => $insumo)
			{
				$insumo_id =(int) $insumo_id;
				$produto->insumos()->attach($insumo_id, array('unidade_id' => (int)$insumo['unidade'],'qtd' => (float)$insumo['quantidade']));
			}

		return Redirect::route('produtos.index');	
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /produtos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Produto::find($id)->delete();
		return Redirect::route('produtos.index');
	}

}