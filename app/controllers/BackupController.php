<?php

class BackupController extends BaseController {

	public function index()
	{
		$lastTime = Backup::all();

		if($lastTime->isEmpty())
		{
			$lastTime = "Não há";
		}else
		{
			$lastTime = $lastTime->last()['created_at'];
			$lastTime = date("d/m/Y à\s H:i:s", strtotime($lastTime));
		}

		return View::make('backup.index', compact('lastTime'));
	}

	public function create()
	{
		Backup::create(array());
		$blacklisted = [
			'users',
			'categoria_conta',
			'forma_pagamento'
		];
		$document = "";
		$document .= "--\n-- Backup executado em ".date("d/m/Y")."\n";
		$document .= "-- Banco de dados: ".DB::getConfig("database")."\n";
		$document .= "--\n\n";
		$document .= "SET FOREIGN_KEY_CHECKS=0;\n\n";
		$tables = DB::select("SHOW TABLES");
		foreach ($tables as $table)
		{
			$tableName = $table->{"Tables_in_".DB::getConfig("database")};
			if(in_array($tableName, $blacklisted)) //não pega tabelas 
			{
				continue;
			}
			//pega o nome das colunas
			$columns = DB::select("SHOW COLUMNS FROM ".$tableName);
			$colLen = count($columns);
			//pega os dados da tabela
			$rows = DB::table($tableName)->get();
			$rowLen = count($rows);
			//se a tabela está vazia não há porque pega-la
			if($rowLen == 0)
			{
				continue;
			}
			$tableData = "";
			$tableData .= "--\n--  Tabela: ".$tableName."\n--\n";
			$tableData .= "LOCK TABLES `".$tableName."` WRITE;\n";
			$tableData .= "/*!40000 ALTER TABLE `".$tableName."` DISABLE KEYS */;\n";
			$tableData .= "TRUNCATE `".$tableName."`;\n";
			$tableData .= "INSERT INTO `".$tableName."` \n(";
			
			foreach ($columns as $key => $column) {
				$tableData .= "`".$column->Field."`".(($key ==$colLen-1)?"":", ");
			}
			$tableData .= ")\n VALUES \n";
			foreach($rows as $rkey => $row)
			{
				$tableData .= "(";
				foreach ($columns as $key => $column)
				{	
					$type = gettype($row->{$column->Field});
					if(strcmp($type, "NULL") == 0)
					{
						$tableData .= "NULL".($key == $colLen-1?"":", ");
					}else
					if(strcmp($type, "string") == 0)
					{
						$row->{$column->Field} = str_replace("\"","\\\"",$row->{$column->Field});
						$row->{$column->Field} = str_replace("'","\\'",$row->{$column->Field});
						$tableData .= "'".$row->{$column->Field}."'".($key == $colLen-1?"":", ");
					}else
						$tableData .= $row->{$column->Field}.($key == $colLen-1?"":", ");
				}
				$tableData .= ")".($rkey == $rowLen-1?";":",\n");
			}
			$tableData .= "\n/*!40000 ALTER TABLE `".$tableName."` ENABLE KEYS */;\n";
			$tableData .= "UNLOCK TABLES;\n\n";
			$document .= $tableData;
		}
		$document .= "SET FOREIGN_KEY_CHECKS=1;\n\n";
		header("Content-Type: text/plain");
		header("Content-Disposition: attachment; filename=backup".date("Y-m-d").".sql");
		echo $document;	
	}

	public function restore()
	{
		if(Input::hasFile('arquivo'))
		{
			$file = Input::file('arquivo');
			if($file->isValid() && (strcmp($file->getClientOriginalExtension(),'sql') == 0))
			{
				$handle = fopen($file->getRealPath(), 'r');
				$contents = fread($handle, $file->getSize());
				fclose($handle);
				unlink($file->getRealPath());				
	    			
					/*
					$tables = DB::select("SHOW TABLES");
					DB::statement("SET FOREIGN_KEY_CHECKS=0");
					foreach($tables as $table)
					{
						$tableName = $table->{"Tables_in_".DB::getConfig("database")};
						if(strcmp($tableName, "users") == 0) //não deleta tabela usuarios
						{
							continue;
						}
						DB::delete("DELETE FROM `".$tableName."` WHERE 1");
					}
					DB::statement("SET FOREIGN_KEY_CHECKS=1");/* */
					
					//parse file contents
					$aux = array();
					$queries = explode("\n", $contents);
					//select meaningful queries
					foreach($queries as $query)
					{
						if(strstr($query, "--") == false &&
							strstr($query, "/*") == false &&
							strstr($query, "*\/") == false &&
							strlen($query) != 0)
						{ //string appears to be a meaningful query
							$aux[] = $query;
						}
					}
					$queries = $aux;
					$aux = array(); $counter = 0;
					$aux[$counter] = "";
					$qLen = count($query);
					foreach($queries as $key => $query)
					{
						if(strstr($query, ";"))
						{
							$aux[$counter++] .= $query;
							$aux[$counter] = "";
						continue;
						}
						$aux[$counter] .= $query;
					}
					$queries = $aux;
					
					$pdo = DB::connection()->getPdo();
				
					
					foreach($queries as $query)
					{
						try 
						{
							if(!empty($query))
							{
								$pdo->beginTransaction();	
								$pdo->exec($query);
								$pdo->commit();
							}
						} catch (PDOException $e) {
    						$pdo->rollBack();
							return Redirect::route('config.backup.index')->with('fail','O sql executado contém algum erro. Analise o arquivo.')
											->withErrors([$e->getMessage(), 'Query: '.$query]);
						}
					} /* */
				
    				
				
				return Redirect::route('config.backup.index')->with('success','O backup foi restaurado.');
			}
			else
			{
				return Redirect::route('config.backup.index')->with('fail','O arquivo recebido contém erros ou não é válido.');
			}
		}
		else
		{
			return Redirect::route('config.backup.index')->with('fail','O servidor não recebeu arquivo algum.');
		}
		return Redirect::route('config.backup.index')->with('fail','Algo errado ocorreu no framework.');
	}

	public function reset()
	{
		//define('STDIN',fopen("php://stdin","r"));
		try{
			$query = 'DROP DATABASE `sig-ind`; CREATE DATABASE `sig-ind`;';
			DB::getPdo()->exec($query);
			//Artisan::call('migrate',array('--pretend'=>true, '--force'=>true, '--path' => 'app/database/migrations'));
			//Artisan::call('db:seed',array('--force'=>true));
		}
		catch(Exception $e)
		{
			Response::make($e->getMessage(), 500);
		}
		return Redirect::route('config.backup.index')->with('success','Banco de dados limpo e restabelecido com sucesso.');
	}

}