<?php

class PedidosController extends BaseController {

	public function index()
	{
		$input = Input::all();
		if(!$input)
		{
			$dateA = new DateTime(date('Y').'-'.date('m').'-1');
			$dateB = new DateTime(date('Y').'-'.date('m').'-1');
			$dateB = $dateB->add(new DateInterval('P1M'));
			$dateB = $dateB->sub(new DateInterval('P1D'));
			$dateA = $dateA->format('Y-m-d');
			$dateB = $dateB->format('Y-m-d');

			$data = date_parse(date('Y-m-d'));

			$pgNum = 1;

		}else 
		{
			if($input['mes'] > 12)
			{
				$input['mes'] = 1;
				$input['ano'] +=1; 
			}
			if($input['mes'] < 1 )
			{
				$input['mes'] = 12;
				$input['ano'] -=1;
			}
			
			$dateA = new DateTime($input['ano'].'-'.$input['mes'].'-1');
			$dateB = new DateTime($input['ano'].'-'.$input['mes'].'-1');;
			$dateB = $dateB->add(new DateInterval('P1M'));
			$dateB = $dateB->sub(new DateInterval('P1D'));
			$dateA = $dateA->format('Y-m-d');
			$dateB = $dateB->format('Y-m-d');
			
			$data = date_parse($input['ano'].'-'.$input['mes'].'-0');

			if(Input::has('pag'))
			{
				if($input['pag'] < 1)
					$input['pag'] = 1;
				$pgNum = $input['pag'];
			}
			else
				$pgNum = 1;
		}

		$pedidos = Pedido::orderBy('id','desc')->whereBetween('data',[$dateA, $dateB]);
		$monthCount = $pedidos->count();
		$pedidos = $pedidos->skip(($pgNum-1)*10)->take(10)->get();
		//->paginate();
		return View::make('pedidos.index', compact('pedidos','monthCount', 'data', 'pgNum'));
	}

	public function create()
	{
		$funcionarios = Funcionario::orderBy('nome', 'asc')->lists('nome','id');
		$veiculos = Veiculo::orderBy('placa', 'asc')->lists('placa','id');
		$data= date_parse(date('Y-m-d'));
		
		$dateA = new DateTime($data['year'].'-'.$data['month'].'-1');
		$dateB = new DateTime($data['year'].'-'.$data['month'].'-1');
		$dateB = $dateB->add(new DateInterval('P1M'));
		$dateB = $dateB->sub(new DateInterval('P1D'));
		$dateA = $dateA->format('Y-m-d');
		$dateB = $dateB->format('Y-m-d');

		$count = Pedido::whereBetween('data',[$dateA, $dateB])->count();
		$count +=1;
		$pedidoCod = $count."(".sprintf("%02d",$data['month'])."/".$data['year'].")";
		$pedido = Pedido::where('codigo','=', $pedidoCod)->get();
		while(!$pedido->isEmpty())
		{
			$count++;
			$pedidoCod = $count."(".sprintf("%02d",$data['month'])."/".$data['year'].")";
			$pedido = Pedido::where('codigo','=', $pedidoCod)->get();
		}
		return View::make('pedidos.create', compact('funcionarios','veiculos','pedidoCod'));
	}

	public function store()
	{
		//echo "<pre>";
		$input = Input::all();
		$validation = Validator::make($input, Pedido::$rules);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}

		if(Input::has('efetivado'))
		{    
			if($input['efetivado'] == 'on')
				$input['efetivado'] = true ;
			else
			$input['efetivado'] = false; 
		}
		else
			$input['efetivado'] = false; 
		
		//deve se fazer validação de produtos
		//deve se fazer a validação da entrega
		$entrega = array_pull($input,'entrega');
		$produtos = array_pull($input,'produtos');
		Pedido::create($input);
		$pedido_id = DB::getPdo()->lastInsertId();
		$pedido = Pedido::find($pedido_id);

		Entrega::create($entrega);
		$entrega_id = DB::getPdo()->lastInsertId();
		$pedido->entrega_id = $entrega_id;
		$pedido->save();


		foreach($produtos as $produto_id => $produto)
		{
			$produto_id =(int) $produto_id;

			$produto_ = Produto::find($produto_id);
			$produto_->preco = (float)$produto['preco'];
			$produto_->save();

			$pedido->produtos()->attach($produto_id, ['qtd' => (float)$produto['qtd'],'preco' => (float)$produto['preco']]);
		}

		if($input['efetivado'])
		{
			foreach($pedido->produtos as $item)
			{
				$produto = Produto::find($item->id);
				if($produto->qtd_estoque <= 0)
				{
					$ordemProd = new Producao;
					$data= date_parse(date('Y-m-d'));

					$dateA = new DateTime($data['year'].'-'.$data['month'].'-1');
					$dateB = new DateTime($data['year'].'-'.$data['month'].'-1');
					$dateB = $dateB->add(new DateInterval('P1M'));
					$dateB = $dateB->sub(new DateInterval('P1D'));
					$dateA = $dateA->format('Y-m-d');
					$dateB = $dateB->format('Y-m-d');

					$count = Producao::whereBetween('data', [$dateA, $dateB])->count();
					$count +=1;
					$ordemProd->data = date('Y-m-d');
					$ordemProd->codigo = $count."(".sprintf("%02d",$data['month'])."/".$data['year'].")";;

					$producao = Producao::where('codigo','=',$ordemProd->codigo)->get();
					while(!$producao->isEmpty())
					{
						$count++;
						$ordemProd->codigo = $count."(".sprintf("%02d",$data['month'])."/".$data['year'].")";			
						$producao = Producao::where('codigo', '=', $ordemProd->codigo)->get();
					}

					$ordemProd->produto_id = $produto->id;
					$ordemProd->qtd_inicial = $item->pivot->qtd;
					$ordemProd->qtd_final = $item->pivot->qtd;
					$produto->qtd_estoque += $item->pivot->qtd;
					$ordemProd->concluido = true;
					$ordemProd->save();
					//atualizar quantidade de insumos estoque
					$aux = $ordemProd->qtd_inicial / $produto->rendimento; //quantidade que será utilizada de insumos
					$composicao = $produto->insumos->lists('pivot');
					foreach($composicao as $ingrediente)
					{
						$insumo = Insumo::find($ingrediente->insumo_id);
						$unid_conv = $insumo->unidades;
						foreach($unid_conv as $unit)
						{
							if($insumo->unidade_id == $unit->id)
								$qtdUnPad = $unit->pivot->qtd;
							if($ingrediente->unidade_id == $unit->id)
								$qtdUnDes = $unit->pivot->qtd;
						}
						$tot_insumo = (($ingrediente->qtd * $aux) * $qtdUnPad)/$qtdUnDes;
						$insumo->qtd_estoque -= $tot_insumo;
						$insumo->save();
					}
				}
				$produto->qtd_estoque -= $item->pivot->qtd;
				$produto->save();
			}

			//procurar no contas a receber se existe uma conta do cliente que seja da data do pedido
			$contas = ContaReceber::where('cliente_id', '=', $pedido->cliente_id)
								->where('data', '=', $pedido->data)->get();
			if(!$contas->isEmpty())
			{
				$found_id = false;
				foreach($contas as $conta)
				{
					foreach($conta->itens as $item)
					{
						if($item->entrega->endereco->id == $pedido->entrega->endereco->id
							 &&
							$item->produtos[0]->id == $pedido->produtos[0]->id)
						{
							$found_id = true;
							break;
						}
					}
					if($found_id == true)
					{
						$found_id = $conta->id;
						break;
					}
				}
				if($found_id !== false)
				{
					$conta = ContaReceber::find($found_id);
					$conta->valor_total += $pedido->total;
					$conta->itens()->attach($pedido->id);
					$conta->save();
				} else {
					$conta = new ContaReceber;
					$conta->cliente_id = $pedido->cliente_id;
					$conta->data = $pedido->data;
					$conta->valor_total = $pedido->total;
					$conta->save();
					$conta->itens()->attach($pedido->id);
				}

			} else {
				$conta = new ContaReceber;
				$conta->cliente_id = $pedido->cliente_id;
				$conta->data = $pedido->data;
				$conta->valor_total = $pedido->total;
				$conta->save();
				$conta->itens()->attach($pedido->id);
			}

		};

		return Redirect::route('pedidos.index');
	}

	public function show($id)
	{
		$pedido = Pedido::find($id);

		return View::make('pedidos.show', compact('pedido'));
	}

	public function _print($id)
	{
		$pedido = Pedido::find($id);

		return View::make('pedidos.print', compact('pedido'));
	}

	public function edit($id)
	{
		$pedido = Pedido::find($id);
		$funcionarios = Funcionario::orderBy('nome', 'asc')->lists('nome','id');
		$veiculos = Veiculo::orderBy('placa', 'asc')->lists('placa','id');
		
		if (is_null($pedido))
		{
			return Redirect::route('pedidos.index');
		}

		return View::make('pedidos.edit', compact('pedido','funcionarios','veiculos'));
	}

	public function update($id)
	{
		$pedido = Pedido::find($id);
		$input = Input::all();
		
		if(Input::has('efetivado'))
		{    
			if($input['efetivado'] == 'on')
				$input['efetivado'] = true ;
			else
			$input['efetivado'] = false; 
		}
		else
			$input['efetivado'] = false; 
		
		//deve se fazer validação de produtos
		//deve se fazer a validação da entrega
		$entrega = array_pull($input,'entrega');
		$produtos = array_pull($input,'produtos');
		//echo "<pre>";
		//dd($produtos);

		$pedido->update($input);

		$pedido->entrega->update($entrega);

		$produtosPed = $pedido->produtos;
		$aux = $produtosPed->lists('id');
		
		foreach($produtos as $produto_id => $produto)
		{
			if($produtosPed->contains((int)$produto_id))
			{
				$produto_id =(int) $produto_id;
				
				$produto_ = Produto::find($produto_id);
				$produto_->preco = (float)$produto['preco'];
				$produto_->save();

				$pos = array_search($produto_id, $aux);
				if(!($pos === false)) 
				{
					array_splice($aux, $pos, 1);
				}

				$produtosPed->find($produto_id)->update($produto);
			} else {
				//se a quantidade produtos for a mesma, retirar o que não faz parte //todo
				$produto_id =(int) $produto_id;
				
				$produto_ = Produto::find($produto_id);
				$produto_->preco = (float)$produto['preco'];
				$produto_->save();

				$pedido->produtos()->attach($produto_id, ['qtd' => (float)$produto['qtd'],'preco' => (float)$produto['preco']]);

			}
		}
		if(!empty($aux))		
			$pedido->produtos()->detach($aux);
		

		if($input['efetivado'])
		{
			foreach($pedido->produtos as $item)
			{
				$produto = Produto::find($item->id);
				if($produto->qtd_estoque <= 0)
				{
					$ordemProd = new Producao;
					$data= date_parse(date('Y-m-d'));

					$dateA = new DateTime($data['year'].'-'.$data['month'].'-1');
					$dateB = new DateTime($data['year'].'-'.$data['month'].'-1');
					$dateB = $dateB->add(new DateInterval('P1M'));
					$dateB = $dateB->sub(new DateInterval('P1D'));
					$dateA = $dateA->format('Y-m-d');
					$dateB = $dateB->format('Y-m-d');

					$count = Producao::whereBetween('data', [$dateA, $dateB])->count();
					$count +=1;
					$ordemProd->data = date('Y-m-d');
					$ordemProd->codigo = $count."(".sprintf("%02d",$data['month'])."/".$data['year'].")";;

					$producao = Producao::where('codigo','=',$ordemProd->codigo)->get();
					while(!$producao->isEmpty())
					{
						$count++;
						$ordemProd->codigo = $count."(".sprintf("%02d",$data['month'])."/".$data['year'].")";			
						$producao = Producao::where('codigo', '=', $ordemProd->codigo)->get();
					}

					$ordemProd->produto_id = $produto->id;
					$ordemProd->qtd_inicial = $item->pivot->qtd;
					$ordemProd->qtd_final = $item->pivot->qtd;
					$produto->qtd_estoque += $item->pivot->qtd;
					$ordemProd->concluido = true;
					$ordemProd->save();
					//atualizar quantidade de insumos estoque
					$aux = $ordemProd->qtd_inicial / $produto->rendimento; //quantidade que será utilizada de insumos
					$composicao = $produto->insumos->lists('pivot');
					foreach($composicao as $ingrediente)
					{
						$insumo = Insumo::find($ingrediente->insumo_id);
						$unid_conv = $insumo->unidades;
						foreach($unid_conv as $unit)
						{
							if($insumo->unidade_id == $unit->id)
								$qtdUnPad = $unit->pivot->qtd;
							if($ingrediente->unidade_id == $unit->id)
								$qtdUnDes = $unit->pivot->qtd;
						}
						$tot_insumo = (($ingrediente->qtd * $aux) * $qtdUnPad)/$qtdUnDes;
						$insumo->qtd_estoque -= $tot_insumo;
						$insumo->save();
					}
				}
				$produto->qtd_estoque -= $item->pivot->qtd;
				$produto->save();
			}

			//procurar no contas a receber se existe uma conta do cliente que seja da data do pedido
			$contas = ContaReceber::where('cliente_id', '=', $pedido->cliente_id)
								->where('data', '=', $pedido->data)->get();
			if(!$contas->isEmpty())
			{
				$found_id = false;
				foreach($contas as $conta)
				{
					foreach($conta->itens as $item)
					{
						if($item->entrega->endereco->id == $pedido->entrega->endereco->id
							 &&
							$item->produtos[0]->id == $pedido->produtos[0]->id)
						{
							$found_id = true;
							break;
						}
					}
					if($found_id == true)
					{
						$found_id = $conta->id;
						break;
					}
				}
				if($found_id !== false)
				{
					$conta = ContaReceber::find($found_id);
					$conta->valor_total += $pedido->total;
					$conta->itens()->attach($pedido->id);
					$conta->save();
				} else {
					$conta = new ContaReceber;
					$conta->cliente_id = $pedido->cliente_id;
					$conta->data = $pedido->data;
					$conta->valor_total = $pedido->total;
					$conta->save();
					$conta->itens()->attach($pedido->id);
				}

			} else {
				$conta = new ContaReceber;
				$conta->cliente_id = $pedido->cliente_id;
				$conta->data = $pedido->data;
				$conta->valor_total = $pedido->total;
				$conta->save();
				$conta->itens()->attach($pedido->id);
			}

		};

		return Redirect::route('pedidos.index');
	}

	public function efetivar($id)
	{
		$pedido = Pedido::find($id);
		$input = Input::all();
		if($input['efetivado'])
		{
			foreach($pedido->produtos as $item)
			{
				$produto = Produto::find($item->id);
				if($produto->qtd_estoque <= 0)
				{
					$ordemProd = new Producao;
					$data= date_parse(date('Y-m-d'));

					$dateA = new DateTime($data['year'].'-'.$data['month'].'-1');
					$dateB = new DateTime($data['year'].'-'.$data['month'].'-1');
					$dateB = $dateB->add(new DateInterval('P1M'));
					$dateB = $dateB->sub(new DateInterval('P1D'));
					$dateA = $dateA->format('Y-m-d');
					$dateB = $dateB->format('Y-m-d');

					$count = Producao::whereBetween('data', [$dateA, $dateB])->count();
					$count +=1;

					$ordemProd->data = date('Y-m-d');
					$ordemProd->codigo = $count."(".sprintf("%02d",$data['month'])."/".$data['year'].")";;
					
					$producao = Producao::where('codigo','=',$ordemProd->codigo)->get();
					while(!$producao->isEmpty())
					{
						$count++;
						$ordemProd->codigo = $count."(".sprintf("%02d",$data['month'])."/".$data['year'].")";			
						$producao = Producao::where('codigo', '=', $ordemProd->codigo)->get();
					}

					$ordemProd->produto_id = $produto->id;
					$ordemProd->qtd_inicial = $item->pivot->qtd;
					$ordemProd->qtd_final = $item->pivot->qtd;
					$produto->qtd_estoque += $item->pivot->qtd;
					$ordemProd->concluido = true;
					$ordemProd->save();
					//atualizar quantidade de insumos estoque
					$aux = $ordemProd->qtd_inicial / $produto->rendimento; //quantidade que será utilizada de insumos
					$composicao = $produto->insumos->lists('pivot');
					foreach($composicao as $ingrediente)
					{
						$insumo = Insumo::find($ingrediente->insumo_id);
						$unid_conv = $insumo->unidades;
						foreach($unid_conv as $unit)
						{
							if($insumo->unidade_id == $unit->id)
								$qtdUnPad = $unit->pivot->qtd;
							if($ingrediente->unidade_id == $unit->id)
								$qtdUnDes = $unit->pivot->qtd;
						}
						$tot_insumo = (($ingrediente->qtd * $aux) * $qtdUnPad)/$qtdUnDes;
						$insumo->qtd_estoque -= $tot_insumo;
						$insumo->save();
					}
				}
				$produto->qtd_estoque -= $item->pivot->qtd;
				$produto->save();
			}

			//procurar no contas a receber se existe uma conta do cliente que seja da data do pedido
			$contas = ContaReceber::where('cliente_id', '=', $pedido->cliente_id)
								->where('data', '=', $pedido->data)->get();
			if(!$contas->isEmpty())
			{
				$found_id = false;
				foreach($contas as $conta)
				{
					foreach($conta->itens as $item)
					{
						if($item->entrega->endereco->id == $pedido->entrega->endereco->id
							 &&
							$item->produtos[0]->id == $pedido->produtos[0]->id)
						{
							$found_id = true;
							break;
						}
					}
					if($found_id == true)
					{
						$found_id = $conta->id;
						break;
					}
				}
				if($found_id !== false)
				{
					$conta = ContaReceber::find($found_id);
					$conta->valor_total += $pedido->total;
					$conta->itens()->attach($pedido->id);
					$conta->save();
				} else {
					$conta = new ContaReceber;
					$conta->cliente_id = $pedido->cliente_id;
					$conta->data = $pedido->data;
					$conta->valor_total = $pedido->total;
					$conta->save();
					$conta->itens()->attach($pedido->id);
				}

			} else {
				$conta = new ContaReceber;
				$conta->cliente_id = $pedido->cliente_id;
				$conta->data = $pedido->data;
				$conta->valor_total = $pedido->total;
				$conta->save();
				$conta->itens()->attach($pedido->id);
			}
			$pedido->efetivado = true;
			$pedido->save();

			return Redirect::route('pedidos.index')->with('success','Pedido '.$pedido->codigo.' efetivado.');
		};		
		return Redirect::route('pedidos.index')->with('fail','Não foi possivel efetivar o pedido.');
	}

	public function cancelar($id)
	{
		
	}

	public function destroy($id)
	{
		//deve-se verificar se existe uma conta com parcelas pagas já
		//se existe uma conta com parcelas paga não será possivel deletar pedido
		//ainda há o problema da ordem de produção criada
		$pedido = Pedido::find($id);
		$cod = $pedido->codigo;
		if($conta = $pedido->conta())
		{
			foreach($conta->parcelas as $parcela)
			{
				if($parcela->data_pagamento)
				{
					return Redirect::route('pedidos.index')->with('fail','Não é possivel deletar um pedido que já possui uma conta com parcelas pagas');
				}
			}
			$conta->valor_total -= $pedido->total;
			$conta->save();
			if($conta->valor_total <= 0)
			{
				$conta->delete();
			} else {
				//deleta o pedido da conta
				$conta->itens()->detach($pedido->id);
				//readequa parcelas
				if(!$conta->parcelas->isEmpty())
				{

					while($conta->parcelas->sum('valor') > $conta->valor_total)
					{
						$conta->parcelas->last()->delete();
					}
					if($conta->parcelas->sum('valor') < $conta->valor_total)
					{
						$conta->parcelas->last()->valor += $conta->valor_total - $conta->parcelas->sum('valor');
						$conta->parcelas->last()->save();
					}
				}

			}
		}
		$pedido->produtos()->detach();
		$pedido->delete();
		return Redirect::route('pedidos.index')->with('success','Pedido '.$cod.' deletado.');
	}

}