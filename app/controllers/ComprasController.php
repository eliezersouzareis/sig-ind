<?php

class ComprasController extends BaseController {

	public function index()
	{
	   // $compras = Compra::orderBy('insumo_id', 'data')->paginate(10);
		$compras = Compra::all();
		return View::make('compras.index', compact('compras'));
	}

	public function create()
	{
		$insumos_options = DB::table('insumos')->orderBy('nome', 'asc')
							->lists('nome', 'id');
		return View::make('compras.create', compact('insumos_options'));
	}

	public function store()
	{ 
		$input = Input::all();

		if(Input::has('insumos'))
		{
			$insumos = array_pull($input, 'insumos');	
		}else{
			return Redirect::back()->withInput()->with('fail','É necessário existir itens na entrada');
		}
		$validation = Validator::make($input, Compra::$rules);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}

		Compra::create($input);
		$compra_id = DB::getPdo()->lastInsertId();
		$compra = Compra::find($compra_id);

		if(Input::has('insumos'))
		{
			
			foreach($insumos as $insumo_id => $insumo)
			{
				$insumo_id =(int) $insumo_id;
				$_insumo = Insumo::find($insumo_id);
				$compra->itens()->attach($insumo_id, array('unidade_id' => $_insumo->unidade_id,'qtd' => (float)$insumo['qtd'],'preco' => (float)$insumo['preco']));
				$_insumo->qtd_estoque += $insumo['qtd'];
				$_insumo->save();
			}
		}

		$conta = new ContaPagar;

		$conta->data = $compra->data;
		$conta->categoria_conta_id = 1;
		$conta->fornecedor =$compra->fornecedor->nome;
		$conta->valor_total = $compra->valor_total;
		$conta->compra_id = $compra->id;
		
		$conta->save();

		$conta = DB::getPdo()->lastInsertId();;
		$conta = ContaPagar::find($conta);

		foreach($compra->itens as $item)
		{
			$contaPagarItem = new ContaPagarItem;
			$contaPagarItem->conta_pagar_id = $conta->id;
			$contaPagarItem->nome = $item->nome;
			$contaPagarItem->qtd = $item->pivot->qtd;
			$contaPagarItem->valor = $item->pivot->preco;
			$contaPagarItem->save();
		}

		return Redirect::route('contaspagar.edit', $conta->id);
	}

	public function show($id)
	{
		return Compra::find($id);
	}

	public function edit($id)
	{
		$compra = Compra::find($id);
		
		if (is_null($compra))
		{
			return Redirect::route('compras.index');
		}
		
		return View::make('compras.edit', compact('compra'));
	}

	public function update($id)
	{
		$compra = Compra::find($id);

		$input = Input::all();

		//faz alteração no estoque
		if (Input::get('quantidade') <> Session::get('qtdAnterior')){
			$qtd_new = Input::get('quantidade') - Session::get('qtdAnterior');
			$insumo = Insumo::find($input['insumo_id']);
			$insumo->qtd_estoque += $qtd_new;
			$insumo->save();
		}

		Session::forget('qtdAnterior');
		// deve-se Atualizar conta criada
		/*
		$conta = Conta::where('compra_id',$id)->get();

		// ARRUMAR TUDO AQUI -> Talvez seja melhor criar todas as contas a pagar para aas entradas antigas
		//                      e ai só deixar o conteudo do primeiro IF
		/*
		if (Conta::where('compra_id',$id)->get()) {
			$conta = Conta::where('compra_id',$id)->get();

			$conta->data = Input::get('data');
			$conta->fornecedor = DB::table('fornecedores')->where('id', Input::get('fornecedor_id'))->pluck('nome');
			$conta->nota = DB::table('fornecedores')->where('id', Input::get('material_id'))->pluck('nome');
			$conta->quantidade = Input::get('quantidade');
			$conta->valor = Input::get('valor_item');
			$conta->valor_total = Input::get('valor_total');

			$conta->save();
		} else {
			$conta = new Conta;
			
			$conta->data = Input::get('data');
			$conta->fornecedor = DB::table('fornecedores')->where('id', Input::get('fornecedor_id'))->pluck('nome');
			$conta->nota = DB::table('fornecedores')->where('id', Input::get('material_id'))->pluck('nome');
			$conta->quantidade = Input::get('quantidade');
			$conta->valor = Input::get('valor_item');
			$conta->valor_total = Input::get('valor_total');

			$conta->save();

			$conta = new Conta;
		}
		*/
		
		$compra->update($input);

		return Redirect::route('compras.index');
	}

	public function destroy($id)
	{
		
		$compra = Compra::find($id);
		$insumos = $compra->itens;
		/*
		echo "<pre>";
		foreach ($insumos as $insumo)
		{
			dd($insumo->pivot->qtd);
			$_insumo = Insumo::find($insumo->id);
			$_insumo->unidades->find();
			$qtdRem = $insumo->pivot->qtd;
			$_insumo->save();


		}
		$compra->delete();*/
		return Redirect::route('compras.index');
	}

}