<?php

class ContasReceberController extends BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /receber
	 *
	 * @return Response
	 */
	public function index()
	{
		$contas =  ContaReceber::all()->sortByDesc('data');
		return View::make('contasreceber.index', compact('contas'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /receber/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('contasreceber.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /receber
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /receber/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($conta)
	{
		Session::put('contareceber',$conta->id);
		return View::make('contasreceber.show', compact('conta'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /receber/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($conta)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /receber/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($conta)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /receber/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($conta)
	{
		$conta->delete();
		return Redirect::route('contasreceber.index');
	}

}