<?php

class ParcelasController extends BaseController {

	public function index($conta)
	{   
		if(is_a($conta,'ContaPagar'))
		{
			return View::make('contaspagar.show', compact('conta'));
		}else
		{
			return View::make('contasreceber.show', compact('conta'));
		}		
	}

	public function create($conta)
	{
		if(is_a($conta,'ContaPagar'))
		{
			return View::make('parcelas.formapagamento', compact('conta'));
		}else
		{
			return View::make('parcelas.formarecebimento', compact('conta'));
		}
		return Redirect::to(URL::previous());
	}

	public function store($conta)
	{
		$input = Input::all();
			//$conta = ContaPagar::find($input['contapagar_id']);
			
			if(Input::has('parcela'))
				$parcelas = array_pull($input,'parcela');

			if(Input::has('parcela'))
			{
				$parcelasExistentes = $conta->parcelas;//collection
				if(count($parcelas) >= $parcelasExistentes->count())
				{
					foreach($parcelas as $parcelaId => $parcela)
					{
						if($parcelasExistentes->contains((int)$parcelaId))
						{
							if(!isset($parcela['data_vencimento']))
								$parcela['data_vencimento'] = date('Y-m-d');
							$contaParcela = Parcela::find((int)$parcelaId);
							$contaParcela->update($parcela);

						} else {	
							if(!isset($parcela['data_vencimento']))
								$parcela['data_vencimento'] = date('Y-m-d');
							Parcela::create($parcela);
							$parcela_id = DB::getPdo()->lastInsertId();
							$conta->parcelas()->attach($parcela_id);
						}
					}
				} else {
					$aux = $conta->parcelas()->getRelatedIds();//pega todos os ids da parcela
					foreach($parcelas as $parcelaId => $parcela)
					{
						if($parcelasExistentes->contains((int)$parcelaId))//vefifica se vai ser atualizado
						{
							$pos = array_search((int)$parcelaId, $aux);
							if(!($pos === false)) 
							{
								array_splice($aux, $pos, 1);
							}
							 
							if(!isset($parcela['data_vencimento']))
								$parcela['data_vencimento'] = date('Y-m-d');
							$contaParcela = Parcela::find((int)$parcelaId);
							$contaParcela->update($parcela);
						}
					}
					if(!empty($aux))
						$conta->parcelas()->detach($aux);
				}
			}			

		if(is_a($conta,'ContaPagar'))
		{
			return Redirect::route('contaspagar.show',$conta->id);
		} else {

			return Redirect::route('contasreceber.show',$conta->id);
		}
	}

	public function show($conta,$parcelaId)
	{
		return Parcela::find($parcelaId);
	}

	public function edit($conta,$parcelaId)
	{
		$parcela = Parcela::find($parcelaId);
		
		return View::make('parcelas.edit', compact('conta', 'parcela'));
	}

	public function pagar($conta,$parcelaId)
	{
		$parcela = Parcela::find($parcelaId);
		
		return View::make('parcelas.pagar', compact('conta', 'parcela'));
	}

	public function pagarSave($conta,$parcelaId) 
	{
		$input = Input::all();
		//trocar a forma de verificação da forma de pagamento// testar se é cheque
		if((int)$input['forma_pagamento_id'] == 8 || (int)$input['forma_pagamento_id'] == 15)
		{
			$chequeIn = array_pull($input, 'cheque');
		}
		
		$validation = Validator::make($input, Parcela::$rulesReceber);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}
		if((int)$input['forma_pagamento_id'] == 8 || (int)$input['forma_pagamento_id'] == 15)
		{
			foreach($chequeIn as $cheque)
			{
				$cheque = Cheque::find($cheque['id']);
				$cheque->compensado = true;
				$cheque->compensacao = date('Y-m-d');
				$cheque->save();
			}

		}

		$parcela = Parcela::find($parcelaId);
		$parcela->update($input);
		$caixa = $parcela->caixa;
		$caixa->saldo -=$parcela->valor_pago;
		$caixa->save();

		return Redirect::route('contaspagar.show',$conta->id);
	}

	public function receber($conta,$parcelaId)
	{
		$parcela = Parcela::find($parcelaId);

		return View::make('parcelas.receber',compact('conta', 'parcela'));
	}

	public function receberSave($conta,$parcelaId)
	{
		$input = Input::all();
		
		if((int)$input['forma_pagamento_id'] == 8 || (int)$input['forma_pagamento_id'] == 15)
		{
			$chequeIn = array_pull($input, 'cheque');
		}
		
		$validation = Validator::make($input, Parcela::$rulesReceber);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}

		if((int)$input['forma_pagamento_id'] == 8 || (int)$input['forma_pagamento_id'] == 15)
		{
			foreach($chequeIn as $cheque)
			{
				$cheque['data_entrada'] = date('Y-m-d');
				$cheque = Cheque::create($cheque);
				$cheque->cliente_id = $conta->cliente->id;
				$cheque->save();
			}

		}
		$parcela = Parcela::find($parcelaId);
		$parcela->update($input);
		$caixa = $parcela->caixa;
		$caixa->saldo +=$parcela->valor_pago;
		$caixa->save();

		return Redirect::route('contasreceber.show',$conta->id);
	}

	public function update($parcelaId)
	{
		$input = Input::all();

		$parcela = Parcela::find($parcelaId);

		$parcela->update($input);
		if($parcela->contaPagar[0])
			return Redirect::route('contaspagar.show',$parcela->contaPagar[0]->id);
		else
			return Redirect::route('contasreceber.show',$parcela->contaReceber[0]->id);
	}

	public function destroy($conta,$parcelaId)
	{
		Parcela::find($parcelaId)->delete();
		return Redirect::to(URL::previous());
	}

}
