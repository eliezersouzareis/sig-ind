<?php

class ProducaoController extends BaseController {

	public function index()
	{
		$input = Input::all();
		if(!$input)
		{
			$dateA = new DateTime(date('Y').'-'.date('m').'-1');
			$dateB = new DateTime(date('Y').'-'.date('m').'-1');
			$dateB = $dateB->add(new DateInterval('P1M'));
			$dateB = $dateB->sub(new DateInterval('P1D'));
			$dateA = $dateA->format('Y-m-d');
			$dateB = $dateB->format('Y-m-d');

			$data = date_parse(date('Y-m-d'));

			$pgNum = 1;

		}else 
		{
			if($input['mes'] > 12)
			{
				$input['mes'] = 1;
				$input['ano'] +=1; 
			}
			if($input['mes'] < 1 )
			{
				$input['mes'] = 12;
				$input['ano'] -=1;
			}
			
			$dateA = new DateTime($input['ano'].'-'.$input['mes'].'-1');
			$dateB = new DateTime($input['ano'].'-'.$input['mes'].'-1');;
			$dateB = $dateB->add(new DateInterval('P1M'));
			$dateB = $dateB->sub(new DateInterval('P1D'));
			$dateA = $dateA->format('Y-m-d');
			$dateB = $dateB->format('Y-m-d');
			
			$data = date_parse($input['ano'].'-'.$input['mes'].'-0');

			if(Input::has('pag'))
			{
				if($input['pag'] < 1)
					$input['pag'] = 1;
				$pgNum = $input['pag'];
			}
			else
				$pgNum = 1;
		}

		$ordensProducao = Producao::orderBy('id', 'desc')->whereBetween('data',[$dateA, $dateB]);
		$monthCount =$ordensProducao->count();
		$ordensProducao = $ordensProducao->skip(($pgNum-1)*10)->take(10)->get();
		return View::make('producao.index', compact('ordensProducao','monthCount', 'data', 'pgNum'));
	}

	public function create()
	{

		return View::make('producao.create',compact(''));
	}

	public function store()
	{
		$ordemProducao = Input::all();
		$data= date_parse(date('Y-m-d'));

		$dateA = new DateTime($data['year'].'-'.$data['month'].'-1');
		$dateB = new DateTime($data['year'].'-'.$data['month'].'-1');
		$dateB = $dateB->add(new DateInterval('P1M'));
		$dateB = $dateB->sub(new DateInterval('P1D'));
		$dateA = $dateA->format('Y-m-d');
		$dateB = $dateB->format('Y-m-d');

		$count = Producao::whereBetween('data', [$dateA, $dateB])->count();
		$count +=1;
		$ordemProducao['data'] = date('Y-m-d');
		$ordemProducao['codigo'] = $count."(".sprintf("%02d",$data['month'])."/".$data['year'].")";
		$producao = Producao::where('codigo','=',$ordemProducao['codigo'])->get();
		while(!$producao->isEmpty())
		{
			$count++;
			$ordemProducao['codigo'] = $count."(".sprintf("%02d",$data['month'])."/".$data['year'].")";			
			$producao = Producao::where('codigo','=',$ordemProducao['codigo'])->get();
		}
		$validation = Validator::make($ordemProducao, Producao::$rules);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}
		Producao::create($ordemProducao);
		return Redirect::route('producao.index');
	}
	
	public function show($id)
	{
		$ordemProducao = Producao::find($id);
		$produto = Produto::find($ordemProducao->produto_id);
		//$produto->unidade = Unidade::find($produto->unidade)->abreviatura;
		return View::make('producao.show', compact('ordemProducao', 'produto'));
	}

	public function concluir($id)
	{
		$ordemProducao = Producao::find($id);
		return View::make('producao.concluir', compact('ordemProducao'));
	}

	public function updateInventory($id)
	{
		//atualizar quantidade final na ordem de produçao
		$input = Input::only('qtd_final');
		$input['concluido'] = true;
		$input['data_fim'] = date('Y-m-d');
		$ordemProducao = Producao::find($id);
		$ordemProducao->update($input);
		//atualizar quantidade de produtos estoque
		$produto = Produto::find($ordemProducao->produto_id);
		$produto->qtd_estoque += $ordemProducao->qtd_final;
		$produto->save();
		//atualizar quantidade de insumos estoque
		$aux = $ordemProducao->qtd_inicial / $produto->rendimento; //quantidade que será utilizada de insumos
		$composicao = $produto->insumos->lists('pivot');
		foreach($composicao as $ingrediente)
		{
			$insumo = Insumo::find($ingrediente->insumo_id);
			$unid_conv = $insumo->unidades;
			foreach($unid_conv as $unit)
			{
				if($insumo->unidade_id == $unit->id)
					$qtdUnPad = $unit->pivot->qtd;
				if($ingrediente->unidade_id == $unit->id)
					$qtdUnDes = $unit->pivot->qtd;
			}
			$tot_insumo = (($ingrediente->qtd * $aux) * $qtdUnPad)/$qtdUnDes;
			$insumo->qtd_estoque -= $tot_insumo;
			$insumo->save();
		}
		return Redirect::route('producao.index');
	}

	public function edit($id)
	{
		$ordemProducao = Producao::find($id);
		return View::make('producao.edit', compact('ordemProducao'));
	}

	public function update($id)
	{
		$ordemProducao = Producao::find($id);
		if(!$ordemProducao->concluido)
		{
			//pegar id do produto tbm, nao esquecer
			$input = Input::all();
			$ordemProducao->update($input);
		}
		else
		{
			$input = Input::only('qtd_final');
			$oldQtd_final = $ordemProducao->qtd_final;
			$ordemProducao->update($input);
			//atualizar quantidade de produtos
			$produto = Produto::find($ordemProducao->produto_id);
			$produto->qtd_estoque += ($ordemProducao->qtd_final - $oldQtd_final);
			$produto->save();
		}
		return Redirect::route('producao.index');
	}

	public function destroy($id)
	{
		$ordemProducao = Producao::find($id);
		if(!$ordemProducao->concluido)
		{
			$ordemProducao->delete();
		}
		else
		{
			$produto = Produto::find($ordemProducao->produto_id);
			$produto->qtd_estoque -= $ordemProducao->qtd_final;
			$produto->save();
			//atualizar quantidade de insumos estoque
			$aux = $ordemProducao->qtd_inicial / $produto->rendimento; //quantidade que será utilizada de insumos
			$composicao = $produto->insumos->lists('pivot');
			foreach($composicao as $ingrediente)
			{
				$insumo = Insumo::find($ingrediente->insumo_id);
				$unid_conv = $insumo->unidades;
				foreach($unid_conv as $unit)
				{
					if($insumo->unidade_id == $unit->id)
						$qtdUnPad = $unit->pivot->qtd;
					if($ingrediente->unidade_id == $unit->id)
						$qtdUnDes = $unit->pivot->qtd;
				}
				$tot_insumo = (($ingrediente->qtd * $aux) * $qtdUnPad)/$qtdUnDes;
				$insumo->qtd_estoque += $tot_insumo;
				$insumo->save();
			}			
			$ordemProducao->delete();
		}

		return Redirect::route('producao.index');
	}


}