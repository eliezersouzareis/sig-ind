<?php

class CaixasController extends BaseController {

    public function index()
    {
        $caixas = Caixa::all();

        return View::make('caixas.index', compact('caixas'));
    }

    public function create()
    {
        return View::make('caixas.create');
    }

    public function store()
    {
        $input = Input::all();

        Caixa::create($input);

        return Redirect::route('caixas.index');
    }

    public function show($id)
    {
        return Caixa::find($id);
    }

    public function edit($id)
    {
        $caixa = Caixa::find($id);
        
        if (is_null($caixa))
        {
            return Redirect::route('caixas.index');
        }

        return View::make('caixas.edit', compact('caixa'));
    }

    public function update($id)
    {
        $input = Input::all();
 
        $caixa = Caixa::find($id);
        $caixa->update($input);

        return Redirect::route('caixas.index');
    }

    public function destroy($id)
    {
        Caixa::find($id)->delete();
        return Redirect::route('caixas.index');
    }

}