<?php

class CargosController extends BaseController {

	public function index()
	{
		$cargos = Cargo::all();

		return View::make('cargos.index', compact('cargos'));
	}

	public function create()
	{
		return View::make('cargos.create');
	}

	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Cargo::$rules);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}
		Cargo::create($input);

		return Redirect::route('cargos.index');
	}

	public function show($id)
	{
		return Cargo::find($id);
	}

	public function edit($id)
	{
		$cargo = Cargo::find($id);
		
		if (is_null($cargo))
		{
			return Redirect::route('cargos.index');
		}

		return View::make('cargos.edit', compact('cargo'));
	}

	public function update($id)
	{
		$input = Input::all();
 
		$cargo = Cargo::find($id);
		$cargo->update($input);

		return Redirect::route('cargos.index');
	}

	public function destroy($id)
	{
		Cargo::find($id)->delete();
		return Redirect::route('cargos.index');
	}

}