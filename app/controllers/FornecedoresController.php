<?php

class FornecedoresController extends BaseController {
	
	public function index()
	{
		$fornecedores = Fornecedor::all();

		return View::make('fornecedores.index', compact('fornecedores'));
	}

	public function create()
	{
		$insumos = Insumo::all();
		return View::make('fornecedores.create', compact('insumos'));
	}

	public function store()
	{
		$input = Input::all();
		$endereco = array_pull($input,'endereco');
		if(Input::has('insumos'))
			$insumos = array_pull($input,'insumos');
		$validation = Validator::make($input, Fornecedor::$rules);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}
		$endereco['nome'] = 'Matriz';
		$endereco['telefone'] = $input['telefone_fixo'];
		$validation = Validator::make($endereco, Endereco::$rules);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}

		Fornecedor::create($input);
		$fornecedor_id = DB::getPdo()->lastInsertId();
		$fornecedor = Fornecedor::find($fornecedor_id);

		Endereco::create($endereco);
		$endereco_id = Db::getPdo()->lastInsertId();

		$fornecedor->endereco_id = $endereco_id;
		$fornecedor->save();
		
		if(Input::has('insumos'))
			foreach($insumos as $insumo_id => $preco) {
				$insumo_id = (int) $insumo_id;
				$fornecedor->insumos()->attach($insumo_id, array('preco' => $preco));
			}

		return Redirect::route('fornecedores.index')->with('success','Cadastro realizado com sucesso.');
	}

	public function show($id)
	{
		$fornecedor = Fornecedor::find($id);
		return View::make('fornecedores.show', compact('fornecedor'));
	}

	public function edit($id)
	{
		$insumos = Insumo::all();
		$fornecedor = Fornecedor::find($id);
		$fornecedor['endereco'] = $fornecedor->endereco;
		
		if (is_null($fornecedor))
		{
			return Redirect::route('fornecedores.index');
		}

		return View::make('fornecedores.edit', compact('fornecedor', 'insumos'));
	}

	public function update($id)
	{
		$input = Input::all();
		$endereco = array_pull($input,'endereco');
		if(Input::has('insumos'))
			$insumos = array_pull($input,'insumos');

		$fornecedor = Fornecedor::find($id);
		$fornecedor->update($input);

		Endereco::find($fornecedor->endereco_id)->update($endereco);

		$fornecedor->insumos()->detach();
		if(Input::has('insumos'))
			foreach($insumos as $insumo_id => $preco) {
				$insumo_id = (int) $insumo_id;
				$fornecedor->insumos()->attach($insumo_id, array('preco' => $preco));
			}
 
		return Redirect::route('fornecedores.index');
	}

	public function destroy($id)
	{
		$fornecedor = Fornecedor::find($id);
		Endereco::find($fornecedor->endereco_id)->delete();
		$fornecedor->delete();
		return Redirect::route('fornecedores.index');
	}

}