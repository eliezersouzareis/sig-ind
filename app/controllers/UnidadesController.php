<?php

class UnidadesController extends BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /unidades
	 *
	 * @return Response
	 */
	public function index()
	{
		$unidades = Unidade::orderBy('id')->get();

		return View::make('unidades.index',compact('unidades'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /unidades/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('unidades.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /unidades
	 *
	 * @return Response
	 */
	public function store()
	{

		$input = Input::all();
		$validation = Validator::make($input, Unidade::$rules);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}

		Unidade::create($input);

		return Redirect::route('config.unidades.index');
	}

	/**
	 * Display the specified resource.
	 * GET /unidades/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /unidades/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$unidade = Unidade::find($id);

		return View::make('unidades.edit',compact('unidade'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /unidades/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();

		$unidade = Unidade::find($id);

		$unidade->update($input);

		return Redirect::route('config.unidades.index');

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /unidades/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Unidade::find($id)->delete();
		return Redirect::route('config.unidades.index');
	}

}