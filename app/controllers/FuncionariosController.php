<?php

class FuncionariosController extends BaseController {

    public function index()
    {
        $funcionarios = Funcionario::all();

        return View::make('funcionarios.index', compact('funcionarios'));
    }

    public function create()
    {
        $veiculo_options = Veiculo::orderBy('placa', 'asc')->lists('placa','id');
        $cargos_options = Cargo::orderBy('nome', 'asc')->lists('nome','id');
        return View::make('funcionarios.create', compact('veiculo_options','cargos_options'));
    }

    public function store()
    {
        $input = Input::all();
        if(Input::has('cargo_id'))
        {
            $cargo = Cargo::find(Input::get('cargo_id'));
            if ($cargo->nome != 'Motorista'){
                $input = Input::except('veiculo_id');
            }
        }
        $endereco = array_pull($input,'endereco');
        $validation = Validator::make($input, Funcionario::$rules);
        if($validation->fails())
        {
            return Redirect::back()->withInput()->withErrors($validation->messages());
        }
        $endereco['nome'] = 'Casa';
        $validation = Validator::make($endereco, Endereco::$rules);
        if($validation->fails())
        {
            return Redirect::back()->withInput()->withErrors($validation->messages());
        }
        
        Funcionario::create($input);
        $funcionario_id = DB::getPDO()->lastInsertId();
        $funcionario = Funcionario::find($funcionario_id);
        
        Endereco::create($endereco);
        $endereco_id = DB::getPDO()->lastInsertId();

        $funcionario->endereco_id = $endereco_id;
        $funcionario->save();

		return Redirect::route('funcionarios.index')->with('success','Cadastro realizado com sucesso.');
    }

    public function show($id)
    {
        $funcionario = Funcionario::find($id);
        return View::make('funcionarios.show', compact('funcionario'));
    }

    public function edit($id)
    {
        $veiculo_options = Veiculo::orderBy('placa', 'asc')->lists('placa','id');
        $cargos_options = Cargo::orderBy('nome', 'asc')->lists('nome','id');
    
    	$funcionario = Funcionario::find($id);
        $funcionario['endereco'] = $funcionario->endereco;
        
        if (is_null($funcionario))
        {
            return Redirect::route('funcionarios.index');
        }

        return View::make('funcionarios.edit', compact('funcionario','veiculo_options','cargos_options'));
    }

    public function update($id)
    {
        $input = Input::all();
        $endereco = array_pull($input, 'endereco');
        $funcionario = Funcionario::find($id);
        $funcionario->update($input);

        Endereco::find($funcionario->endereco_id)->update($endereco);

        return Redirect::route('funcionarios.index');
    }

    public function destroy($id)
    {
        $funcionario = Funcionario::find($id);
        Endereco::find($funcionario->endereco_id)->delete();
        $funcionario->delete();
        return Redirect::route('funcionarios.index');
    }

}