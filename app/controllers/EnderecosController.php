<?php

class EnderecosController extends BaseController {


	public static function index()
	{
		//$clientes = Cliente::all();
		//return View::make('enderecos.index', compact('clientes'));
	}

	public function create($cliente_id)
	{
		$cliente = Cliente::find($cliente_id);
		return View::make('enderecos.create', compact('cliente'));
	}

	public function store($cliente_id)
	{
		$input = Input::all();
		$validation = Validator::make($input, Endereco::$rules);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}
		Endereco::create($input);
		$endereco_id = DB::getPdo()->lastInsertId();

		$cliente = Cliente::find($cliente_id);

		if($cliente->endereco_id)
		{   
			$cliente->filiais()->attach($endereco_id);
		}else{
			$cliente->endereco_id = $endereco_id;
			$cliente->save();
		}

		return Redirect::route('clientes.show',$cliente_id);
	}

	public function show($cliente_id,$endereco_id)
	{
		return Endereco::find($endereco_id);
	}

	public function edit($cliente_id, $endereco_id)
	{
		$endereco = Endereco::find($endereco_id);
		$cliente = Cliente::find($cliente_id);
		if (is_null($endereco))
		{
			return Redirect::route('clientes.show',$cliente_id);
		}

		return View::make('enderecos.edit', compact('cliente', 'endereco'));
	}

	public function update($cliente_id, $endereco_id)
	{
		$input = Input::all();
 
		$endereco = Endereco::find($endereco_id);
		$endereco->update($input);

		return Redirect::route('clientes.show',$cliente_id);
	}

	public function destroy($cliente_id, $endereco_id)
	{
		Cliente::find($cliente_id)->filiais()->detach($endereco_id);
		Endereco::find($endereco_id)->delete();
		
		return Redirect::route('clientes.show',$cliente_id);
	}

}