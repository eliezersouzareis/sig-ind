<?php

class InsumosController extends BaseController {

	public function index()
	{
		$insumos = Insumo::all();
		return View::make('insumos.index', compact('insumos'));
	}

	public function create()
	{
		$unidades =  Unidade::orderBy('nome')->get();
		$aux = array();
		foreach($unidades as $unidade)
		{
			$aux[$unidade->id] = $unidade->nome;
		};
		$unidades = $aux;
		return View::make('insumos.create', compact('unidades'));
	}

	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Insumo::$rules);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}
		$input[ 'unidade_id' ] = $input[ 'unidade_primaria' ];
		if(Input::has('unidades'))
		{
			$unidades = array_pull($input, 'unidades'); 
		}
		$input['qtd_estoque'] = 0.00;
		
		$insumo = Insumo::create($input);
		
		if(Input::has('unidades'))
		{       
			$insumo->unidades()->sync($unidades);
			//$insumo_id = DB::getPdo()->lastInsertId();
			//$insumo = Insumo::find($insumo_id);
			/*
			foreach ($unidades as $unidade)
			{
				//dd($unidade);
				$insumo->unidades()->attach($unidade['unidade_id'], array('qtd' => (float)$unidade['qtd']));
			}*/
		}

		return Redirect::route('insumos.index');
	}

	public function show($id)
	{
		$insumo = Insumo::find($id);
		$unidades = Unidade::all();
	  //  dd($unidades);
		return View::make('insumos.show', compact('insumo', 'unidades'));
	}

	public function edit($id)
	{
		$insumo = Insumo::find($id);
		
		if (is_null($insumo))
		{
			return Redirect::route('insumos.index');
		}
		$unidades =  Unidade::orderBy('nome')->get();
		$aux = array();
		foreach($unidades as $unidade)
		{
			$aux[$unidade->id] = $unidade->nome;
		};
		$unidades = $aux;
		return View::make('insumos.edit', compact('insumo', 'unidades'));
	}

	public function update($id)
	{
		//echo "<pre>";
		$input = Input::all();
		$validation = Validator::make($input, [ 'unidade_primaria'=> 'required' ]);
		if($validation->fails())
		{
			return Redirect::back()->withInput()->withErrors($validation->messages());
		}
		$input[ 'unidade_id' ] = $input[ 'unidade_primaria' ];
		if(Input::has('unidades'))
			$unidades = array_pull($input, 'unidades');

		$insumo = Insumo::find($id);
		$old_unPrimaria = $insumo->unidade_id;
		$insumo->update($input);

		//tabela de conversao
		if(Input::has('unidades'))
		{
			if($old_unPrimaria != $insumo->unidade_id)
			{
				
			}

			// lista dos ids de unidades já cadastradas
			$aux = $insumo->unidades->modelKeys();

			//atualiza existentes
			foreach($unidades as $unidade)
			{
				//vefica se já é existente
				$pos = array_search($unidade['unidade_id'], $aux);
				if(!($pos === false))//é assim mesmo. peculiaridade do PHP. zero é uma posiçao válida para array e tbm é considerado como valor falso
				{
					array_splice($aux, $pos, 1);
					$insumo->unidades()->updateExistingPivot($unidade['unidade_id'],['qtd' => (float)$unidade['qtd']]);
				}else{
					$insumo->unidades()->attach($unidade['unidade_id'],['qtd' => (float)$unidade['qtd']]);
				}
			}
			if(!empty($aux))
					$insumo->unidades()->detach($aux);
		
		}

		return Redirect::route('insumos.index');
	}

	public function destroy($id)
	{
		Insumo::find($id)->delete();
		return Redirect::route('insumos.index');
	}

}