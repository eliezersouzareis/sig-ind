<?php

class Fornecedor extends Eloquent {
	protected $table = 'fornecedores';
	protected $fillable = ['nome', 'nome_fantasia', 'cpf_cnpj', 'email', 'telefone_fixo', 'telefone_celular',
							'descricao', 'endereco_id'];
	protected $guarded = array();

	public static $rules = array(
		'nome'      => 'required',
		'cpf_cnpj'  => 'required'
	);
	
	public function insumos() {
		return $this->belongsToMany('Insumo')->withPivot('preco');    
	}

	public function endereco()
	{
		return $this->belongsTo('Endereco');
	}

	public $timestamps = false;
}