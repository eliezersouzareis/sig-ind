<?php

class Pedido extends Eloquent {
	protected $table = 'pedidos';

	protected $fillable = ['codigo', 'cliente_id', 'entrega_id', 'data',
							'hora', 'total','observacoes', 'efetivado'];
	protected $guarded = array();

	public static $rules = array(
			'codigo'		=> 'required|unique:pedidos',
			'data'			=> 'required',
			'cliente_id'	=> 'required'
		);

	public function cliente(){
		return $this->belongsTo('Cliente', 'cliente_id');
	}

	public function entrega(){
		return $this->belongsTo('Entrega', 'entrega_id');
	}

	public function produtos()
	{
		return $this->belongsToMany('Produto')->withPivot('qtd','preco');
	}

	public function conta()
	{
		return $this->belongsToMany('ContaReceber', 'conta_receber_itens')->first();
	}

	
	public $timestamps = false;

}
