<?php

class ContaReceber extends Eloquent {
	protected $table = 'conta_receber';

	protected $fillable = [ 'data', 'nota', 'valor_total', 'desconto', 'cliente_id'];

	public function itens(){
		return $this->belongsToMany('Pedido', 'conta_receber_itens');
	}

	public function parcelas(){
		return $this->belongsToMany('Parcela');
	}
	
	public function cliente(){
		return $this->belongsTo('Cliente');
	}


	public $timestamps = false;
}