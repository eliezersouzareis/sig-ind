<?php

class AdicaoManual extends Eloquent {
	protected $table = 'adicaomanual';
	protected $fillable = ['data','produto_id','qtd'];

	public function produto()
	{
		return $this->belongsTo('Produto');
	}

	public $timestamps = false;
}