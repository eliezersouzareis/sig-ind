<?php

class Endereco extends Eloquent {
	protected $table = 'enderecos';
	protected $fillable = ['nome', 'contato', 'telefone', 'logradouro', 'numero',
							'complemento', 'referencia', 'bairro', 'cep', 'cidade',
							'estado'];
	protected $guarded = array();

	public static $rules = array(
			'logradouro'	=> 'required',
			'numero'		=> 'required',
			'bairro'		=> 'required',
			'cidade'		=> 'required',
			'estado'		=> 'required'
		);

	public $timestamps = false;
}