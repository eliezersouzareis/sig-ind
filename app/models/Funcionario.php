<?php

class Funcionario extends Eloquent {
	protected $table = 'funcionarios';
	protected $fillable = [	'nome', 'cpf','email', 'telefone_fixo',
							'telefone_celular', 'endereco_id', 'cargo_id', 'salario',
	 						'observacoes', 'data_admissao', 'data_demissao', 'ativo'
	 						];
	protected $guarded = array();

	public static $rules = array(
		'nome'		=> 'required',
		'cpf'	=> 'required'
	);

	public function cargos() {
		return $this->belongsTo('Cargo');
	}

	public function endereco() {
		return $this->belongsTo('Endereco');
	}

	public $timestamps = false;
}