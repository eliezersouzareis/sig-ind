<?php

class Producao extends Eloquent {
	protected $table = 'producao';

	protected $fillable = ['data', 'data_fim', 'codigo', 'produto_id',
							'qtd_inicial', 'qtd_final', 'concluido'];

	protected $guarded = array();

	public static $rules = array(
			'data'		=> 'required',
			'produto_id'=> 'required',
			'codigo'	=> 'required'
		);

	public function produto() {
		return $this->belongsTo('Produto');
	}

	public $timestamps = true;
}