<?php

class Entrega extends Eloquent {
	protected $table = 'entregas';
	protected $fillable = ['endereco_id', 'funcionario_id', 'veiculo_id'];

	public function endereco()
	{
		return $this->belongsTo('Endereco','endereco_id');
	}
	public function funcionario()
	{
		return $this->belongsTo('Funcionario','funcionario_id');
	}
	public function veiculo()
	{
		return $this->belongsTo('Veiculo', 'veiculo_id');
	}


	public $timestamps = false;
}