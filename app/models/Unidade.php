 <?php

class Unidade extends Eloquent {
	protected $table = 'unidades';
	protected $fillable = ['nome','abreviatura'];
	protected $guarded = array();

	public static $rules = array(
			'nome'			=> 'required',
			'abreviatura'	=> 'required'
		);

	public $timestamps = false;
}