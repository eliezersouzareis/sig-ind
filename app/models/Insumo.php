<?php

class Insumo extends Eloquent {
	protected $table = 'insumos';
	protected $fillable = ['nome', 'qtd_estoque', 'unidade_id'];
	protected $guarded = array();

	public static $rules = array(
			'nome'      => 'required|unique:insumos',
			'unidade_primaria'=> 'required'
		);

	public function fornecedores() {
		return $this->belongsToMany('Fornecedor');    
	}
	public function compras() {
		return $this->hasMany('Compra');    
	}

	public function unidadePrimaria() {
		return $this->belongsTo('Unidade', 'unidade_id');
	}

	public function unidades(){
		return $this->belongsToMany('Unidade', 'insumo_unidade')->withPivot('qtd');//tabela insumo unidade qtd
	}

	public $timestamps = false;
}