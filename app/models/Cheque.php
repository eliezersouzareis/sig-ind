<?php

class Cheque extends Eloquent {
	protected $table = 'cheques';
	protected $fillable = ['data_entrada', 'banco',	'numero_cheque', 'valor',
	 'vencimento', 'compensacao', 'compensado', 'cliente_id' ];
	protected $guarded = array();

	public static $rules = array(
			'banco'			=> 'required',
			'numero_cheque'	=> 'required'
		);

	public function cliente()
	{
		return $this->belongsTo('Cliente');
	}

	public function parcela()
	{
		return $this->hasMany('Parcela');
	}

	public $timestamps = false;
}