<?php

class Produto extends Eloquent {
	protected $table = 'produtos';

	protected $fillable = ['nome','codigo', 'unidade_id', 'rendimento',
							 'descricao', 'qtd_estoque', 'preco'];

	protected $guarded = array();

	public static $rules = array(
			'nome'		=> 'required',
			'codigo'	=> 'required|unique:produtos'
		);

	public function unidade()
	{
		return $this->belongsTo('Unidade', 'unidade_id');
	}

	public function insumos()
	{
		return $this->belongsToMany('Insumo')->withPivot('unidade_id','qtd');
	}

	public $timestamps = false;
	
}