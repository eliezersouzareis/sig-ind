<?php

class Parcela extends Eloquent {
	protected $table = 'parcelas';
	protected $fillable = [ 'forma_pagamento_id', 'data_vencimento', 'data_pagamento',
							'valor', 'valor_pago', 'caixa_id', 'cheque_id'];

	public static $rules = array(
			'valor'		=> 'required|numeric',
		);
	public static $rulesReceber = array(
			'valor_pago'		=> 'required|numeric',
			'caixa_id'		=> 'required|numeric'
		);

	public function caixa(){
		return $this->belongsTo('Caixa');
	}
	
	public function cheque(){
		return $this->belongsTo('Cheque');
	}
	
	public function formaPagamento()
	{
		return $this->belongsTo('FormaPagamento');
	}

	public function contaPagar()
	{
		return $this->belongsToMany('ContaPagar','conta_pagar_parcela');
	}
	public function contaReceber()
	{
		return $this->belongsToMany('ContaReceber','conta_receber_parcela');
	}

	public $timestamps = false;
}