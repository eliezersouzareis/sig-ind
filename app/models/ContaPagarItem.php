<?php

class ContaPagarItem extends Eloquent {
	protected $table	= 'conta_pagar_itens';
	protected $fillable = [ 'conta_pagar_id', 'nome', 'qtd', 'valor' ];

	public $timestamps = false;
}