<?php
class Caixa extends Eloquent {
	protected $table = 'caixas';
	protected $fillable = ['nome', 'nota', 'saldo'];
	protected $guarded = array();

	public function Pagamento(){
		return $this->belongsTo('Pagamento');
	}

	public function parcela(){
		return $this->hasMany('Parcela');
	}
	public $timestamps = false;
}