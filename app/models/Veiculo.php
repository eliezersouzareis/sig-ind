<?php

class Veiculo extends Eloquent {
	protected $fillable = ['placa','descricao'];
	protected $guarded = array();

	public static $rules = array(
			'placa'	=> 'required'
		);

	public function veiculos(){
		return $this->hasMany('Funcionario');
	}
	
	public $timestamps = false;
}