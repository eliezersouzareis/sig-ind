<?php

class Compra extends Eloquent {
	protected $table = 'compras';
	protected $fillable = ['data', 'fornecedor_id', 'valor_total', 'notas'];
	protected $guarded = array();

	public static $rules = array(
			'data' => 'required',
			'fornecedor_id' => 'required',
			'valor_total' => 'required'
		);

	public function fornecedor()
	{
		return $this->belongsTo('Fornecedor', 'fornecedor_id');
	}

	public function itens()
	{
		return $this->belongsToMany('Insumo')->withPivot('unidade_id','qtd', 'preco');
	}

	public function contas()
	{
		return $this->hasMany('ContaPagar');
	}

	public $timestamps = false;
}