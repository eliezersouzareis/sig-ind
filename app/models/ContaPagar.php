<?php

class ContaPagar extends Eloquent {
	protected $table = 'conta_pagar';
	protected $fillable = ['fornecedor', 'data', 'nf_data','nf_num',
		 'categoria_conta_id', 'nota', 'valor_total', 'desconto', 'compra_id'];
	protected $guarded = array();

	public function parcelas(){
		return $this->belongsToMany('Parcela');
	}

	public function itens(){
		return $this->hasMany('ContaPagarItem', 'conta_pagar_id');
	}

	public function compra(){
		return $this->belongsTo('Compra', 'compra_id');
	}

	public function categoria()
	{
		return $this->belongsTo('CategoriaConta', 'categoria_conta_id');
	}

	public $timestamps = false;

}