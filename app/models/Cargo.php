<?php

class Cargo extends Eloquent {
	protected $table = 'cargos';
	protected $fillable = ['nome', 'descricao'];
	protected $guarded = array();

	public static $rules = array(
			'nome'	=> 'required'
		);

	public function cargos(){
		return $this->hasMany('Funcionario');
	}
	public $timestamps = false;
}