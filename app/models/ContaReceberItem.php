<?php

class ContaReceberItem extends Eloquent {
	protected $table = 'conta_receber_itens';

	protected $fillable = ['conta_receber_id','pedido_id'];

	public function pedido(){
		return $this->belongsTo('Pedido');
	}
	public function conta(){
		return $this->belongsTo('ContaReceber');
	}

	public $timestamps = false;
}