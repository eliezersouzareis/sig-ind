<?php

class Confs extends Eloquent {
	protected $fillable = ['conf_name', 'type', 'value'];

	public $timestamps = false;
}