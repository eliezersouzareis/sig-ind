<?php

class FormaPagamento extends Eloquent {
	protected $table = 'forma_pagamento';
	
	protected $fillable = ['nome', 'sigla','codigo', 'parent_id'];

	public static $rules = array(
			'nome'		=> 'required',
			'sigla'		=> 'required',
			'codigo'	=> 'required'
		);

	public function pai()
	{
		return $this->belongsTo('FormaPagamento', 'parent_id');
	}

	public $timestamps = false;
}