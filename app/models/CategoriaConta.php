<?php

class CategoriaConta extends Eloquent {
	protected $table = 'categoria_conta';
	protected $fillable = ['identificador', 'nome', 'codigo', 'parent_id'];

	public static $rules = [
		'nome' => 'required'
	];

	public function pai()
	{
		return $this->belongsTo('CategoriasConta', 'parent_id');
	}

	public $timestamps = false;
}