<?php

class Cliente extends Eloquent {
	protected $table = 'clientes';
	protected $fillable = [	'nome','nome_fantasia', 'cpf_cnpj', 'email', 'telefone_fixo',
							'telefone_celular', 'endereco_id'];
	protected $guarded = array();

	public static $rules = array(
			'nome'		=> 'required',
			'cpf_cnpj'	=> 'required'
		);
	public function filiais() {
		return $this->belongsToMany('Endereco');
	}

	public function matriz() {
		return $this->belongsTo('Endereco','endereco_id');
	}


	public $timestamps = false;
}