<?php

class UnidadesTableSeeder extends Seeder {

	public function run()
	{
		Unidade::create(array(
			'nome'     		=> 'Unidades',
			'abreviatura'	=> 'un'
			)
		);
		Unidade::create(array(
			'nome'     		=> 'Sacos',
			'abreviatura'	=> 'sc'
			)
		);
		Unidade::create(array(
			'nome'     		=> 'Litros',
			'abreviatura'	=> 'l'
			)
		);
		Unidade::create(array(
			'nome'     		=> 'Kilogramas',
			'abreviatura'	=> 'kg'
			)
		);
		Unidade::create(array(
			'nome'     		=> 'Metros Cubicos',
			'abreviatura'	=> 'm³'
			)
		);	
	}
};