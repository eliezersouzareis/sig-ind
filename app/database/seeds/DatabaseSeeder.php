<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('ConfsTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('UnidadesTableSeeder');
		$this->call('CategoriasContasTableSeeder');
		$this->call('FormaPagamentoTableSeeder');

	}

}
