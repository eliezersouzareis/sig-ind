<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CategoriasContasTableSeeder extends Seeder {

	public function run()
	{
		CategoriaConta::create([
			'identificador' => 1,
			'nome'			=> 'Custo Direto de Produção',
			'codigo'		=> '1'
		]);
		CategoriaConta::create([
			'identificador' => 2,
			'nome'			=> 'Custo Fixo',
			'codigo'		=> '2'
		]);
		CategoriaConta::create([
			'identificador' => 3,
			'nome'			=> 'Custo Variável',
			'codigo'		=> '3'
		]);
		CategoriaConta::create([
			'identificador' => 4,
			'nome'			=> 'Impostos',
			'codigo'		=> '4'
		]);
		CategoriaConta::create([
			'identificador' => 5,
			'nome'			=> 'Folha de Pagamento',
			'codigo'		=> '5'
		]);
	}

}