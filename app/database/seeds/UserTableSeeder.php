<?php

class UserTableSeeder extends Seeder
{

	public function run()
	{
		User::create(array(
			'name'     => 'Administrador',
			'username' => 'admin',
			'email'    => 'mail@mail.com',
			'password' => Hash::make('Senha.123')
		));
	}

}
