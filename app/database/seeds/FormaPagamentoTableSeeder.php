<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class FormaPagamentoTableSeeder extends Seeder {

	public function run()
	{
		//['nome', 'sigla','codigo', 'parent_id'];
			FormaPagamento::create([
				'nome'		=> 'a vista',
				'sigla'		=> '',
				'codigo'	=> '1',
				'parent_id'	=> null
			]);
			FormaPagamento::create([
				'nome'		=> 'a prazo',
				'sigla'		=> '',
				'codigo'	=> '2',
				'parent_id'	=> null
			]);
			FormaPagamento::create([
				'nome'		=> 'Permuta',
				'sigla'		=> '',
				'codigo'	=> '3',
				'parent_id'	=> null
			]);
			FormaPagamento::create([
				'nome'		=> 'Dinheiro',
				'sigla'		=> 'CASH',
				'codigo'	=> '1.1',
				'parent_id'	=> 1
			]);
			FormaPagamento::create([
				'nome'		=> 'Pagamento Online',
				'sigla'		=> 'ONLINE',
				'codigo'	=> '1.2',
				'parent_id'	=> 1
			]);
			FormaPagamento::create([
				'nome'		=> 'Débito',
				'sigla'		=> 'DEBITO',
				'codigo'	=> '1.3',
				'parent_id'	=> 1
			]);
			FormaPagamento::create([
				'nome'		=> 'Cartão',
				'sigla'		=> 'CARTAO',
				'codigo'	=> '1.4',
				'parent_id'	=> 1
			]);
			FormaPagamento::create([
				'nome'		=> 'Cheque',
				'sigla'		=> 'CHEQUE',
				'codigo'	=> '1.5',
				'parent_id'	=> 1
			]);
			FormaPagamento::create([
				'nome'		=> 'Transferência',
				'sigla'		=> 'TRANSF',
				'codigo'	=> '1.6',
				'parent_id'	=> 1
			]);
			$lastId = DB::getPdo()->lastInsertId();
			FormaPagamento::create([
				'nome'		=> 'TED',
				'sigla'		=> 'TED',
				'codigo'	=> '1.6.1',
				'parent_id'	=> $lastId
			]);
			FormaPagamento::create([
				'nome'		=> 'TEV',
				'sigla'		=> 'TEV',
				'codigo'	=> '1.6.2',
				'parent_id'	=> $lastId
			]);
			FormaPagamento::create([
				'nome'		=> 'DOC',
				'sigla'		=> 'DOC',
				'codigo'	=> '1.6.3',
				'parent_id'	=> $lastId
			]);

			FormaPagamento::create([
				'nome'		=> 'Boleto',
				'sigla'		=> 'BOLETO',
				'codigo'	=> '2.1',
				'parent_id'	=> 2
			]);
			FormaPagamento::create([
				'nome'		=> 'Cartão',
				'sigla'		=> 'CARTAO',
				'codigo'	=> '2.2',
				'parent_id'	=> 2
			]);
			FormaPagamento::create([
				'nome'		=> 'Cheque',
				'sigla'		=> 'CHEQUE',
				'codigo'	=> '2.3',
				'parent_id'	=> 2
			]);
			FormaPagamento::create([
				'nome'		=> 'produto',
				'sigla'		=> '',
				'codigo'	=> '3.1',
				'parent_id'	=> 3
			]);
			FormaPagamento::create([
				'nome'		=> 'insumo',
				'sigla'		=> '',
				'codigo'	=> '3.2',
				'parent_id'	=> 3
			]);
	}

}