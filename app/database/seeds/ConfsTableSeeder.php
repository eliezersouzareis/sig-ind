<?php

class ConfsTableSeeder extends Seeder {

	public function run()
	{/*
		Confs::create(array(
			'conf_name' => '',
			'type' => '',
			'value' => ''
			));
	*/
		Confs::create(array(
			'conf_name' => 'NomeEmpresa',
			'type' => 'string',
			'value' => ''
			));
		Confs::create(array(
			'conf_name' => 'CNPJ',
			'type' => 'string',
			'value' => ''
			));
		Confs::create(array(
			'conf_name' => 'endereco',
			'type' => 'json',
			'value' => ''
			));
		Confs::create(array(
			'conf_name' => 'telefones',
			'type' => 'json',
			'value' => ''
			)); 
		Confs::create(array(
			'conf_name' => 'logo',
			'type' => 'string',
			'value' => ''
			));
	}

}