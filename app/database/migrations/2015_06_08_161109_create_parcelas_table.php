<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateParcelasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('parcelas', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('forma_pagamento_id')->unsigned();

			$table->date('data_vencimento')->nullable();
			$table->date('data_pagamento')->nullable();
			$table->decimal('valor',16,2)->nullable();
			$table->decimal('valor_pago',16,2)->nullable();
			
			$table->integer('cheque_id')->unsigned()->nullable();//?

			$table->integer('caixa_id')->unsigned()->nullable();//?
			
			
			
			//$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('parcelas');
	}

}
