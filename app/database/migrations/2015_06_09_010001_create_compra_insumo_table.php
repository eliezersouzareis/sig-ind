<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompraInsumoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('compra_insumo', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('compra_id')->unsigned()->index();
			$table->integer('insumo_id')->unsigned()->index();
			$table->integer('unidade_id')->unsigned()->index();
			$table->double('qtd',32,8)->nullable();
			$table->decimal('preco',16,2)->nullable();

			//$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('compra_insumo');
	}

}
