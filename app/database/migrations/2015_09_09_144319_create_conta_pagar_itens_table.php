<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContaPagarItensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conta_pagar_itens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('conta_pagar_id')->unsigned()->index();
			$table->string('nome');
			$table->double('qtd',32,8);
			$table->decimal('valor',16,2);
			
			//$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conta_pagar');
	}

}
