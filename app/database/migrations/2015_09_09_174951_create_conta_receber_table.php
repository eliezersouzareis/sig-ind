<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContaReceberTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conta_receber', function(Blueprint $table)
		{
			//protected $fillable = [ 'data', 'nota', 'valor_total', 'cliente_id'];
			$table->increments('id');
			$table->date('data');
			$table->string('nota')->nullable();
			$table->decimal('valor_total',16,2)->nullable();
			$table->decimal('desconto',16,2)->nullable()->default(0.00);
			$table->integer('cliente_id')->unsigned()->index();
			//$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conta_receber');
	}

}
