<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeys extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cheques', function(Blueprint $table){
			$table->foreign('cliente_id')->references('id')->on('clientes');
		});

		Schema::table('clientes', function(Blueprint $table){
			$table->foreign('endereco_id')->references('id')->on('enderecos')->onDelete('cascade');
		});

		Schema::table('cliente_endereco', function(Blueprint $table){
			$table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade');
			$table->foreign('endereco_id')->references('id')->on('enderecos')->onDelete('cascade');
		});

		Schema::table('compras', function(Blueprint $table){
			$table->foreign('fornecedor_id')->references('id')->on('fornecedores');
		});

		Schema::table('compra_insumo',function(Blueprint $table){
			$table->foreign('compra_id')->references('id')->on('compras')->onDelete('cascade');
			$table->foreign('insumo_id')->references('id')->on('insumos')->onDelete('cascade');
			$table->foreign('unidade_id')->references('id')->on('unidades')->onDelete('cascade');
		});

		Schema::table('conta_pagar', function(Blueprint $table){
			$table->foreign('compra_id')->references('id')->on('compras')->onDelete('cascade');
			$table->foreign('categoria_conta_id')->references('id')->on('categoria_conta')->onDelete('cascade');
		});

		Schema::table('conta_pagar_itens', function(Blueprint $table){
			$table->foreign('conta_pagar_id')->references('id')->on('conta_pagar')->onDelete('cascade');
		});

		Schema::table('conta_receber', function(Blueprint $table){
			$table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade');
		});

		Schema::table('conta_receber_itens', function(Blueprint $table){
			$table->foreign('pedido_id')->references('id')->on('pedidos')->onDelete('cascade');
			$table->foreign('conta_receber_id')->references('id')->on('conta_receber')->onDelete('cascade');
		});

		Schema::table('entregas', function(Blueprint $table){
			$table->foreign('endereco_id')->references('id')->on('enderecos');
			$table->foreign('funcionario_id')->references('id')->on('funcionarios');
			$table->foreign('veiculo_id')->references('id')->on('veiculos');
		});
		
		Schema::table('fornecedores', function(Blueprint $table){
			$table->foreign('endereco_id')->references('id')->on('enderecos')->onDelete('cascade');
		});

		Schema::table('fornecedor_insumo', function(Blueprint $table){
			$table->foreign('fornecedor_id')->references('id')->on('fornecedores')->onDelete('cascade');
			$table->foreign('insumo_id')->references('id')->on('insumos')->onDelete('cascade');
		});

		Schema::table('funcionarios', function(Blueprint $table){
			$table->foreign('cargo_id')->references('id')->on('cargos');
			$table->foreign('endereco_id')->references('id')->on('enderecos')->onDelete('cascade');
		});
		
		Schema::table('insumos', function(Blueprint $table){
			$table->foreign('unidade_id')->references('id')->on('unidades');
		});

		Schema::table('insumo_produto', function(Blueprint $table){
			$table->foreign('insumo_id')->references('id')->on('insumos')->onDelete('cascade');
			$table->foreign('unidade_id')->references('id')->on('unidades')->onDelete('cascade');
			$table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
		});

		Schema::table('insumo_unidade', function(Blueprint $table){
			$table->foreign('insumo_id')->references('id')->on('insumos')->onDelete('cascade');
			$table->foreign('unidade_id')->references('id')->on('unidades')->onDelete('cascade');
		});
							
		Schema::table('parcelas', function(Blueprint $table){
			$table->foreign('caixa_id')->references('id')->on('caixas')->onDelete('cascade');
			$table->foreign('cheque_id')->references('id')->on('cheques');
			$table->foreign('forma_pagamento_id')->references('id')->on('forma_pagamento');
		});

		Schema::table('pedidos', function(Blueprint $table){
			$table->foreign('cliente_id')->references('id')->on('clientes');
			$table->foreign('entrega_id')->references('id')->on('entregas');
		});				

		Schema::table('producao', function(Blueprint $table){
			$table->foreign('produto_id')->references('id')->on('produtos');
		});

		Schema::table('produtos', function(Blueprint $table){
			$table->foreign('unidade_id')->references('id')->on('unidades');
		});
										
		Schema::table('pedido_produto', function(Blueprint $table){
			$table->foreign('pedido_id')->references('id')->on('pedidos')->onDelete('cascade');
			$table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cheques', function(Blueprint $table){
			$table->dropForeign('cheques_cliente_id_foreign');
		});

		Schema::table('clientes', function(Blueprint $table){
			$table->dropForeign('clientes_endereco_id_foreign');
		});

		Schema::table('cliente_endereco', function(Blueprint $table){
			$table->dropForeign('cliente_endereco_cliente_id_foreign');
			$table->dropForeign('cliente_endereco_endereco_id_foreign');
		});

		Schema::table('compras', function(Blueprint $table){
			$table->dropForeign('compras_fornecedor_id_foreign');
			$table->dropForeign('compras_insumo_id_foreign');
		});

		Schema::table('conta_pagar', function(Blueprint $table){
			$table->dropForeign('conta_pagar_compra_id_foreign');
		});

		Schema::table('fornecedores', function(Blueprint $table){
			$table->dropForeign('fornecedores_endereco_id_foreign');
		});

		Schema::table('fornecedor_insumo', function(Blueprint $table){
			$table->dropForeign('fornecedores_insumos_fornecedor_id_foreign');
			$table->dropForeign('fornecedores_insumos_insumo_id_foreign');
		});
		
		Schema::table('funcionarios', function(Blueprint $table){
			$table->dropForeign('funcionarios_cargo_id_foreign');
			$table->dropForeign('funcionarios_veiculo_id_foreign');
		});
		
		Schema::table('insumos', function(Blueprint $table){
			$table->dropForeign('insumos_unidade_id_foreign');
		});

		Schema::table('insumo_produto', function(Blueprint $table){
			$table->dropForeign('insumo_produto_insumo_id_foreign');
			$table->dropForeign('insumo_produto_produto_id_foreign');
			$table->dropForeign('insumo_produto_unidade_id_foreign');
		});

		Schema::table('insumo_unidade', function(Blueprint $table){
			$table->dropForeign('insumo_unidade_insumo_id_foreign');
			$table->dropForeign('insumo_unidade_unidade_id_foreign');
		});																

		Schema::table('pagamentos', function(Blueprint $table){
			$table->dropForeign('pagamentos_caixa_id_foreign');
			$table->dropForeign('pagamentos_compra_id_foreign');
			$table->dropForeign('pagamentos_pagamentos_categorias_id_foreign');
		});
		
		Schema::table('pagamentos_cheques', function(Blueprint $table){
			$table->dropForeign('pagamentos_cheques_cheque_id_foreign');
			$table->dropForeign('pagamentos_cheques_pagamento_id_foreign');
		});

		Schema::table('parcelas', function(Blueprint $table){
			$table->dropForeign('parcelas_caixa_id_foreign');
			$table->dropForeign('parcelas_cheque_id_foreign');
			$table->dropForeign('parcelas_forma_pagamento_id_foreign');
		});
		
		Schema::table('pedidos', function(Blueprint $table){
			$table->dropForeign('pedidos_cliente_id_foreign');
			$table->dropForeign('pedidos_entrega_id_foreign');
		});

		Schema::table('producao', function(Blueprint $table){
			$table->dropForeign('producao_produto_id_foreign');
		});

		Schema::table('produtos', function(Blueprint $table){
			$table->dropForeign('produtos_unidade_id_foreign');
		});

		Schema::table('pedido_produto', function(Blueprint $table){
			$table->dropForeign('pedido_produto_pedido_id_foreign');
			$table->dropForeign('pedido_produto_produto_id_foreign');
		});


	}

}
