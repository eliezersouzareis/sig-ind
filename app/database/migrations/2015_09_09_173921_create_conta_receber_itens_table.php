<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContaReceberItensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conta_receber_itens', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('conta_receber_id')->unsigned();
			$table->integer('pedido_id')->unsigned();

			//$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conta_receber_itens');
	}

}
