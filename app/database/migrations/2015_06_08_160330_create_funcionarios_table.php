<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFuncionariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('funcionarios', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nome')->nullable();
			$table->string('cpf')->nullable();
			$table->string('email')->nullable();
			$table->string('telefone_fixo')->nullable();
			$table->string('telefone_celular')->nullable();
			
			$table->integer('cargo_id')->unsigned()->nullable();
			
			$table->decimal('salario',16,2)->nullable();
			$table->string('observacoes')->nullable();

			$table->integer('endereco_id')->unsigned()->nullable();

			
			$table->date('data_admissao')->nullable();
			$table->date('data_demissao')->nullable();
			$table->boolean('ativo')->default(true);

			//$table->timestamps();
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('funcionarios');
	}

}
