<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEnderecosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('enderecos', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nome')->nullable();
			$table->string('contato')->nullable();
			$table->string('telefone')->nullable();
			$table->string('logradouro')->nullable();
			$table->string('numero')->nullable();
			$table->string('complemento')->nullable();
			$table->string('referencia')->nullable();
			$table->string('bairro')->nullable();
			$table->string('cep', 9)->nullable();
			$table->string('cidade')->nullable();
			$table->string('estado',2)->nullable();
			
			//$table->integer('cliente_id')->unsigned();
			

			//$table->timestamps();
		});
		
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('obras');
	}

}
