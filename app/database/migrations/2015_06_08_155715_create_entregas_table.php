<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEntregasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('entregas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('endereco_id')->unsigned();
			$table->integer('funcionario_id')->unsigned()->nullable();
			$table->integer('veiculo_id')->unsigned()->nullable();
			//$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('entregas');
	}

}
