<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContaPagarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conta_pagar', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('fornecedor');

			$table->date('data')->nullable();

			$table->date('nf_data')->nullable();
			$table->text('nf_num')->nullable();
			
			$table->integer('categoria_conta_id')->unsigned()->nullable();
			
			$table->string('nota')->nullable();
			
			$table->decimal('valor_total',16,2)->nullable();
			$table->decimal('desconto',16,2)->nullable()->default(0.00);

			$table->integer('compra_id')->unsigned()->nullable();
			
			
			//$table->timestamps();
		});
		
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conta_pagar');
	}

}
