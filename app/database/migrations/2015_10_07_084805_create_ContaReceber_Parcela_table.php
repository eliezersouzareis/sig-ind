<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContaReceberParcelaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conta_receber_parcela', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('conta_receber_id')->unsigned()->index();
			$table->foreign('conta_receber_id')->references('id')->on('conta_receber')->onDelete('cascade');
			$table->integer('parcela_id')->unsigned()->index();
			$table->foreign('parcela_id')->references('id')->on('parcelas')->onDelete('cascade');
			//$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conta_receber_parcela');
	}

}
