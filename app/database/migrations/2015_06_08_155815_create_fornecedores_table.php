<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFornecedoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fornecedores', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nome')->nullable();
			$table->string('nome_fantasia')->nullable();
			$table->string('cpf_cnpj')->nullable();
			$table->string('email')->nullable();
			$table->string('telefone_fixo')->nullable();
			$table->string('telefone_celular')->nullable();
			$table->text('descricao')->nullable();

			$table->integer('endereco_id')->unsigned()->nullable();

			//$table->timestamps();
		});
		
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{	
		Schema::drop('fornecedores');
	}

}
