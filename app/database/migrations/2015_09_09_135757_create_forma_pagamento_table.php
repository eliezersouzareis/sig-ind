<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFormaPagamentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forma_pagamento', function(Blueprint $table)
		{
			$table->increments('id');
			//$table->integer('identificador')->unsigned();
			$table->string('nome');
			$table->string('sigla')->nullable();
			$table->string('codigo')->unique();
			$table->integer('parent_id')->unsigned()->nullable();
			
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forma_pagamento');
	}

}
