<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInsumoUnidadeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('insumo_unidade', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->integer('insumo_id')->unsigned();
			$table->integer('unidade_id')->unsigned();
			$table->double('qtd',32,8)->unsigned();
			//$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('insumo_unidade');
	}

}
