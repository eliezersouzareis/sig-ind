<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriaContaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categoria_conta', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('identificador')->unsigned();
			$table->string('nome');
			$table->string('codigo')->unique();
			$table->integer('parent_id')->nullable();
		});

		/*Schema::table('categorias_conta', function(Blueprint $table){
			$table->foreign('parent_id')->references('id')->on('categorias_conta')->onDelete('cascade');
		});/**/
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		/*Schema::table('categorias_conta', function(Blueprint $table){
			$table->dropForeign('categorias_conta_parent_id_foreign');
		});/**/
		Schema::drop('categorias_conta');
	}

}
