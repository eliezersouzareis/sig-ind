<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateComprasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('compras', function(Blueprint $table)
		{
			$table->increments('id');

			$table->date('data')->nullable();

			$table->integer('fornecedor_id')->unsigned();

			$table->decimal('valor_total',16,2)->nullable();
			$table->decimal('valor_final',16,2)->nullable();

			$table->text('notas')->nullable();

			//$table->timestamps();
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('compras');
	}

}
