<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFornecedorInsumoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fornecedor_insumo', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('fornecedor_id')->unsigned();
			
			$table->integer('insumo_id')->unsigned();

			$table->decimal('preco',16,2)->nullable()->default(0.00);

			//$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fornecedor_insumo');
	}

}
