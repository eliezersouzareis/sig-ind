<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInsumoProdutoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('insumo_produto', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->integer('produto_id')->unsigned();	
			$table->integer('insumo_id')->unsigned();
			$table->integer('unidade_id')->unsigned();
			$table->double('qtd',32,8);

			//$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('insumo_produto');
	}

}
