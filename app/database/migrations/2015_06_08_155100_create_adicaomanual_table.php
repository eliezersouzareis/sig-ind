<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdicaomanualTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adicaomanual', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('data');
			$table->integer('produto_id')->unsigned();
			$table->double('qtd',32,8);
			//$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adicaomanual');
	}

}
