<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChequesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cheques', function(Blueprint $table)
		{
			$table->increments('id');

			$table->date('data_entrada')->nullable();
			
			$table->integer('cliente_id')->unsigned()->nullable();
			
			$table->string('banco')->nullable();
			$table->string('numero_cheque')->nullable();
			$table->string('valor')->nullable();
			$table->date('vencimento')->nullable();
			
			$table->date('compensacao')->nullable();
			$table->boolean('compensado')->default(false)->nullable();
			
			//$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cheques');
	}

}
