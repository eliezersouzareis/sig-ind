<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProdutosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produtos', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nome')->nullable();
			$table->string('codigo')->unique();
			$table->integer('unidade_id')->unsigned();

			$table->string('descricao')->nullable();
			$table->double('rendimento',32,8)->nullable();
			$table->double('qtd_estoque',32,8)->nullable();
			$table->decimal('preco',16,2)->default(0.00)->nullable();
			
			//$table->timestamps();
		});


	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produtos');
	}

}
