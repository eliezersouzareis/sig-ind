<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePedidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pedidos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('codigo')->unique();
			$table->integer('cliente_id')->unsigned();
			$table->integer('entrega_id')->unsigned()->nullable();
			$table->date('data')->nullable();
			$table->string('hora')->nullable();
			$table->decimal('total',16,2)->default(0.00)->nullable();
			$table->string('observacoes')->nullable();
			$table->boolean('efetivado')->default(false);
			
			//$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pedidos');
	}

}
