<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProducaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('producao', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('codigo')->unique();
			$table->date('data');
			$table->date('data_fim')->nullable();
			$table->integer('produto_id')->unsigned();
			$table->double('qtd_inicial',32,8);
			$table->double('qtd_final',32,8)->default(0.00);
			$table->boolean('concluido')->default(false);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('producao');
	}

}
