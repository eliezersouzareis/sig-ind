<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCaixasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('caixas', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nome');
			$table->string('nota')->nullable();
			$table->decimal('saldo',16,2)->nullable();
			
			//$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('caixas');
	}

}
