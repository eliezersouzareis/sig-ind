<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePedidoProdutoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pedido_produto', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->integer('pedido_id')->unsigned();
			$table->integer('produto_id')->unsigned();
			$table->decimal('qtd',16,2)->default(0.00);
			$table->decimal('preco',16,2)->default(0.00);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pedido_produto');
	}

}
