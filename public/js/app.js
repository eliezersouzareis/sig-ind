// ~~~~~~ App declaration ~~~~~~

var app = angular.module('sigind', []);
app.config(function ($interpolateProvider) {
		$interpolateProvider.startSymbol('[[');
		$interpolateProvider.endSymbol(']]');
});

// ~~~~~~ || ~~~~~~


// ~~~~~~ some helpers ~~~~~~


//filtro para array.prototype.map()
	function byId(obj)
	{
		return obj.id;
	};

//vai para o link
	function url(link)
	{
		window.location = link;
	}

	app.directive('jqDatepicker',function() {
		return {
			restrict: 'A',
			link : function (scope, element, attrs, ngModelCtrl) {
				$(function(){
					element.datepicker({
						dateFormat:'yy-mm-dd',
						onSelect:function (date) {
							scope.$apply(function () {
								ngModelCtrl.$setViewValue(date);
							});
						}
					});
				});
			}
		};
	});

// ~~~~~~ || ~~~~~~


// ~~~~~~ Controllers ~~~~~~

app.controller('SelecionarCliente', function($scope, $http){
	$http.get('/json/Clientes').success(function(clientes) {
		$scope.clientes = clientes;
	});

	$scope.pegaCliente = function(cnome, cid) {
		$scope.valores = {
			nome:cnome, novoid:cid
		};
	}
});

app.controller('buscaEndereco', function($scope, $http) {
	$scope.endereco = new Object();

	$scope.buscaCEP= function() {
		$http.get('http://api.postmon.com.br/v1/cep/'+$scope.CEP).success(function(dados){
			console.log(dados);
			$scope.endereco = dados;
		});
	};
});

app.controller('AdicionarProduto', function($scope, $http){
	$scope.produtoId = null;
	$http.get('/json/Insumos').success(function(data) {
		$scope.insumos = data;
		$http.get('/json/Unidades').success(function(data){
			$scope.unidades = data;
			$http.get('/json/InsumosUnidades').success(function(data){
				$scope.insumosUnidades = data;
				angular.forEach($scope.insumosUnidades, function(item){
					var pos = $scope.unidades.map(byId).indexOf(item.unidade_id);
					item.unidade = $scope.unidades[pos];
				});
				angular.forEach($scope.insumos, function(item){
					var keys = $scope.insumosUnidades.map(function(obj){return obj.insumo_id});
					var pos = Array();
					var aux = keys.indexOf(item.id);
					while(aux !== -1)
					{
						pos.push(aux);
						aux = keys.indexOf(item.id, aux+1);
					}    
					item.unidades = Array();
					pos.forEach(function(key){
						item.unidades.push($scope.insumosUnidades[key]);
					});
				});
				console.log($scope.insumos);
				if($scope.produtoId)
				{

					$http.get('/json/Receita',{params:{ id : $scope.produtoId }}).success(function(data){  
						angular.forEach(data,function(insumo){
							var pos = $scope.insumos.map(byId).indexOf(insumo.insumo_id);
							$scope.insumos[pos].qtd_usada = insumo.qtd;
							$scope.insumos[pos].unidade_usada = insumo.unidade_id;
							$scope.insumosSelecionados.push($scope.insumos[pos]);
							$scope.insumos.splice(pos, 1);
							//console.log($scope.insumosSelecionados);
						});
					});
				}
			});
		});
	});
	
	$scope.insumosSelecionados = Array();

	$scope.getInsumo = function(iid) {
		var pos = $scope.insumos.map(byId).indexOf(iid);
		$scope.insumosSelecionados.push($scope.insumos[pos]);
		$scope.insumos.splice(pos, 1);
	};

	$scope.delInsumo = function(iid) {
		var pos = $scope.insumosSelecionados.map(byId).indexOf(iid);
		$scope.insumos.push( $scope.insumosSelecionados[pos]);
		$scope.insumosSelecionados.splice(pos, 1);
	};
});

app.controller('AdicionarInsumo', function($scope, $http){

	$scope.unidadesSelecionadas = Array();
	$scope.unidadePrimaria = null;
	$scope.insumoId = null;

	$http.get('/json/Unidades').success(function(itens) {
		$scope.unidades = itens;
		console.log($scope.unidades);
		console.log($scope.insumoId);
		if($scope.insumoId)
			{
				$http.get('/json/Conversao',{params:{ id : $scope.insumoId }}).success(function(itens){
			 
					$scope.unidadesSelecionadas = itens;
					angular.forEach($scope.unidadesSelecionadas,function(unidade){
						var pos = $scope.unidades.map(byId).indexOf(unidade.id);
						$scope.unidades.splice(pos, 1);
						console.log($scope.unidades);
					});
						var pos = $scope.unidadesSelecionadas.map(byId).indexOf(Number($scope.aux.id));
						$scope.unidadePrimaria = {
							id: $scope.unidadesSelecionadas[pos].id,
							nome: $scope.unidadesSelecionadas[pos].nome,
							abreviatura: $scope.unidadesSelecionadas[pos].abreviatura,
							qtd: $scope.unidadesSelecionadas[pos].qtd
						}
						$scope.unidadesSelecionadas.splice(pos, 1);
				});
			}
	});
	
	$scope.getUnidade = function(uid) {
		var pos = $scope.unidades.map(byId).indexOf(uid);
		if($scope.unidadePrimaria == null)
		{
			$scope.unidadePrimaria = $scope.unidades[pos];
			$scope.unidadePrimaria.qtd = 1;
			$scope.unidades.splice(pos, 1);
			return 1;
		}
		$scope.unidadesSelecionadas.push($scope.unidades[pos]);
		$scope.unidades.splice(pos, 1);
	};

	$scope.delUnidade = function(uid) {
		var pos = $scope.unidadesSelecionadas.map(byId).indexOf(uid);
		$scope.unidades.push( $scope.unidadesSelecionadas[pos] );
		$scope.unidadesSelecionadas.splice(pos, 1);
	};

	$scope.delPrimaria = function() {
		$scope.unidades.push( $scope.unidadePrimaria );
		$scope.unidadePrimaria = null;
	};

});

app.controller('AdicionarPedido', function($scope, $http){
	$scope.page = 0;
	$scope.produtosSelecionados = Array();
	$scope.produto = new Object;
	$http.get('/json/Clientes').success(function(dados) {
		$scope.clientes = dados;
		if($scope.cliente_id)
		{
			$scope.getCliente($scope.cliente_id);
			$scope.getObras();
		}
	});
	$http.get('/json/Produtos').success(function(dados) {
		$scope.produtos = dados;
		//console.log($scope.produtos);
		$http.get('/json/Unidades').success(function(dados) {
			$scope.unidades = dados;

			angular.forEach($scope.produtos,function(item){
				var pos = $scope.unidades.map(byId).indexOf(item.unidade_id);
				item.unidade = $scope.unidades[pos];
			});
			if($scope.pedido_id)
			{
				$http.get('/json/PedidoProdutos',{params:{id:$scope.pedido_id}}).success(function(dados){
					$scope.get1Produto(dados[0].id);
					$scope.produto.preco = dados[0].pivot.preco;
					$scope.produto.qtd = dados[0].pivot.qtd;
					
					angular.forEach(dados,function(prod){
						$scope.getProduto(prod.id);
						var pos = $scope.produtosSelecionados.map(byId).indexOf(prod.id);
						$scope.produtosSelecionados[pos].preco = prod.pivot.preco;
						$scope.produtosSelecionados[pos].qtd = prod.pivot.qtd;
					});
				});
			}
			//console.log($scope.produtos);
		});
	});
	
	$scope.getCliente = function(cid) {
		var pos = $scope.clientes.map(byId).indexOf(cid);
		//console.log($scope.clientes[pos]);
		$scope.cliente = $scope.clientes[pos];
		$scope.endereco = new Object;
	};
	$scope.getProduto = function(pid) {
		var pos = $scope.produtos.map(byId).indexOf(pid);
		$scope.produtos[pos].qtd = 1;
		$scope.produtosSelecionados.push($scope.produtos[pos]);
		$scope.produtos.splice(pos, 1);
	};
	$scope.get1Produto = function(pid) {
		var pos = $scope.produtos.map(byId).indexOf(pid);
		$scope.produto = $scope.produtos[pos];
		//console.log($scope.produto);
		$scope.getComposicao(pid);
	};
	$scope.delProduto = function(pid) {
		var pos = $scope.produtosSelecionados.map(byId).indexOf(pid);
		$scope.produtos.push($scope.produtosSelecionados[pos]);
		$scope.produtosSelecionados.splice(pos, 1);
	};

	$scope.getObras = function() {
		$http.get('/json/ClienteEnderecos/',{params:{id:$scope.cliente.id}}).success(function(dados) {
			$scope.enderecos = dados;
		 	if($scope.endereco_id)
		 	{
		 		$scope.getEndereco($scope.endereco_id);
		 	}
		});
	};
	$scope.getComposicao = function(pid)
	{
		$http.get('/json/Receita',{params:{id:pid}}).success(function(dados) {
			$scope.produto.composicao = dados;
			$http.get('/json/Insumos').success(function(dados){
				$scope.insumos = dados;
				angular.forEach($scope.produto.composicao,function(ingrediente){
					var pos = $scope.insumos.map(byId).indexOf(ingrediente.insumo_id);
					ingrediente.insumo = $scope.insumos[pos];
					pos = $scope.unidades.map(byId).indexOf(ingrediente.unidade_id);
					ingrediente.unidade = $scope.unidades[pos];
				});
				//console.log($scope.produto);
			});
		});
	};

	$scope.getEndereco = function(eid) {
		var pos = $scope.enderecos.map(byId).indexOf(eid);
		$scope.endereco = $scope.enderecos[pos];
	 // console.log($scope.obra);
	};

	$scope.total = function()
	{
		return ((Number(($scope.produto.qtd||0)) * Number(($scope.produto.preco||0))) + Number($scope.taxa ||0)).toFixed(2);
	};

	$scope.calcTotal = function()
	{
		total = 0;
		angular.forEach($scope.produtosSelecionados, function(produto){
			produto.qtd = (produto.qtd||1);
			total += produto.qtd* produto.preco;
		});
		return total.toFixed(2);
	};
});

app.controller('AdicionarFormaPagamento', function($scope, $http){
	$scope.parcelas = Array();
	$scope.parcelaId = 0;
	$scope.sum_parcelas = 0;
	$scope.avista= Object();	
	$http.get('/json/FormaPagarLista').success(function(dados){
		$scope.formasPagamento = dados;
		if($scope.contapagar_id)
		{
			$http.get('/json/ContaPagar',{params:{id:$scope.contapagar_id}}).success(function(dados){
				$scope.conta = dados;
				console.log($scope.conta);
				$http.get('/json/ContaPagarParcelas',{params:{id:$scope.contapagar_id}}).success(function(dados){
					$scope.parcelas = dados;
					$('#opcoesPagamento a[href="#aprazo"]').tab('show')
					angular.forEach($scope.parcelas, function(parcela){
						$http.get('/json/FormaPagar',{params:{id:parcela.forma_pagamento_id}}).success(function(dados){
							parcela.formaPagamento = dados;
						});
					});
					console.log($scope.parcelas);
				});
			});
		}
	});

	$scope.getFormaPagamento = function(fid)
	{
		$http.get('/json/FormaPagarLista',{params:{id:fid}}).success(function(dados){
			//console.log(dados[0]);
			if(dados.length !== 0)
			{
				$scope.formasPagamento = dados;
				return false;
			}else{
				var pos = $scope.formasPagamento.map(byId).indexOf(fid);
				$scope.formaPagamentoSel = $scope.formasPagamento[pos];
				$http.get('/json/FormaPagarLista').success(function(dados){
					$scope.formasPagamento = dados;
				});
				$('#formaPagamentoModal').modal('hide');
			}
		});
	}
	$scope.clearFormaPagamento = function()
	{
		$http.get('/json/FormaPagarLista').success(function(dados){
			$scope.formasPagamento = dados;
		});
	};

	$scope.getFormaPagamentoParcela = function(pid,fid)
	{
		
		$http.get('/json/FormaPagarLista',{params:{id:fid}}).success(function(dados){
			if(dados.length !== 0)
			{
				$scope.formasPagamento = dados;
				return false;
			}else{
				var pos = $scope.formasPagamento.map(byId).indexOf(fid);
				if(pid === 'avista')
				{
					$scope.avista.formaPagamento = $scope.formasPagamento[pos];
				}else{
					var ppos = $scope.parcelas.map(byId).indexOf(pid);
					$scope.parcelas[ppos].formaPagamento = $scope.formasPagamento[pos];
				}
				$http.get('/json/FormaPagarLista').success(function(dados){
					$scope.formasPagamento = dados;
				});
				$('#formaPagamentoParcelaModal').modal('hide');
			}
		});
	}

	$scope.openFormaPagamentoParcelaModal = function(pid)
	{
		$scope.parcelaId = pid;
		$('#formaPagamentoParcelaModal').modal('show');
	}

	$scope.addParcela = function()
	{
		parcela = {
			id: $scope.parcelas.length ? $scope.parcelas[$scope.parcelas.length-1].id+1 : 0,
			valor: 0
		};
		$scope.parcelas.push(parcela);
	}

	$scope.delParcela = function(pid)
	{
		var pos = $scope.parcelas.map(byId).indexOf(pid);
		$scope.parcelas.splice(pos,1);
	}

	$scope.gerarParcelas = function()
	{
		
		$scope.parcelas = Array();
		var total = $scope.totalConta;
		total -= $scope.valorEntrada ? $scope.valorEntrada : 0;
		var valorParcela = total / $scope.numParcelas;
		if($scope.valorEntrada > 0 && $scope.valorEntrada < $scope.totalConta)
		{
			parcela = {
				id : 0,
				valor : $scope.valorEntrada,
				formaPagamento: $scope.formaPagamentoSel
			};
			$scope.parcelas.push(parcela);
		}
		console.log($scope.numParcelas);
		var i;
		for(i = 0; i < $scope.numParcelas; i++)
		{
			parcela = {
			id : $scope.parcelas.length ? $scope.parcelas[$scope.parcelas.length-1].id+1 : 0,
			valor : valorParcela,
			formaPagamento: $scope.formaPagamentoSel
			};
			$scope.parcelas.push(parcela);	
		}
	};
	$scope.sumParcelas = function()
	{
		sum = 0;
		angular.forEach($scope.parcelas, function(parcela){
			sum += Number(parcela.valor);
		});
		$scope.sum_parcelas = sum;
		return sum;
	};
});

app.controller('AdicionarContaPagar',function($scope, $http){
	$scope.categoriasConta = Array();
	$scope.cesta = Array();
	
	$http.get('/json/FornecedoresContas').success(function(dados){
		$("#autocomplete").autocomplete({source:dados});
	})

	$http.get('/json/CategoriasConta').success(function(dados){
		$scope.categoriasConta = dados;

		if($scope.contaPagarId)
		{
			$http.get('/json/ContaPagar',{params:{id:$scope.contaPagarId}}).success(function(dados){
				$scope.contaPagar = dados;
				$scope.getCategoria($scope.contaPagar.categoria_conta_id);
				$http.get('/json/ContaPagarItem',{params:{id:$scope.contaPagarId}}).success(function(dados){
					$scope.cesta = dados;
					console.log(dados);
				});
			});
		}
	});


	$scope.addItem = function()
	{
		$scope.cesta.push({
				id: $scope.cesta.length ? $scope.cesta[$scope.cesta.length-1].id+1  : 0,
				valor: null,
				qtd: null,
				total: 0
			});
	}

	$scope.delItem = function(id)
	{
		var pos = $scope.cesta.map(byId).indexOf(id);
		$scope.cesta.splice(pos,1);
	}

	$scope.sum = function()
	{
		var total = 0;
		angular.forEach($scope.cesta,function(item){
			total += Number(item.total);
		});
		total -= $scope.desconto || 0;
		return total;
	}

	$scope.getCategoria = function(cid)
	{
		var pos = $scope.categoriasConta.map(byId).indexOf(cid);
		$scope.categoriaConta = $scope.categoriasConta[pos];
	}
});

app.controller('AdicionarContaReceber', function($scope,$http){
	$scope.clientes = Array();
	$scope.cliente = Object();
	$scope.pedidos = Array();
	$scope.pedidosSel = Array();

	$http.get('/json/Clientes').success(function(dados){
		$scope.clientes = dados;
	});

	$scope.getCliente = function(cid)
	{
		var pos = $scope.clientes.map(byId).indexOf(cid);
		$scope.cliente = $scope.clientes[pos];
		$scope.pedidosSel = Array();
		$http.get('/json/Pedidos',{params:{id:$scope.cliente.id}}).success(function(dados){
			$scope.pedidos = dados;
		});
	};

	$scope.addPedido = function(pid)
	{
		var pos = $scope.pedidos.map(byId).indexOf(pid);
		$scope.pedidosSel.push($scope.pedidos[pos]);
		$scope.pedidos.splice(pos, 1);
	};

	$scope.delPedido = function(pid)
	{
		var pos = $scope.pedidosSel.map(byId).indexOf(pid);
		$scope.pedidos.push($scope.pedidosSel[pos]);
		$scope.pedidosSel.splice(pos, 1);
	};

	$scope.sum = function()
	{
		var total = 0;
		angular.forEach($scope.pedidosSel,function(item){
			total += item.total;
		});
		return total;		
	};
});

app.controller('EditarParcela', function($scope, $http){

	$http.get('/json/FormaPagarLista').success(function(dados){
		$scope.formasPagamento = dados;
		if($scope.formapagamentoId)
		{
			$http.get('/json/FormaPagar',{params:{id:$scope.formapagamentoId}}).success(function(dados){
				$scope.formaPagamentoSel = dados;
			});
		}
	});


	$scope.getFormaPagamento = function(fid)
	{
		$http.get('/json/FormaPagarLista',{params:{id:fid}}).success(function(dados){
			if(dados.length !== 0)
			{
				$scope.formasPagamento = dados;
				return false;
			}else{
				var pos = $scope.formasPagamento.map(byId).indexOf(fid);
				$scope.formaPagamentoSel = $scope.formasPagamento[pos];
				$http.get('/json/FormaPagarLista').success(function(dados){
					$scope.formasPagamento = dados;
				});
				$('#formaPagamentoModal').modal('hide');
			}
		});
	}

});

app.controller('PagarParcela',function($scope, $http){
	$scope.formasPagamento = Array();
	$scope.formaPagamentoSel = Object();
	$scope.caixaSel = Object();
	$scope.cesta = Array();
	$scope.cheques = Array();
	$scope.chequesSel = Array();

	$http.get('/json/Caixas').success(function(dados){
		$scope.caixas = dados;
	});
	$http.get('/json/FormaPagarLista').success(function(dados){
		$scope.formasPagamento = dados;
		if($scope.forma_pagamento_id)
		{
			$http.get('/json/FormaPagar',{params:{id:$scope.forma_pagamento_id}}).success(function(dados){
				$scope.formaPagamentoSel = dados;
			});
		}
	});
	$http.get('/json/Cheques').success(function(dados){
			$scope.cheques = dados;
	});

	$scope.getFormaPagamento = function(fid)
	{
		$http.get('/json/FormaPagarLista',{params:{id:fid}}).success(function(dados){
			//console.log(dados[0]);
			if(dados.length !== 0)
			{
				$scope.formasPagamento = dados;
				return false;
			}else{
				var pos = $scope.formasPagamento.map(byId).indexOf(fid);
				$scope.formaPagamentoSel = $scope.formasPagamento[pos];
				$http.get('/json/FormaPagarLista').success(function(dados){
					$scope.formasPagamento = dados;
				});
				$('#formaPagamentoModal').modal('hide');
			}
		});
	};

	$scope.getCaixa = function(cid)
	{
		var pos = $scope.caixas.map(byId).indexOf(cid);
		$scope.caixaSel = $scope.caixas[pos];		
	};

	$scope.addItem = function()
	{
		$scope.cesta.push({
				id: $scope.cesta.length ? $scope.cesta[$scope.cesta.length-1].id+1  : 0,
				valor: null,
				qtd: null,
				total: 0
			});
	}

	$scope.delItem = function(id)
	{
		var pos = $scope.cesta.map(byId).indexOf(id);
		$scope.cesta.splice(pos,1);
	}

	$scope.submitProprio = function()
	{
		$http.get('/json/SaveCheque',{params:$scope.chequeProprio}).success(function(dados){
			console.log(dados);
			$http.get('/json/Cheque',{params:{id:dados}}).success(function(dados)
				{
					$scope.chequesSel.push(dados);
					$http.get('/json/Cheques').success(function(dados){
						$scope.cheques = dados;
					});
					$scope.chequeProprio = {
						banco: "",
						numero_cheque: null,
						valor: null,
						vencimento: ""
					};
			});
		});
	}
	$scope.getCheque = function(cid) {
		var pos = $scope.cheques.map(byId).indexOf(cid);
		$scope.chequesSel.push($scope.cheques[pos]);
		$scope.cheques.splice(pos, 1);
	};

	$scope.removeCheque = function(cid) {
		var pos = $scope.chequesSel.map(byId).indexOf(cid);
		$scope.cheques.push($scope.chequesSel[pos]);
		$scope.chequesSel.splice(pos, 1);
	};	
});

app.controller('AdicionarFornecedor', function($scope, $http){
	$scope.insumosSelecionados = Array();
	$scope.insumos = Array();
	$scope.fornecedorId = null;
	$http.get('/json/Insumos').success(function(dados){
		$scope.insumos = dados;
		$http.get('/json/Unidades').success(function(dados){
			$scope.unidades = dados;
			angular.forEach($scope.insumos,function(insumo){
				var pos = $scope.unidades.map(byId).indexOf(insumo.unidade_id);
				insumo.unidade = $scope.unidades[pos];
			});
			if($scope.fornecedorId){
				$http.get('/json/FornecedorInsumo',{params:{ id : $scope.fornecedorId }}).success(function(dados)
				{
					angular.forEach(dados,function(item){
						var pos = $scope.insumos.map(byId).indexOf(item.insumo_id);
						$scope.insumos[pos].preco = item.preco;
						$scope.insumosSelecionados.push($scope.insumos[pos]);
						$scope.insumos.splice(pos, 1);
					});
				});
			}
			console.log($scope.insumos);
		});
	});

	$scope.getInsumo = function(iid) {
		var pos = $scope.insumos.map(byId).indexOf(iid);
		$scope.insumosSelecionados.push($scope.insumos[pos]);
		$scope.insumos.splice(pos, 1);
	};

	$scope.delInsumo = function(iid) {
		var pos = $scope.insumosSelecionados.map(byId).indexOf(iid);
		$scope.insumos.push($scope.insumosSelecionados[pos]);
		$scope.insumosSelecionados.splice(pos, 1);
	};
});

app.controller('AdicionarEntrada', function($scope, $http){
	$scope.insumo_abv = '';
	$scope.fornecedorSelecionado = new Object();
	$scope.insumosSelecionados = Array();
	$scope.compraId = null;

	$http.get('/json/CategoriasConta').success(function(dados){
		$scope.categoriasConta = dados;
	});

	$http.get('/json/Fornecedores').success(function(dados) {
		$scope.fornecedores = dados;		
		$http.get('/json/Insumos').success(function(dados) {
			$scope.insumos = dados;
			$http.get('/json/Unidades').success(function(dados) {
				$scope.unidades = dados;
				angular.forEach($scope.insumos,function(insumo){
					var pos = $scope.unidades.map(byId).indexOf(insumo.unidade_id);
					insumo.unidade = $scope.unidades[pos];
				}); 
				console.log($scope.compraId);
				if($scope.compraId)
				{
					$http.get('/json/Entrada',{params:{id: $scope.compraId }}).success(function(dados) {
						$scope.compra = dados;
						$scope.getFornecedor($scope.compra.fornecedor_id);
					});
				};
			});
		});
	});
	$scope.insumoUnidade = function() {
		var pos = $scope.insumos.map(byId).indexOf(Number($scope.insumoSelecionado.id));
		$scope.insumoSelecionado = $scope.insumos[pos];
	};

	$scope.getFornecedor = function(fid) {
		$scope.insumosSelecionados = Array();
		pos = $scope.fornecedores.map(byId).indexOf(fid);
		$scope.fornecedorSelecionado = $scope.fornecedores[pos];
		$http.get('/json/FornecedorInsumo',{params:{ id : fid }}).success( function (dados) {
			$scope.insumosFornecidos = dados;
			angular.forEach($scope.insumosFornecidos, function(insumo){
				var pos = $scope.insumos.map(byId).indexOf(insumo.insumo_id);
				insumo = Object.assign(insumo, $scope.insumos[pos]);
			});
			console.log($scope.insumosFornecidos);
			if($scope.compraId)
			{
				$http.get('/json/EntradaItem',{params:{id: $scope.compraId }}).success(function(dados) {
						angular.forEach(dados,function(item){
							var pos = $scope.insumosFornecidos.map(byId).indexOf(item.id);
							var insumo = $scope.insumosFornecidos[pos];
							insumo.preco = item.pivot.preco;
							insumo.qtd = item.pivot.qtd;
							console.log(insumo);
							$scope.insumosSelecionados.push(insumo);
							$scope.insumosFornecidos.splice(pos, 1);
						});
					});
			}
		});
	};	

	$scope.getInsumo = function(iid) {
		var pos = $scope.insumosFornecidos.map(byId).indexOf(iid);
		$scope.insumosSelecionados.push($scope.insumosFornecidos[pos]);
		$scope.insumosFornecidos.splice(pos, 1);
	}

	$scope.delInsumo = function(iid) {
		var pos = $scope.insumosSelecionados.map(byId).indexOf(iid);
		$scope.insumosFornecidos.push($scope.insumosSelecionados[pos]);
		$scope.insumosSelecionados.splice(pos, 1);		
	}

	$scope.sum = function(){
		var total = 0;
		angular.forEach($scope.insumosSelecionados,function(item){
			total += item.preco*item.qtd;
		});
		return total;
	}
	
	$scope.getCategoria = function(cid)
	{
		var pos = $scope.categoriasConta.map(byId).indexOf(cid);
		$scope.categoriaConta = $scope.categoriasConta[pos];
	}
});

app.controller('ViewContaPagar', function($scope,$http){
	console.log('do nothing yet');
});

app.controller('SelecionarFornecedorInsumo', function($scope, $http){
	$scope.insumo_abv = '';
	$http.get('/json/Fornecedores').success(function(dados) {
		$scope.fornecedores = dados;
		$http.get('/json/Insumos').success(function(dados) {
			$scope.insumos = dados;
			$http.get('/json/Unidades').success(function(dados) {
			$scope.unidades = dados;
			});
		});
	});
	$scope.insumoUnidade = function() {
		var pos = $scope.insumos.map(byId).indexOf(Number($scope.valoresf.insumo_id));
		var un_id = $scope.insumos[pos].unidade_id;
		pos = $scope.unidades.map(byId).indexOf(un_id);
		$scope.insumo_abv = $scope.unidades[pos].abreviatura;
	};
	$scope.pegaFornecedor = function(fid) {
		//??
	}
});

app.controller('SelecionarProduto', function($scope, $http){
	$http.get('/json/Produtos').success(function(dados) {
		$scope.produtos = dados;
		$http.get('/json/Unidades').success(function(dados) {
			$scope.unidades = dados;
			angular.forEach($scope.produtos,function(item){
				var pos = $scope.unidades.map(byId).indexOf(item.unidade_id);
				item.unidade = $scope.unidades[pos];
			});
		});
	});
	$scope.getProduto = function(pid) {
		var pos = $scope.produtos.map(byId).indexOf(pid);
		$scope.produtoSelecionado = $scope.produtos[pos];
	};
});

app.controller('MostrarItem', function($scope, $http, $sce){
	
	$scope.item = "";
	$scope.modalTitle = "";

	$scope.getItem = function(Title,Address){
		$scope.modalTitle = Title;
		$http.get(Address).success(function(resposta) {
			$scope.item = $sce.trustAsHtml(resposta);
		});
	};

});

app.controller('MostrarPedido', function($scope, $http, $sce){
	$scope.pedido = "";
	$scope.printPedido = function(id){
		$http.get('/pedidos/'+ id +'/print').success(function(resposta){
			$scope.pedido = $sce.trustAsHtml(resposta);
		});
	};
});