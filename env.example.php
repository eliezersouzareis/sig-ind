<?php

return array(

	'APP_DEBUG'	=> true,
	'APP_URL'	=> 'http://localhost',
	'APP_KEY'	=> '',
	'APP_TIMEZONE'	=> 'UTC',
	
	'DB_HOST'	=> 'localhost',
	'DB_DATABASE'	=> '',
	'DB_USERNAME'	=> '',
	'DB_PASSWORD'	=> ''

);
