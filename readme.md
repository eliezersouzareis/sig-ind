#SIG-IND - Sistema Gerencial para Indústrias

SIG-IND é um sistema de gerenciamento voltado à indústrias de pequeno e médio porte. Desenvolvido em PHP, utilizando Laravel, AngularJS e Bootstrap.


## Laravel PHP Framework

v4.2

[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

## AngularJS

 AngularJS v1.4.1

 (c) 2010-2015 Google, Inc. http://angularjs.org

 License: MIT

## Bootstrap

 [Bootstrap v3.3.4](http://getbootstrap.com)

 Copyright 2011-2015 Twitter, Inc.

 [Licensed under MIT](https://github.com/twbs/bootstrap/blob/master/LICENSE)

